package com.cab1.driver

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.TextUtils
import android.text.TextWatcher
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.cab1.driver.api.RestClient
import com.cab1.driver.model.StartTiripModel
import com.cab1.driver.pojo.StartTripPojo
import com.cab1.driver.util.MyUtils
import com.example.admin.myapplication.SessionManager
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_otp_verification.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*

class OtpVerificationActivity : AppCompatActivity(),View.OnKeyListener {

    var bookingId:String=""
    lateinit var sessionManager : SessionManager
    var currentLatLong: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp_verification)
        sessionManager=SessionManager(this)
        setSupportActionBar(toolbar)

        toolbar.title=resources.getString(R.string.verification)
        supportActionBar!!.setDisplayShowTitleEnabled(false)

        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }


        if (intent!=null)
        {
            if(intent.hasExtra("bookingId"))
            {
                bookingId=intent.getStringExtra("bookingId")
            }
            if(intent.hasExtra("currentLatLong"))
            {
                currentLatLong=intent.getStringExtra("currentLatLong")
            }
        }
        customTextViewResendOTP(tvOtpresend)
        edittext_pin_1.setOnKeyListener(this)
        edittext_pin_2.setOnKeyListener(this)
        edittext_pin_3.setOnKeyListener(this)
        edittext_pin_4.setOnKeyListener(this)



        edittext_pin_1.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (edittext_pin_1.text.toString().length == 1)
                //size as per your requirement
                {
                    edittext_pin_2.requestFocus()
                }
            }

        })
        edittext_pin_2.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (edittext_pin_2.text.toString().length == 1)
                //size as per your requirement
                {
                    edittext_pin_3.requestFocus()
                } else if (edittext_pin_2.text.toString().isEmpty()) {
                    edittext_pin_1.requestFocus()
                }
            }

        })
        edittext_pin_3.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (edittext_pin_3.text.toString().length == 1)
                //size as per your requirement
                {
                    edittext_pin_4.requestFocus()
                } else if (edittext_pin_3.text.toString().isEmpty()) {
                    edittext_pin_2.requestFocus()
                }
            }

        })
        edittext_pin_4.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (edittext_pin_4.text.toString().length == 1)
                //size as per your requirement
                {
                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(
                        edittext_pin_4.windowToken,
                        InputMethodManager.RESULT_UNCHANGED_SHOWN
                    )
                } else if (edittext_pin_4.text.toString().isEmpty()) {
                    edittext_pin_3.requestFocus()
                }
            }

        })

        btnSubmit.setOnClickListener {
            MyUtils.hideKeyboard1(this@OtpVerificationActivity)

            if (TextUtils.isEmpty(edittext_pin_1.text.toString()) && TextUtils.isEmpty(edittext_pin_2.text.toString())
                && TextUtils.isEmpty(edittext_pin_3.text.toString()) && TextUtils.isEmpty(edittext_pin_4.text.toString())
            ) {
                showSnackbar(this@OtpVerificationActivity.resources.getString(R.string.OTPV))
            } else if (TextUtils.isEmpty(edittext_pin_1.text.toString())) {
                showSnackbar(resources.getString(R.string.OTPV))
            } else if (TextUtils.isEmpty(edittext_pin_2.text.toString())) {
                showSnackbar(resources.getString(R.string.OTPV))


            } else if (TextUtils.isEmpty(edittext_pin_3.text.toString())) {
                showSnackbar(resources.getString(R.string.OTPV))
            } else if (TextUtils.isEmpty(edittext_pin_4.text.toString())) {
                showSnackbar(resources.getString(R.string.OTPV))
            } else {
                val OTPCOde = (edittext_pin_1.text.toString() + edittext_pin_2.text.toString()
                        + edittext_pin_3.text.toString() + edittext_pin_4.text.toString())

                if (MyUtils.isInternetAvailable(this@OtpVerificationActivity)) {
                    getStartTip(OTPCOde)
                } else {
                    showSnackbar(resources.getString(R.string.error_common_network)

                    )
                }

            }


        }

        edittext_pin_4.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if ((actionId == EditorInfo.IME_ACTION_DONE)) {
                    MyUtils.hideKeyboard1(this@OtpVerificationActivity)


                    if (TextUtils.isEmpty(edittext_pin_1.text.toString()) && TextUtils.isEmpty(edittext_pin_2.text.toString())
                        && TextUtils.isEmpty(edittext_pin_3.text.toString()) && TextUtils.isEmpty(edittext_pin_4.text.toString())
                    ) {
                        showSnackbar(this@OtpVerificationActivity.resources.getString(R.string.OTPV))
                    } else if (TextUtils.isEmpty(edittext_pin_1.text.toString())) {
                        showSnackbar(resources.getString(R.string.OTPV))
                    } else if (TextUtils.isEmpty(edittext_pin_2.text.toString())) {
                        showSnackbar(resources.getString(R.string.OTPV))


                    } else if (TextUtils.isEmpty(edittext_pin_3.text.toString())) {
                        showSnackbar(resources.getString(R.string.OTPV))
                    } else if (TextUtils.isEmpty(edittext_pin_4.text.toString())) {
                        showSnackbar(resources.getString(R.string.OTPV))
                    } else {
                        val OTPCOde = (edittext_pin_1.text.toString() + edittext_pin_2.text.toString()
                                + edittext_pin_3.text.toString() + edittext_pin_4.text.toString())

                        if (MyUtils.isInternetAvailable(this@OtpVerificationActivity)) {
                            getStartTip(OTPCOde.trim({ it <= ' ' }))
                        } else {
                            showSnackbar(resources.getString(R.string.error_common_network)

                            )
                        }

                    }
                    return true

                }
                return false
            }

        })


    }


    private fun customTextViewResendOTP(tvOtpresend: TextView?) {

        val spanTxt: SpannableStringBuilder
        spanTxt = SpannableStringBuilder("Didn't Receive OTP? ")
        spanTxt.append(getString(R.string.ResendOTP))
        spanTxt.setSpan(object : ClickableSpan() {
            override fun onClick(widget: View) {


            }
        }, spanTxt.length - getString(R.string.ResendOTP).length, spanTxt.length, 0)
        spanTxt.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.text_primary)),
            spanTxt.length - getString(R.string.ResendOTP).length,
            spanTxt.length,
            0
        )

        tvOtpresend?.movementMethod = LinkMovementMethod.getInstance()
        tvOtpresend?.setText(spanTxt, TextView.BufferType.SPANNABLE)

    }

    override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
        if (event?.action == KeyEvent.ACTION_DOWN) {
            val id = v?.id
            when (id) {
                R.id.edittext_pin_4 -> if (keyCode == KeyEvent.KEYCODE_DEL) {

                    edittext_pin_3.requestFocus()
                    edittext_pin_4.setText("")

                    return true
                }

                R.id.edittext_pin_3 -> if (keyCode == KeyEvent.KEYCODE_DEL) {
                    //
                    edittext_pin_2.requestFocus()
                    edittext_pin_3.setText("")

                    return true
                }

                R.id.edittext_pin_2 -> if (keyCode == KeyEvent.KEYCODE_DEL) {
                    //
                    edittext_pin_1.requestFocus()
                    edittext_pin_2.setText("")

                    return true
                }

                R.id.edittext_pin_1 -> if (keyCode == KeyEvent.KEYCODE_DEL) {

                    edittext_pin_1.requestFocus()
                    edittext_pin_1.setText("")

                    return true
                }


                else -> return false
            }
        }

        return false
    }

    override fun onBackPressed() {
        MyUtils.showMessageOKCancel(this@OtpVerificationActivity, "Are you sure want to exit ?","Verification Code",
            DialogInterface.OnClickListener { dialogInterface, i ->
                MyUtils.finishActivity(
                    this@OtpVerificationActivity,
                    true
                )
            })
    }


    fun getStartTip(otpCode: String) {
        btnSubmit.startAnimation()
        val df = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

        val datetime = df.format(Calendar.getInstance().time)
        Log.e("date",datetime)

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("driverID", sessionManager.get_Authenticate_User().driverID)
            jsonObject.put("bookingID",bookingId)
            jsonObject.put("bookingOTP", otpCode)
            jsonObject.put("bookingTripStartTime",datetime)
            jsonObject.put("bookingTripStartlatlong",currentLatLong)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)

        val startTiripModel = ViewModelProviders.of(this@OtpVerificationActivity).get(StartTiripModel::class.java)
        startTiripModel.UpdateBookindStatus(this@OtpVerificationActivity, false, jsonArray.toString(),"startTrip").observe(this@OtpVerificationActivity,
            Observer<List<StartTripPojo>> { startTripPojo ->
                if (startTripPojo != null && startTripPojo.isNotEmpty()) {
                    if (startTripPojo[0].status.equals("true", true)) {
                        btnSubmit.revertAnimation()
                        var intent = Intent()
                        intent.putExtra("startTiripData",startTripPojo[0].data[0] as Serializable)
                        setResult(Activity.RESULT_OK, intent)
                        MyUtils.finishActivity(this@OtpVerificationActivity,true)
                    } else {
                        btnSubmit.revertAnimation()

                        showSnackbar(startTripPojo[0].message)

                    }


                } else {
                    btnSubmit.revertAnimation()
                    try {
                        if (!MyUtils.isInternetAvailable(this@OtpVerificationActivity)) {
                            showSnackbar(resources.getString(R.string.error_common_network))
                        } else {
                            showSnackbar(resources.getString(R.string.error_crash_error_message))
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
    })



    }


    fun showSnackbar(message: String) {

        Snackbar.make(ll_mainOtp, message, Snackbar.LENGTH_LONG).show()

    }

}
