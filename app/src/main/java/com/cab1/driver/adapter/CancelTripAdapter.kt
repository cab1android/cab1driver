package com.cab1.driver.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cab1.driver.R
import com.cab1.driver.pojo.CancelTripData
import kotlinx.android.synthetic.main.item_cancel_trip_layout.view.*

class CancelTripAdapter(
    val context: Context, val list: ArrayList<CancelTripData>,
    var onItemClickListener: OnItemClickListener? = null) :
    RecyclerView.Adapter<CancelTripAdapter.ViewHolder>() {

    var mSelection = -1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_cancel_trip_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.reason.text=list[position].reasonReason
        holder.reasonTextView_price.text= "Charges for cancellation Rs."+list[position].reasonCharge
        if (mSelection == position) {
            holder.radioButtonSelection.isChecked=true

        } else {
            holder.radioButtonSelection.isChecked=false

        }


        holder.itemView.setOnClickListener {
            if (mSelection == holder.adapterPosition) {
                mSelection = -1
            } else {
                mSelection = holder.adapterPosition
                if (onItemClickListener != null)
                    onItemClickListener!!.itemClick(holder.adapterPosition,list[holder.adapterPosition].reasonReason,list[holder.adapterPosition].reasonID,list[holder.adapterPosition].reasonChargeWhom,list[holder.adapterPosition].reasonCharge,list[holder.adapterPosition].reasonPayment)

            }
            notifyDataSetChanged()
        }
        holder.radioButtonSelection.setOnClickListener{
            holder.itemView.performClick()
        }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val reason = view.reasonTextView
        val radioButtonSelection = view.radioButtonSelection
        val reasonTextView_price = view.reasonTextView_price
    }

    interface OnItemClickListener {
        fun itemClick(pos: Int, reason: String, reasonID: String, reasonChargeWhom: String, reasonCharge: String,reasonPayment:String)
    }
}
