package com.cab1.driver.adapter

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cab1.driver.R
import com.cab1.driver.api.RestClient
import com.cab1.driver.pojo.CommonStatus
import com.cab1.driver.pojo.NotificationlistData
import com.cab1.driver.util.MyUtils
import com.cab1.driver.viewHolder.LoaderViewHolder
import com.example.admin.myapplication.SessionManager
import kotlinx.android.synthetic.main.item_notification.view.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import java.util.*

class NotificationAdapter(val context: Activity, val onItemClick: OnItemClick,val notificationListData : ArrayList<NotificationlistData?>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == MyUtils.Loder_TYPE) run {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.loader, parent, false)

            return LoaderViewHolder(view)

        } else {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.item_notification, parent, false)
            return NotificationViewHolder(v,context)
        }
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LoaderViewHolder)
        {

        } else if (holder is NotificationViewHolder) {
            val holder1 = holder as NotificationViewHolder


            holder.bind(notificationListData[position], holder1.adapterPosition, onItemClick)
        }
    }

    override fun getItemCount(): Int {
        return notificationListData.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (notificationListData[position] == null) MyUtils.Loder_TYPE else MyUtils.TEXT_TYPE
    }
    class NotificationViewHolder(itemView: View,context: Activity) : RecyclerView.ViewHolder(itemView) {

        fun bind(notificationListData:NotificationlistData?, position: Int, onitemClick: OnItemClick) = with(itemView) {
            tv_notification.text=notificationListData?.notificationMessageText
         /*   if (notificationListData?.notificationReadStatus.equals("No",true)) {
               ll_notification.setBackgroundColor(context.getResources().getColor(R.color.notificationunread))
            } else {
               ll_notification.setBackgroundColor(context.getResources().getColor(R.color.white))
            }*/


        }


    }


    interface OnItemClick {
        fun onClicklisneter(pos: Int, name: String)

    }

}