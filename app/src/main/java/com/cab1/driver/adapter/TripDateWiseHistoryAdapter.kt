package com.cab1.driver.adapter

import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cab1.driver.R
import com.cab1.driver.pojo.StartTripData
import com.cab1.driver.pojo.TripHistoryDetailsData
import com.cab1.driver.util.MyUtils
import kotlinx.android.synthetic.main.item_trip_details_date_history_layout_inflater.view.*
import android.graphics.Paint.UNDERLINE_TEXT_FLAG



class TripDateWiseHistoryAdapter (val context: Context, var data: ArrayList<StartTripData>,var onItemClickListener:OnItemClickListener ) :
    RecyclerView.Adapter<TripDateWiseHistoryAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bind(data[position],position,onItemClickListener)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_trip_details_date_history_layout_inflater,
                parent,
                false
            )
        )
    }

    override fun getItemCount() = data.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(tripHistoryDetailsData: StartTripData,position:Int,onItemClickListener:OnItemClickListener) {
            with(itemView)
            {
                tvFromLocationAddress.text=tripHistoryDetailsData.bookingPickupAddress
                tvdestinationLocationAddress.text=tripHistoryDetailsData.bookingDropAddress
                if(  tvdestinationLocationAddress.text.isEmpty())
                    tvdestinationLocationAddress.text="Unknown"
                tvTripPrice.text ="Rs. "+MyUtils.priceFormat(tripHistoryDetailsData.bookingDriverCommisionAmount)
                tvTripPaymentMode.text=tripHistoryDetailsData.bookingPaymentMode
                tv_BookingId.text=tripHistoryDetailsData.bookingNo
                tv_BookingType.text=tripHistoryDetailsData.bookingType
                tv_BookingId.paintFlags = tv_BookingId.paintFlags or Paint.UNDERLINE_TEXT_FLAG
                tv_BookingId.tag=position
                tv_BookingId.setOnClickListener {
                    onItemClickListener.onItemClick(it.tag as Int,"TripDetail")
                }
                tvTripPrice.tag=tripHistoryDetailsData
                tvTripPrice.setOnClickListener {
                    onItemClickListener.onFareBreakup(it.tag as StartTripData)
                }


            }

        }


    }

    interface OnItemClickListener {
        fun onItemClick(position: Int, typeofOperation: String)
        fun onFareBreakup(startTripData: StartTripData)
    }
}