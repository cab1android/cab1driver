package com.cab1.driver.adapter

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cab1.driver.R
import com.cab1.driver.pojo.TripHistoryData
import com.cab1.driver.util.MyUtils
import com.cab1.driver.viewHolder.LoaderViewHolder
import kotlinx.android.synthetic.main.item_trip_history_layout_inflater.view.*

class TripHistoryAdapter (val context: Context, var onRecyclerItemClickListener: OnItemClickListener, val tripHistoryData:  ArrayList<TripHistoryData?>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == MyUtils.Loder_TYPE) run {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.loader, parent, false)

            return LoaderViewHolder(view)

        } else {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.item_trip_history_layout_inflater, parent, false)
            return TripHistoryViewHolder(v, context as Activity)
        }
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LoaderViewHolder)
        {

        } else if (holder is TripHistoryViewHolder) {

            holder.bind(tripHistoryData[position], holder.adapterPosition, onRecyclerItemClickListener)
            holder.itemView.rootCardView.setOnClickListener {
                if(onRecyclerItemClickListener!=null)
                    onRecyclerItemClickListener.onItemClick(holder.adapterPosition,"")
            }
        }
    }

    override fun getItemCount(): Int {
        return tripHistoryData.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (tripHistoryData[position] == null) MyUtils.Loder_TYPE else MyUtils.TEXT_TYPE
    }
    class TripHistoryViewHolder(itemView: View,context: Activity) : RecyclerView.ViewHolder(itemView) {

        fun bind(tripHistoryData:  TripHistoryData?, position: Int, onitemClick: OnItemClickListener) = with(itemView) {

            tvTripDate.text=MyUtils.formatDate(tripHistoryData?.tripDate!!,"yyyy-MM-dd","EEE, MMM dd yyyy")
            tvTripTotalTime.text=tripHistoryData?.onlineMinutes+" hrs Online"

            try {
                if(tripHistoryData.onlineMinutes?.toInt()!! >=60 )
                    tvTripTotalTime.text = tripHistoryData.onlineMinutes?.toInt()?.div(60).toString()+" hrs Online"
                else
                    tvTripTotalTime.text = tripHistoryData.onlineMinutes+" minute Online"
            } catch (e: Exception) {
            }


            tvTripTotalPriceValue.text="Rs. "+MyUtils.priceFormat(tripHistoryData?.totalAmount.toString())
            try {
                if(tripHistoryData?.totalTrip.toString().toInt()<=1)
                tvNoOfTripValue.text=tripHistoryData?.totalTrip+" Trip"
                else
                    tvNoOfTripValue.text=tripHistoryData?.totalTrip+" Trips"
            } catch (e: Exception) {
            }


        }


    }

    interface OnItemClickListener {
        fun onItemClick(position: Int,typeofOperation: String)
    }
}