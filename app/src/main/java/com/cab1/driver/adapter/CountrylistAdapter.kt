package com.cab1.driver.adapter

import android.app.Activity
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cab1.driver.R
import com.cab1.driver.api.RestClient
import com.cab1.driver.pojo.Data
import kotlinx.android.synthetic.main.item_countrylist.view.*

class CountrylistAdapter(val context: Activity, var countrylist:List<Data>, val onItemClick: OnItemClick) :
    RecyclerView.Adapter<CountrylistAdapter.CounteryViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CounteryViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_countrylist, parent, false)
        return CounteryViewHolder(v)
    }


    override fun onBindViewHolder(holder: CounteryViewHolder, position: Int) {
        holder.bind(countrylist[position],position, onItemClick)
    }

    override fun getItemCount(): Int {
        return countrylist.size
    }


    class CounteryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        fun bind(countrylist: Data, position: Int, onitemClick: OnItemClick) = with(itemView) {
             tv_countryname.text=countrylist.countryName
            img_countrymap.setImageURI(Uri.parse(RestClient.image_countryflag_url +countrylist.countryFlagImage))

            setOnClickListener {
                onitemClick.onClicklisneter(position, countrylist)
            }
        }
    }

    interface OnItemClick {
        fun onClicklisneter(pos: Int, countrylist: Data)

    }

}