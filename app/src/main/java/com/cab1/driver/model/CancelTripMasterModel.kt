package com.cab1.driver.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.driver.api.RestCallback
import com.cab1.driver.api.RestClient
import com.cab1.driver.pojo.CancelTrip
import retrofit2.Call
import retrofit2.Response

class CancelTripMasterModel : ViewModel() {

    lateinit var languageresponse: LiveData<List<CancelTrip>>
    lateinit var mContext: Context
    var isShowing: Boolean = false

    var searchkeyword: String = ""
    var json: String = ""

    fun getCancelTripMaster(
        context: Context,
        isShowing: Boolean,
        json: String
    ): LiveData<List<CancelTrip>> {

        this.mContext = context
        this.isShowing = isShowing;
        this.searchkeyword = searchkeyword
        this.json = json
        languageresponse = getLoginApi()

        return languageresponse;
    }

    private fun getLoginApi(): LiveData<List<CancelTrip>> {
        val data = MutableLiveData<List<CancelTrip>>()



        var call = RestClient.get()!!.cancelTripMaster(json.toString())
        call.enqueue(object:RestCallback<List<CancelTrip>>(mContext) {
            override fun Success(response: Response<List<CancelTrip>>) {
                data.value=response.body()
            }

            override fun failure() {
                data.value=null
            }

        })


        return data
    }




}


