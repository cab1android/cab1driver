package com.cab1.driver.model

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.driver.api.RestCallback
import com.cab1.driver.api.RestClient
import com.cab1.driver.pojo.NotificationlistPojo
import retrofit2.Call
import retrofit2.Response

class NotificationlistModel : ViewModel() {

    lateinit var languageresponse: LiveData<List<NotificationlistPojo>>
    lateinit var mContext: Context

    var isShowing:Boolean = false
    lateinit var pbDialog:Dialog
    var json: String = ""

    var searchkeyword: String = ""

    fun  getNotificationList(
        context: Context,
        isShowing: Boolean,
        json: String): LiveData<List<NotificationlistPojo>> {
        this.json = json

        this.mContext = context
        this.isShowing = isShowing;
        this.searchkeyword = searchkeyword
        languageresponse = getNotificationListApi()

        return languageresponse
    }

    private fun getNotificationListApi(): LiveData<List<NotificationlistPojo>> {
        val data = MutableLiveData<List<NotificationlistPojo>>()

        if (isShowing) {
            showPb()
        }



        var call = RestClient.get()!!.getNotification(json)
        call.enqueue(object : RestCallback<List<NotificationlistPojo>>(mContext) {
            override fun Success(response: Response<List<NotificationlistPojo>>) {
                data.value=response.body()
            }

            override fun failure() {
                data.value=null
            }

        })





        return data
    }

    private fun closePb() {
        pbDialog.dismiss()
    }

    private fun showPb() {
        pbDialog = Dialog(mContext)
        pbDialog.show()

    }


}


