package com.cab1.driver.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.driver.api.RestCallback
import com.cab1.driver.api.RestClient
import com.cab1.driver.pojo.CommonStatus
import retrofit2.Call
import retrofit2.Response

class LogoutModel : ViewModel() {

    lateinit var languageresponse: LiveData<List<CommonStatus>>
    lateinit var mContext: Context
    var isShowing: Boolean = false

    var searchkeyword: String = ""
    var json: String = ""

    fun getLogout(
        context: Context,
        isShowing: Boolean,
        json: String
    ): LiveData<List<CommonStatus>> {

        this.mContext = context
        this.isShowing = isShowing;
        this.searchkeyword = searchkeyword
        this.json = json
        languageresponse = getLogoutApi()

        return languageresponse;
    }

    private fun getLogoutApi(): LiveData<List<CommonStatus>> {
        val data = MutableLiveData<List<CommonStatus>>()



        var call = RestClient.get()!!.getDriverLoout(json.toString())
        call.enqueue(object : RestCallback<List<CommonStatus>>(mContext) {
            override fun Success(response: Response<List<CommonStatus>>) {
                data.value=response.body()
            }

            override fun failure() {
                data.value=null
            }

        })





        return data
    }




}


