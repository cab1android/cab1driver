package com.cab1.driver.model

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.driver.api.RestCallback
import com.cab1.driver.api.RestClient
import com.cab1.driver.pojo.AcceptRequestPojo
import com.cab1.driver.pojo.DriverData
import retrofit2.Call
import retrofit2.Response

class AcceptBookingModel : ViewModel() {


    lateinit var mContext: Context

    var json: String = ""

    fun acceptBooking(
        context: Context,
        json: String
    ): LiveData<List<AcceptRequestPojo>>
    {

        this.mContext = context


        this.json = json


        val data = MutableLiveData<List<AcceptRequestPojo>>()



        var call = RestClient.get()!!.acceptRequest(json)
        call.enqueue(object :RestCallback<List<AcceptRequestPojo>>(mContext) {
            override fun Success(response: Response<List<AcceptRequestPojo>>) {
                data.value=response.body()
            }

            override fun failure() {
             data.value=null
            }

        })



        return data
    }




}


