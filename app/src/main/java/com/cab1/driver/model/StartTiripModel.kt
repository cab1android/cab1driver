package com.cab1.driver.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.driver.api.RestCallback
import com.cab1.driver.api.RestClient
import com.cab1.driver.pojo.StartTripPojo
import retrofit2.Call
import retrofit2.Response

class StartTiripModel : ViewModel() {


    lateinit var mContext: Context
    var isShowing: Boolean = false

    var searchkeyword: String = ""
    var json: String = ""
    var type: String = ""

    fun UpdateBookindStatus(
        context: Context,
        isShowing: Boolean,
        json: String,
        type: String): LiveData<List<StartTripPojo>>
    {



        val data = MutableLiveData<List<StartTripPojo>>()

        var call: Call<List<StartTripPojo>>?=null

        when {
            type.equals("startTrip") -> call = RestClient.get()!!.getStartTrip(json)
            type.equals("endTrip") -> call = RestClient.get()!!.getEndTrip(json)
            type.equals("reach") -> call = RestClient.get()!!.getReachTrip(json)
        }

        call!!.enqueue(object : RestCallback<List<StartTripPojo>> (context){
            override fun Success(response: Response<List<StartTripPojo>>) {

                data.value = response.body()
            }

            override fun failure() {
                data.value = null
            }


        })



        return data
    }




}


