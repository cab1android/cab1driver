package com.cab1.driver.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.driver.api.RestCallback
import com.cab1.driver.api.RestClient
import com.cab1.driver.pojo.CommonStatus
import retrofit2.Call
import retrofit2.Response

class ForgotPasswordModel : ViewModel() {


    lateinit var mContext: Context
    var isShowing: Boolean = false

    var searchkeyword: String = ""
    var json: String = ""
    var type: String = ""

    fun forgotPassword(
        context: Context,
        json: String
      ): LiveData<List<CommonStatus>>
    {



        val data = MutableLiveData<List<CommonStatus>>()

        var call: Call<List<CommonStatus>>?=null

         call = RestClient.get()!!.forgotPassword(json)


        call!!.enqueue(object : RestCallback<List<CommonStatus>> (context){
            override fun Success(response: Response<List<CommonStatus>>) {

                data.value = response.body()
            }

            override fun failure() {
                data.value = null
            }


        })



        return data
    }




}


