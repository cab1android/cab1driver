package com.cab1.driver.model

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.driver.api.RestCallback
import com.cab1.driver.api.RestClient
import com.cab1.driver.pojo.DriverData
import retrofit2.Response

class DutyStatusModel : ViewModel() {

    lateinit var languageresponse: LiveData<List<DriverData>>
    lateinit var mContext: Context
    var isShowing: Boolean = false
    lateinit var pbDialog: Dialog
    var apiName: String = ""
    var json: String = ""

    fun getDutyStatus(
        context: Context,
        json: String,apiName:String
    ): LiveData<List<DriverData>> {

        this.mContext = context
        this.isShowing = isShowing;
        this.apiName = apiName
        this.json = json
        languageresponse = getDutyStatusApi()

        return languageresponse;
    }

    private fun getDutyStatusApi(): LiveData<List<DriverData>> {
        val data = MutableLiveData<List<DriverData>>()

        if (isShowing) {
            showPb()
        }

        var call = RestClient.get()!!.getDutyStatus(json.toString())
      if(apiName.equals("GoToHome",true))
        {
            call=RestClient.get()!!.goToHome(json.toString())
        }
        call.enqueue(object : RestCallback<List<DriverData>>(mContext) {
            override fun Success(response: Response<List<DriverData>>) {
                data.value=response.body()
            }

            override fun failure() {
                data.value=null
            }

        })




        return data
    }

    private fun closePb() {
        pbDialog.dismiss()
    }

    private fun showPb() {
        pbDialog = Dialog(mContext)
        pbDialog.show()

    }


}


