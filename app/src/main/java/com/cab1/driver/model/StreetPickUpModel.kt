package com.cab1.driver.model


import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.driver.R
import com.cab1.driver.api.RestCallback
import com.cab1.driver.api.RestClient
import com.cab1.driver.pojo.CommonStatus
import com.cab1.driver.pojo.GoogleDirection
import com.cab1.driver.pojo.AcceptRequestPojo
import com.cab1.driver.util.Googleutil
import com.google.android.gms.maps.model.LatLng
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class StreetPickUpModel : ViewModel() {



        fun createStreetPickUpTrip(mContext: Context, json: String): LiveData<List<AcceptRequestPojo>>
               {

                   val data = MutableLiveData<List<AcceptRequestPojo>>()

                   var call: Call<List<AcceptRequestPojo>>? = RestClient.get()!!.streetPickUp(json)


                   call!!.enqueue(object : RestCallback<List<AcceptRequestPojo>>(mContext){
                       override fun Success(response: Response<List<AcceptRequestPojo>>) {

                           data.value = response.body()
                       }

                       override fun failure() {
                           data.value = null
                       }


                   })



                   return data

               }



}
