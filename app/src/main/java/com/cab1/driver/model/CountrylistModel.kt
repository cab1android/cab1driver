package com.cab1.driver.model

import android.app.Dialog
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.driver.api.RestCallback
import com.cab1.driver.api.RestClient
import com.cab1.driver.pojo.Countrylist
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response

class CountrylistModel : ViewModel() {

    lateinit var languageresponse: LiveData<List<Countrylist>>
    lateinit var mContext: Context

    var isShowing:Boolean = false
    lateinit var pbDialog:Dialog

    var searchkeyword: String = ""

    fun  getCountry(
        context: Context,
        isShowing: Boolean
    ): LiveData<List<Countrylist>> {

        this.mContext = context
        this.isShowing = isShowing;
        this.searchkeyword = searchkeyword
        languageresponse = getCountryApi()

        return languageresponse;
    }

    private fun getCountryApi(): LiveData<List<Countrylist>> {
        val data = MutableLiveData<List<Countrylist>>()

        if (isShowing) {
            showPb()
        }
        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID","0" )
            jsonObject.put("userType","Driver" )
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)



        var call = RestClient.get()!!.getCountryList(jsonArray.toString())
        call.enqueue(object : RestCallback<List<Countrylist>>(mContext) {
            override fun Success(response: Response<List<Countrylist>>) {
                data.value=response.body()
            }

            override fun failure() {
                data.value=null
            }

        })





        return data
    }

    private fun closePb() {
        pbDialog.dismiss()
    }

    private fun showPb() {
        pbDialog = Dialog(mContext)
        pbDialog.show()

    }


}


