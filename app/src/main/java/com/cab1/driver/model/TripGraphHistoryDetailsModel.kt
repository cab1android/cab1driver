package com.cab1.driver.model


import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.driver.api.RestCallback
import com.cab1.driver.api.RestClient
import com.cab1.driver.pojo.EarningHistoryGraphPojo
import com.cab1.driver.pojo.TripHistoryDetailsPojo

import com.cab1.driver.pojo.TripHistoryPojo

import retrofit2.Response

class TripGraphHistoryDetailsModel : ViewModel() {


    lateinit var mContext: Context


    var json: String = ""



    fun  getTripHistory(
        context: Context,

        json: String): LiveData<List<TripHistoryDetailsPojo>> {
        this.json = json

        this.mContext = context


        val data = MutableLiveData<List<TripHistoryDetailsPojo>>()




        var call = RestClient.get()!!.getTripGraphHistoryDetails(json)
        call.enqueue(object : RestCallback<List<TripHistoryDetailsPojo>>(mContext) {
            override fun Success(response: Response<List<TripHistoryDetailsPojo>>) {
                data.value=response.body()
            }

            override fun failure() {
                data.value=null
            }

        })





        return data
    }



}


