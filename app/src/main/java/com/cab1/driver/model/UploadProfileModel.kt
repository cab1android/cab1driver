package com.cab1.driver.model


import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.driver.api.RestClient
import com.cab1.driver.pojo.DriverData
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Response
import java.io.File


class UploadProfileModel : ViewModel() {

    lateinit var languageresponse: LiveData<List<DriverData>>
    lateinit var mContext: Context
    var isShowing: Boolean = false


    var folderName: String = ""
    var json: String = ""
    var file: File? = null

    fun uploadFile(
        context: Context,
        isShowing: Boolean,
        json: String, folderName: String,  file: File
    ): LiveData<List<DriverData>> {

        this.mContext = context
        this.isShowing = isShowing
        this.folderName = folderName
        this.file = file

        this.json = json
        languageresponse = UploadProfile()

        return languageresponse
    }

    private fun UploadProfile(): LiveData<List<DriverData>> {
        /*if (isShowing)
            MyUtils.showProgressDialog(mContext, "Uploading")*/

        val filePart = MultipartBody.Part.createFormData(
            "FileField",
            file?.name,
          RequestBody.create(okhttp3.MediaType.parse("image*//*"), file)
        )

        val data = MutableLiveData<List<DriverData>>()


        val call = RestClient.get()!!.uploadAttachment(filePart, RequestBody.create(MediaType.parse("text/plain"), folderName), RequestBody.create(MediaType.parse("text/plain"), json))
        call.enqueue(object : retrofit2.Callback<List<DriverData>> {
            override fun onFailure(call: Call<List<DriverData>>, t: Throwable) {
                /*if (isShowing)
                    MyUtils.dismissProgressDialog()*/

                data.value = null
            }

            override fun onResponse(call: Call<List<DriverData>>, response: Response<List<DriverData>>) {
                /*if (isShowing)
                    MyUtils.dismissProgressDialog()*/

                data.value = response.body()


            }
        })



        return data
    }


}


