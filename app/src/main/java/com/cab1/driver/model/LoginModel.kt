package com.cab1.driver.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.driver.api.RestCallback
import com.cab1.driver.api.RestClient
import com.cab1.driver.pojo.DriverData
import retrofit2.Call
import retrofit2.Response

class LoginModel : ViewModel() {

    lateinit var languageresponse: LiveData<List<DriverData>>
    lateinit var mContext: Context
    var isShowing: Boolean = false

    var apiName: String = ""
    var json: String = ""

    fun getLogin(
        context: Context,
        isShowing: Boolean,
        json: String,
        apiName:String
    ): LiveData<List<DriverData>> {

        this.mContext = context
        this.isShowing = isShowing
        this.apiName = apiName
        this.json = json
        languageresponse = getLoginApi()

        return languageresponse;
    }

    private fun getLoginApi(): LiveData<List<DriverData>> {
        val data = MutableLiveData<List<DriverData>>()



        var call = RestClient.get()!!.getLoginDriver(json.toString())
        if(apiName.equals("ChangePassword",true))
        {
            call=RestClient.get()!!.changePassword(json.toString())
        }
        else if(apiName.equals("VersionCheck",true))
        {
            call=RestClient.get()!!.getDriverProfile(json.toString())
        }


        call.enqueue(object : RestCallback<List<DriverData>>(mContext) {
            override fun Success(response: Response<List<DriverData>>) {
                data.value=response.body()
            }

            override fun failure() {
                data.value=null
            }

        })




        return data
    }




}


