package com.cab1.driver.model

import android.app.Dialog
import android.content.Context
import android.location.Location
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.driver.api.RestCallback
import com.cab1.driver.api.RestClient
import com.cab1.driver.pojo.DriverData
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import com.google.android.libraries.places.internal.e
import com.google.firebase.iid.InstanceIdResult
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.iid.FirebaseInstanceId


class UpdateLocationModel : ViewModel() {


    lateinit var mContext: Context
    var isShowing: Boolean = false
    lateinit var pbDialog: Dialog
    var searchkeyword: String = ""
    var driverID: String = ""
    var location: Location? = null
    var locationLogType: String = ""
    var json: String = ""
    var bookingId: String = ""
    var needCurrentTripDetails: Boolean = false

    fun updateCurrentLocation(
        context: Context, driverId: String, bookingId: String, needCurrentTripDetails: Boolean, locationLogType: String,
        location: Location
    ): LiveData<List<DriverData>> {

        this.mContext = context
        this.isShowing = isShowing;
        this.driverID = driverId
        this.location = location
        this.needCurrentTripDetails = needCurrentTripDetails
        this.bookingId = bookingId
        this.locationLogType = locationLogType


        val data = MutableLiveData<List<DriverData>>()
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {

            val newToken = it.token




            val jsonArray = JSONArray()
            val jsonObject = JSONObject()
            try {
                jsonObject.put("driverID", driverID)

                jsonObject.put("driverlogLat", location?.latitude.toString())
                jsonObject.put("driverlogLang", location?.longitude.toString())
                jsonObject.put("driverlogType", locationLogType)

                jsonObject.put("tripID", bookingId)
                jsonObject.put("driverDeviceID", newToken)
                jsonObject.put("needCurrentTripDetails", needCurrentTripDetails)
                val gson = GsonBuilder().disableHtmlEscaping().create()
                val type = object : TypeToken<Location>() {}.type
                val json1 = gson.toJson(location, type)
                val jsonObject1 = JSONObject(json1)
                jsonObject.put("driverSerializeLocation", jsonObject1)
                jsonObject.put("driverDeviceType",  RestClient.apiType)

                jsonObject.put("apiType", RestClient.apiType)
                jsonObject.put("apiVersion", RestClient.apiVersion)


            } catch (e: JSONException) {
                e.printStackTrace()

            }
            jsonArray.put(jsonObject)


            var call = RestClient.get()!!.updateLocationStatus(jsonArray.toString())
            call.enqueue(object : RestCallback<List<DriverData>>(mContext) {
                override fun Success(response: Response<List<DriverData>>) {
                    data.value = response.body()

                }

                override fun failure() {
                    data.value = null
                }

            })


            Log.e("newToken", newToken)
        }

        return data
    }


}


