package com.cab1.driver.model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cab1.driver.api.RestCallback
import com.cab1.driver.api.RestClient
import com.cab1.driver.pojo.DriverData
import retrofit2.Call
import retrofit2.Response

class ProfilPictureModel : ViewModel() {

    lateinit var languageresponse: LiveData<List<DriverData>>
    lateinit var mContext: Context
    var isShowing: Boolean = false

    var searchkeyword: String = ""
    var json: String = ""

    fun ProfilePictureUpdate(
        context: Context,
        isShowing: Boolean,
        json: String
    ): LiveData<List<DriverData>> {

        this.mContext = context
        this.isShowing = isShowing;
        this.searchkeyword = searchkeyword
        this.json = json
        languageresponse = getProfilePictureUpdateApi()

        return languageresponse;
    }

    private fun getProfilePictureUpdateApi(): LiveData<List<DriverData>> {
        val data = MutableLiveData<List<DriverData>>()



        var call = RestClient.get()!!.getProfilePictureUpdate(json.toString())
        call.enqueue(object : RestCallback<List<DriverData>>(mContext) {
            override fun Success(response: Response<List<DriverData>>) {
                data.value=response.body()
            }

            override fun failure() {
                data.value=null
            }

        })





        return data
    }




}


