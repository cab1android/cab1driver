package com.cab1.driver.viewHolder

import android.view.View
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView


/**
 * Created by dhavalkaka on 05/02/2018.
 */

class LoaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var mProgressBar: RelativeLayout? = null
}
