package com.cab1.driver.progressButton.customViews

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import com.cab1.driver.R
import kotlinx.android.synthetic.main.customview_speed_time.view.*
import java.util.concurrent.TimeUnit

class CustomViewSpeedTime(context: Context, attrs: AttributeSet) : LinearLayout(context, attrs) {
    init {
        inflate(context, R.layout.customview_speed_time, this)

        val attributes = context.obtainStyledAttributes(attrs, R.styleable.CustomViewSpeedTime)
        tv_speed.text = attributes.getString(R.styleable.CustomViewSpeedTime_speed)
        tv_time.text = attributes.getString(R.styleable.CustomViewSpeedTime_time)

        attributes.recycle()

    }

    fun setSpeed(speed: Double) {

        tv_speed.text = if (speed > 5) {
            val speed1=speed*3.6
            "" + String.format("%.2f", speed1) + " km/h"
        } else {
            "0.0 km/h"
        }
    }

    fun setTime(millis: Long) {
        tv_time.text = String.format(
            "%02d:%02d:%02d",
            TimeUnit.MILLISECONDS.toHours(millis),
            TimeUnit.MILLISECONDS.toMinutes(millis) -
                    TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), // The change is in this line
            TimeUnit.MILLISECONDS.toSeconds(millis) -
                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))
        )
    }
    fun setTimeLabel(label:String)
    {
        tv_time_title.text=label
    }
}