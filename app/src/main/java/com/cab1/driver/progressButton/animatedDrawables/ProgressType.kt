package  com.cab1.driver.progressButton.animatedDrawables

enum class ProgressType {
    DETERMINATE, INDETERMINATE
}
