package com.cab1.driver


import android.Manifest.permission
import android.app.Activity
import android.app.ActivityManager
import android.app.NotificationManager
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.text.TextUtils
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.animation.TranslateAnimation
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.cab1.cab.driver.util.LocationProvider
import com.cab1.driver.api.RestClient
import com.cab1.driver.api.RestClient.Companion.image_countryflag_url
import com.cab1.driver.application.MyApplication
import com.cab1.driver.model.CountrylistModel
import com.cab1.driver.model.ForgotPasswordModel
import com.cab1.driver.model.LoginModel
import com.cab1.driver.model.LogoutModel
import com.cab1.driver.pojo.*
import com.cab1.driver.util.AutoStart
import com.cab1.driver.util.MyUtils
import com.cab1.driver.util.PolyUtil
import com.example.admin.myapplication.SessionManager
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_splash.*
import kotlinx.android.synthetic.main.already_logged_in_layout.*
import kotlinx.android.synthetic.main.forgot_password_layout.*
import kotlinx.android.synthetic.main.login_driver.*
import kotlinx.android.synthetic.main.no_data_internet.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.util.*


class SplashActivity : AppCompatActivity(), View.OnClickListener {


    private val REQUEST_CODE_Location_PERMISSIONS = 6

    var countryname: String = ""
    var countryID: String = ""
    var countryCode: String = ""
    var countryFlagImage: String = ""

    private var countrylist = ArrayList<Data>()
    lateinit var sessionManager: SessionManager
    var push: Push? = null

    private var locationProvider: LocationProvider? = null
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        sessionManager = SessionManager(this@SplashActivity)
        MyUtils.currentLattitude=0.0
        MyUtils.currentLongitude=0.0

        val notificationManager = MyApplication.instance.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager





        if (intent != null) {


            if (intent.hasExtra("body")) {

                try {
                    push= Push()
                    push?.body=intent.getStringExtra("body")
                    push?.title=intent.getStringExtra("title")
                    push?.msg=intent.getStringExtra("msg")
                    push?.type=intent.getStringExtra("type")
                    if(intent.hasExtra("RefKey"))
                        push?.refKey = intent.getStringExtra("RefKey")
                    if(intent.hasExtra("RefData"))
                        push?.RefData=intent.getStringExtra("RefData")

                    if (push?.type.equals("Logout", true)) {
                        sessionManager!!.clear_login_session()
                    }
                } catch (e: Exception) {
                }
            }
            if(intent.hasExtra("Push"))
                push=intent.getSerializableExtra("Push") as Push
        }



        forgotPassTextView.setOnClickListener(this)
        img_login.setOnClickListener(this)
        btn_alreadylogin_logout.setOnClickListener(this)
        img_countrymap.setOnClickListener(this)
        img_flag_forgot.setOnClickListener(this)
        countrycode.setOnClickListener(this)
        countryCodeTextView.setOnClickListener(this)
        btnRetry.setOnClickListener(this)
        btncancel_action.setOnClickListener(this)
        img_forgot.setOnClickListener(this)

        val currentapiVersion = Build.VERSION.SDK_INT



        if (currentapiVersion >= Build.VERSION_CODES.M) {
            permissionLocation()
        } else {
            getCountryList()
        }



        edit_enterpassword.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if ((actionId == EditorInfo.IME_ACTION_DONE)) {
                    MyUtils.hideKeyboard1(this@SplashActivity)

                    reDirectVerficationScreen()
                    return true

                }
                return false
            }

        })
        edit_forgot_enterphonenumber.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if ((actionId == EditorInfo.IME_ACTION_DONE)) {
                    if (TextUtils.isEmpty(edit_forgot_enterphonenumber.text)) {
                        showSnackBar(
                            resources.getString(R.string.please_enter_phone_no)

                        )
                    } else if (TextUtils.isEmpty(edit_forgot_enterphonenumber.text) || edit_forgot_enterphonenumber.text!!.length != 10 ||  !TextUtils.isDigitsOnly(edit_forgot_enterphonenumber.text!!.toString()))
                    {
                        showSnackBar(

                            resources.getString(R.string.enter_phoneno_min10_digits)

                        )

                    }  else {

                        getForgotPasswordApi()


                    }
                    return true



                }
                return false
            }

        })

    }


//Do not delete any comments in this activity


    private fun runSplash() {

        Handler().postDelayed({


            animation(cardview_login)
//            MyAsyncTask().execute()
            //  MyUtils.startActivity(this,MainActivity::class.java,true)
        }, 50)

    }

    private fun animation(view: View) {
        view.visibility = View.VISIBLE

        val animate = TranslateAnimation(
            0f, // fromXDelta
            0f, // toXDelta
            400f, // fromYDelta
            0f// toYDelta
        )
        animate.duration = 1000
        animate.fillAfter = false
        view.startAnimation(animate)

    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun permissionLocation() {

        if (!addPermission(permission.ACCESS_FINE_LOCATION)) {
            val message = getString(R.string.grant_access_location)

            MyUtils.showMessageOKCancel(this@SplashActivity, message, "Use location service?",
                DialogInterface.OnClickListener { dialog, which ->
                    dialog.dismiss()
                    requestPermissions(
                        arrayOf(permission.ACCESS_FINE_LOCATION,permission.WRITE_EXTERNAL_STORAGE),
                        REQUEST_CODE_Location_PERMISSIONS
                    )
                })

        }else {
            getCountryList()
        }

    }


    private fun addPermission(permission: String): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(permission) !== PackageManager.PERMISSION_GRANTED) {
                return false
            }

        }

        return true
    }


    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_CODE_Location_PERMISSIONS -> {
                if (grantResults.isNotEmpty()) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        getCountryList()
                    } else {
                        Snackbar.make(
                            constraintlayoutMain,
                            R.string.read_location_permissions_senied,
                            Snackbar.LENGTH_LONG
                        ).setAction(
                            "Retry"
                        ) {
                            val i = Intent(
                                android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.parse("package:" + BuildConfig.APPLICATION_ID)
                            )
                            startActivity(i)
                        }.show()

                        showSnackBar(
                            getString(R.string.read_location_permissions_senied)

                        )

                    }
                }
            }

            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.forgotPassTextView -> {
                cardview_login.visibility = View.GONE
                animation(cardviewForgotPassword)
            }
            R.id.img_login -> {
                MyUtils.hideKeyboard1(this@SplashActivity)
                /*  MyUtils.startActivity(this@SplashActivity, OtpVerificationActivity::class.java, true)*/
                reDirectVerficationScreen()
            }
            R.id.btn_alreadylogin_logout -> {
                getDriverLogout()
            }
            R.id.btncancel_action -> {
                alreadyLoggedInCardView.visibility = View.GONE
                animation(cardview_login)
            }
            R.id.img_countrymap -> {
                if (countrylist.size > 1) {
                    var i = Intent(this@SplashActivity, CountryListActivity::class.java)
                    if (countrylist != null) {
                        i.putExtra("countrylist", countrylist)
                    }
                    startActivityForResult(i, 1)
                    overridePendingTransition(
                        R.anim.abc_slide_in_top,
                        R.anim.stay
                    )


                }
            }R.id.img_flag_forgot -> {
            if (countrylist.size > 1) {
                var i = Intent(this@SplashActivity, CountryListActivity::class.java)
                if (countrylist != null) {
                    i.putExtra("countrylist", countrylist)
                }
                startActivityForResult(i, 1)
                overridePendingTransition(
                    R.anim.abc_slide_in_top,
                    R.anim.stay
                )


            }
        }
            R.id.countrycode -> {
                img_countrymap.performClick()
            }R.id.countryCodeTextView -> {
            img_flag_forgot.performClick()
        }
            R.id.btnRetry -> {
                ll_no_data.visibility = View.GONE
                getCountryList()
            }
            R.id.img_forgot -> {
                MyUtils.hideKeyboard1(this@SplashActivity)

                if (TextUtils.isEmpty(edit_forgot_enterphonenumber.text)) {
                    showSnackBar(
                        resources.getString(R.string.please_enter_phone_no)

                    )
                } else if (!TextUtils.isEmpty(edit_forgot_enterphonenumber.text) && edit_forgot_enterphonenumber.text!!.length != 10) {
                    showSnackBar(

                        resources.getString(R.string.enter_phoneno_min10_digits)

                    )

                } else {

                    getForgotPasswordApi()


                }
            }

        }
    }

    private fun reDirectVerficationScreen() {

        if (TextUtils.isEmpty(edit_enterphonenumber.text)) {
            showSnackBar(
                resources.getString(R.string.please_enter_phone_no)

            )
        } else if (TextUtils.isEmpty(edit_enterphonenumber.text) || edit_enterphonenumber.text!!.length < 10 || !TextUtils.isDigitsOnly(edit_enterphonenumber.text.toString())) {
            showSnackBar(

                resources.getString(R.string.enter_phoneno_min10_digits)

            )

        } else if (TextUtils.isEmpty(edit_enterpassword.text)) {
            showSnackBar(

                resources.getString(R.string.please_enter_password)

            )
        } else {
            FirebaseInstanceId.getInstance().instanceId
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        Log.d("deviceToken", it.result!!.token)
                        getDriverLogin(it.result!!.token)
                    } else {
                        errorMethod()
                    }
                }


        }

    }

    private fun getDriverLogin(fcmToken: String)
    {


        try {
            img_login.startAnimation()
        } catch (e: Exception) {
        }

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        var  apiName=""
        if(!sessionManager.isLoggedIn())
        {

            try {
                jsonObject.put("languageID", "2")
                jsonObject.put("driverCountryCode", countryCode)
                jsonObject.put("countryID", countryID)
                jsonObject.put("driverMobile", edit_enterphonenumber.text.toString())
                jsonObject.put("driverDeviceType", "Android")
                jsonObject.put("apiType", RestClient.apiType)
                jsonObject.put("apiVersion", RestClient.apiVersion)
                jsonObject.put("driverDeviceID", fcmToken)
                jsonObject.put("driverPassword", edit_enterpassword.text.toString())
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            apiName="Login"

        }
        else
        {
            try {
                jsonObject.put("languageID", "2")

                jsonObject.put("driverID",sessionManager.get_Authenticate_User().driverID)
                jsonObject.put("driverDeviceType", "Android")
                jsonObject.put("apiType", RestClient.apiType)

                jsonObject.put("apiVersion", RestClient.apiVersion)

            } catch (e: JSONException) {
                e.printStackTrace()
            }
            apiName="VersionCheck"
        }


        jsonArray.put(jsonObject)
        val loginModel = ViewModelProviders.of(this@SplashActivity).get(LoginModel::class.java)
        loginModel.getLogin(this@SplashActivity, false, jsonArray.toString(), apiName).observe(this@SplashActivity,
            Observer<List<DriverData>> { loginPojo ->
                if (loginPojo != null && loginPojo.isNotEmpty())
                {
                    if (loginPojo[0].status.equals("true", true))
                    {


                        if(!loginPojo[0].data[0].apiversion.isNullOrEmpty() && checkVersion(loginPojo[0].data[0].apiversion))
                        {



                            MyUtils.showMessageOKCancel(
                                this@SplashActivity,
                                loginPojo[0].data[0].apiversion[0].apiversionMessage,
                                "",
                                DialogInterface.OnClickListener { dialog, which ->

                                    MyUtils.openPlayStore(this@SplashActivity, loginPojo[0].data[0].apiversion[0].apiversionUrl)

                                },
                                DialogInterface.OnClickListener { dialog, which ->

                                    StoreSessionManager(loginPojo[0].data)
                                    MyUtils.startActivity(this@SplashActivity, MainActivity::class.java, true)

                                }, loginPojo[0].data[0].apiversion[0].apiversionIsForcefully.equals("Yes",true))


                        }
                        else {

                            StoreSessionManager(loginPojo[0].data)
                            MyUtils.startActivity(this@SplashActivity, MainActivity::class.java, true)
                        }
                    } else if (loginPojo[0].code == "2") {
                        cardview_login.visibility = View.GONE
                        animation(alreadyLoggedInCardView)
                        try {
                            if(img_login!=null)
                                img_login.revertAnimation()
                        } catch (e: Exception) {
                        }


                    } else {
                        try {
                            if(img_login!=null)
                                img_login.revertAnimation()
                        } catch (e: Exception) {
                        }
                        showSnackBar(loginPojo[0].message)
                    }


                } else {
                    try {
                        if(img_login!=null)
                            img_login.revertAnimation()
                    } catch (e: Exception) {
                    }
                    errorMethod()
                }
            })
    }

    private fun checkVersion(apiVersion:List<ApiVersionPojo>): Boolean {


        if(!apiVersion[0].apiversionNo.isNullOrEmpty()&&BuildConfig.VERSION_CODE < apiVersion[0].apiversionNo.toInt())
        {
            return true
        }

        return false

    }

    fun errorMethod() {
        try {
            if (!MyUtils.isInternetAvailable(this@SplashActivity)) {
                showSnackBar(resources.getString(R.string.error_common_network))
            } else {
                showSnackBar(resources.getString(R.string.error_crash_error_message))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun StoreSessionManager(driverdata: List<Data1>) {

        val gson = Gson()

        val json = gson.toJson(driverdata[0])

        sessionManager.create_login_session(
            json,
            driverdata[0].driverMobile,
            "",
            true,
            sessionManager.isEmailLogin()
        )

    }


    override fun onDestroy() {
        super.onDestroy()
        try {
            locationProvider?.disconnect()
        } catch (e: Exception) {
        }

    }


    private fun CurrentCityName(lattitude: Double, longitude: Double) {
        val geocoder: Geocoder
        var addresses: List<Address>? = null
        geocoder = Geocoder(this@SplashActivity, Locale.getDefault())

        try {

            addresses = geocoder.getFromLocation(
                lattitude,
                longitude,
                1
            )

            if (addresses != null) {


                countryname = addresses[0].countryName
                Log.e("countryname", countryname)
                getCountryList()
                //runSplash()


            } else {
                getCountryList()
            }

        } catch (e: IOException) {
            e.printStackTrace()
        }

    }
    fun getCurrentLocation()
    {
        locationProvider = LocationProvider(
            this@SplashActivity,
            LocationProvider.HIGH_ACCURACY,
            object : LocationProvider.CurrentLocationCallback {
                override fun handleNewLocation(location: Location) {

                    Log.d("currentLocation", location.toString())

                    locationProvider?.disconnect()
                    MyUtils.currentLattitude=location.latitude
                    MyUtils.currentLongitude=location.longitude
                    getCountryList()



                    // if driver is Online location send to server




                }

            })

        locationProvider!!.connect()
    }


    private fun getCountryList() {


        if(MyUtils.currentLattitude==0.0 || MyUtils.currentLongitude==0.0)
        {
            getCurrentLocation()
        }else {


            if(             !AutoStart().startPowerSaverIntent(this@SplashActivity))
            {

                AutoStart().showDialog(this@SplashActivity, DialogInterface.OnClickListener { dialog, which ->

                    getCurrentLocation()
                })

            }else {


                ll_no_data.visibility = View.VISIBLE
                tNodataTitle.text = "Please wait...."
                btnRetry.visibility = View.GONE


                val countrylistModel = ViewModelProviders.of(this@SplashActivity).get(CountrylistModel::class.java)
                countrylistModel.getCountry(this@SplashActivity, false).observe(this@SplashActivity,
                    Observer<List<Countrylist>> { countrylistPojo ->
                        if (countrylistPojo != null && countrylistPojo.size > 0) {
                            if (countrylistPojo[0].status.equals("true")) {

                                if (!countrylistPojo[0].apiversion.isNullOrEmpty() && checkVersion(countrylistPojo[0].apiversion)) {


                                    MyUtils.showMessageOKCancel(
                                        this@SplashActivity,
                                        countrylistPojo[0].apiversion[0].apiversionMessage,
                                        "",
                                        DialogInterface.OnClickListener { dialog, which ->

                                            MyUtils.openPlayStore(
                                                this@SplashActivity,
                                                countrylistPojo[0].apiversion[0].apiversionUrl
                                            )

                                        },
                                        DialogInterface.OnClickListener { dialog, which ->

                                            openLoginPopUp(countrylistPojo)
                                        }, countrylistPojo[0].apiversion[0].apiversionIsForcefully.equals("Yes", true)
                                    )


                                } else {


                                    openLoginPopUp(countrylistPojo)
                                }


                            } else {
                                ll_no_data.visibility = View.VISIBLE
                                tNodataTitle.text = "No Data Found"
                                btnRetry.visibility = View.GONE
                                showSnackBar(countrylistPojo[0].message)

                            }

                        } else {
                            ll_no_data.visibility = View.VISIBLE

                            try {
                                if (!MyUtils.isInternetAvailable(this@SplashActivity)) {
                                    tNodataTitle.text = "No Internet"
                                    btnRetry.visibility = View.VISIBLE
                                } else {
                                    tNodataTitle.text = "Something went wrong"
                                    btnRetry.visibility = View.VISIBLE
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    })
            }
        }

    }

    private fun openLoginPopUp(countrylistPojo:List<Countrylist>) {
        if (sessionManager.isLoggedIn())
        {

            if(checkIsBackgroundRestricted()){
                showAlert(true)
            }else {
                openMainActivity()
            }
        } else {
            ll_no_data.visibility = View.GONE
            countrylist.clear()
            countrylist.addAll(countrylistPojo[0].data)
            if (countrylist.size == 1)
                spinnerCountryIv.visibility = View.GONE
            spinnerCountryIvfotgot.visibility = View.GONE

            countrycode.text = countrylistPojo[0].data[0].countryDialCode
            countryCodeTextView.text = countrylistPojo[0].data[0].countryDialCode

            img_countrymap.setImageURI(Uri.parse(image_countryflag_url + countrylistPojo[0].data[0].countryFlagImage))
            img_flag_forgot.setImageURI(Uri.parse(image_countryflag_url + countrylistPojo[0].data[0].countryFlagImage))
            countryID = countrylistPojo[0].data[0].countryID

            countryname = countrylistPojo[0].data[0].countryName

            countryCode = countrylistPojo[0].data[0].countryDialCode
            if(checkIsBackgroundRestricted()){
                showAlert(false)
            }else {
                runSplash()
            }
        }
    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {


        when (requestCode) {

            LocationProvider.CONNECTION_FAILURE_RESOLUTION_REQUEST -> {
                getCurrentLocation()
            }

            1 -> {
                if (resultCode == Activity.RESULT_OK) {
                    if (data != null) {
                        if (data.hasExtra("countryID")) {
                            countryID = data.getStringExtra("countryID")
                        }
                        if (data.hasExtra("countryname")) {
                            countryname = data.getStringExtra("countryname")
                        }
                        if (data.hasExtra("countryCode")) {
                            countryCode = data.getStringExtra("countryCode")
                            countrycode.text = countryCode
                            countryCodeTextView.text = countryCode

                        }
                        if (data.hasExtra("countryFlagImage")) {
                            countryFlagImage = data.getStringExtra("countryFlagImage")
                            img_countrymap.setImageURI(Uri.parse(image_countryflag_url + countryFlagImage))
                            img_flag_forgot.setImageURI(Uri.parse(image_countryflag_url + countryFlagImage))

                        }
                    }
                }
            }
            else -> super.onActivityResult(requestCode, resultCode, data)
        }

    }


    fun showSnackBar(message: String) {

        Snackbar.make(constraintlayoutMain, message, Snackbar.LENGTH_LONG).show()

    }


    fun openMainActivity() {
        Handler().postDelayed(

            {

                var intent = Intent(this@SplashActivity, MainActivity::class.java)
                if (push != null)
                    intent.putExtra("Push", push)
                startActivity(intent)
                finish()
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
            }, 50
        )

        // getDriverLogin("")

    }


    private fun getDriverLogout() {
        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("languageID", "2")
            jsonObject.put("driverCountryCode", countryCode)
            jsonObject.put("driverMobile", edit_enterphonenumber.text.toString())
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)

        val logoutModel = ViewModelProviders.of(this@SplashActivity).get(LogoutModel::class.java)
        logoutModel.getLogout(this@SplashActivity, false, jsonArray.toString()).observe(this@SplashActivity,
            Observer<List<CommonStatus>> { loginPojo ->
                if (loginPojo != null && loginPojo.isNotEmpty()) {
                    if (loginPojo[0].status.equals("true", true)) {
                        alreadyLoggedInCardView.visibility = View.GONE
                        animation(cardview_login)
                    } else {
                        showSnackBar(loginPojo[0].message)

                    }


                } else {
                    img_login.revertAnimation()
                    errorMethod()
                }
            })
    }

    override fun onBackPressed() {
        if (cardviewForgotPassword.visibility == View.VISIBLE) {
            cardviewForgotPassword.visibility = View.GONE
            animation(cardview_login)
        } else {
            super.onBackPressed()
        }
    }
    private fun getForgotPasswordApi() {

        img_forgot.startAnimation()

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("driverCountryCode", countryCode)
            jsonObject.put("driverMobile", edit_forgot_enterphonenumber.text.toString().trim())
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)
        val forgotPasswordModel = ViewModelProviders.of(this).get(ForgotPasswordModel::class.java)
        forgotPasswordModel.forgotPassword(this@SplashActivity, jsonArray.toString())
            .observe(this, Observer<List<CommonStatus>> { commonstatusPojo ->
                if (commonstatusPojo != null && commonstatusPojo.size > 0) {
                    if (commonstatusPojo[0].status.equals("true")) {
                        img_forgot.revertAnimation()
                        cardviewForgotPassword.visibility = View.GONE
                        animation(cardview_login)
                        showSnackBar(commonstatusPojo[0].message)

                    } else {

                        img_forgot.revertAnimation()
                        showSnackBar(commonstatusPojo[0].message)


                    }

                } else {

                    img_forgot.revertAnimation()
                    errorMethod()
                }
            })
    }

    //App is background restricted
    fun showAlert( isOpenMainActivity: Boolean)


    {
        val builder = MaterialAlertDialogBuilder(this@SplashActivity)
        builder.setMessage("Message will not come to apps which were put into background restriction by the you(such as via: Setting -> Apps and Notification -> [${getString(R.string.app_name)}] -> Battery)")
        builder.setTitle("Background Restricted Apps")
        builder.setCancelable(false)
        builder.setPositiveButton("Ok" ){ dialog, which ->
            dialog.dismiss()
            var intent =  Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            var uri = Uri.fromParts("package", packageName, null)
            intent.data = uri
            startActivity(intent)

        }

        builder.setNegativeButton("Cancel") { dialog, which -> dialog.dismiss()
            if(isOpenMainActivity)
                openMainActivity()
            else
                runSplash()
        }

        builder.show()


    }
    fun checkIsBackgroundRestricted():Boolean
    {
        var am =  this@SplashActivity.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            am.isBackgroundRestricted
        } else {
            false
        }
    }
}


