package com.cab1.driver


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import androidx.appcompat.app.AppCompatActivity
import com.cab1.driver.adapter.GooglePlaceAutocompleteAdapter
import com.cab1.driver.util.Googleutil
import com.cab1.driver.util.MyUtils
import com.example.admin.myapplication.SessionManager
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.libraries.places.compat.AutocompleteFilter
import com.google.android.libraries.places.compat.Places
import kotlinx.android.synthetic.main.activity_destination_location.*
import kotlinx.android.synthetic.main.toolbar.*


class DestinationLocationActivity : AppCompatActivity() {


    internal var mAdapter: GooglePlaceAutocompleteAdapter? = null


    private lateinit var BOUNDS_INDIA: LatLngBounds

    lateinit var sessionManager: SessionManager

    lateinit var from: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_destination_location)


        sessionManager = SessionManager(this)



        search_locality_edit_text.postDelayed({
            MyUtils.showKeyboardWithFocus(search_locality_edit_text,this)

        },1000)


        if (intent.hasExtra("lats")) {

            var lats = intent.getStringExtra("lats")
            var longs = intent.getStringExtra("longs")

            from = intent.extras.getString("from")


            tvVerifcationTitle.text = resources.getString(R.string.headerdestinationviewtitle)


            BOUNDS_INDIA =
                    LatLngBounds(
                        LatLng(lats.toDouble() - 0.25, longs.toDouble() - 0.25),
                        LatLng(lats.toDouble() + 0.25, longs.toDouble() + 0.25)
                    )
        }


        list_search.visibility = View.VISIBLE






        try {
            initViews()
        } catch (e: Exception) {
            Log.e("Exception", e.toString())
        }
//


        toolbar.setNavigationOnClickListener {

            onBackPressed()

        }


    }


    private fun getCompleteAddressString(
        latitude: LatLng, primaryAddress: String, secondaryAddress: String
    ) {


            var list = Googleutil.getMyLocationAddressList(this@DestinationLocationActivity,latitude.latitude,latitude.longitude)
            if (list != null && list.size > 0) {


                var lats = "" + MyUtils.coordinatesFormat(latitude.latitude)
                var longs = "" + MyUtils.coordinatesFormat(latitude.longitude)

                var intent = Intent()
                intent.putExtra("lattitude", lats)
                intent.putExtra("longitude", longs)
                intent.putExtra("primaryAddress", "$primaryAddress, $secondaryAddress")
                var city= if( !list[0].subAdminArea.isNullOrBlank()){
                list[0].subAdminArea
                }
                else if(!list[0].locality.isNullOrBlank())
                    {
                        list[0].locality
                    }
                else
                {""
                }
                if(!city.isNullOrEmpty()) {
                    intent.putExtra("desCity", city)
                    setResult(Activity.RESULT_OK, intent)
                    MyUtils.finishActivity(this@DestinationLocationActivity, true)
                }
            }

    }

    private fun initViews() {
//        val llm = LinearLayoutManager(this)
//        list_search.layoutManager = llm

        var typeFilter = AutocompleteFilter.Builder()
            .setTypeFilter(AutocompleteFilter.TYPE_FILTER_NONE)
         //   .setCountry("IN")
            .build()
        var mGeoDataClient = com.google.android.libraries.places.compat.Places.getGeoDataClient(this)
        mAdapter = GooglePlaceAutocompleteAdapter(
            this,
            mGeoDataClient, BOUNDS_INDIA, typeFilter
        )
        list_search.adapter = mAdapter
        list_search.setOnItemClickListener { adapterView: AdapterView<*>, view1: View, i: Int, l: Long ->

                mAdapter?.notifyDataSetChanged()

                try {
                    val placeId = (mAdapter!!.getItem(i).placeId)
                    val primaryAddress =
                        (mAdapter!!.getItem(i).getPrimaryText(null).toString().toLowerCase().capitalize())
                    val secondaryAddress =
                        (mAdapter!!.getItem(i).getSecondaryText(null).toString().toLowerCase().capitalize())
                    val placeResult = Places.getGeoDataClient(this).getPlaceById(placeId.toString())
                    placeResult.addOnCompleteListener {

                        if(it!=null && it.isSuccessful && it.result !=null  )

                        getCompleteAddressString(it.result?.get(0)?.latLng!!, primaryAddress, secondaryAddress)
                        else

                            MyUtils.showSnackbar(this, "Try with different location", mainLayoutDestinationLocation)

                    }
                } catch (e: Exception) {
                    Log.e("System out", e.toString())
                }


        }


        search_locality_edit_text.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                //
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (count > 0) {
//                    searchImageViewLocationclear.visibility = View.VISIBLE

                    if (mAdapter != null) {
                        list_search.visibility = View.VISIBLE
//                        text_no_data.visibility = View.GONE
                        list_search.adapter = mAdapter
                        mAdapter?.notifyDataSetChanged()
                    } else {
                        list_search.visibility = View.GONE
//                        text_no_data.visibility = View.VISIBLE
                    }
                } else {


                }
                if (s.toString() != "") {
                    mAdapter!!.filter.filter(s.toString())
                }
            }

            override fun afterTextChanged(s: Editable) {
            }
        })

    }


    override fun onBackPressed() {
//        super.onBackPressed()

        MyUtils.finishActivity(this, true)

    }
    fun showSoftKeyboard(view: View) {
        if (view.requestFocus()) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }
    }
}
