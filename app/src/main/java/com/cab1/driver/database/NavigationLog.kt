package com.cab1.driver.database

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class NavigationLog(

    var bookID: String?,
    var latitude: Double?,
    var longitude: Double?,
    var logDate: Date?

)
{
    @PrimaryKey(autoGenerate = true) var id: Int=0
}