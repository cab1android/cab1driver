package com.cab1.driver.database

import android.content.Context
import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.Room


@Database(entities = arrayOf(NavigationLog::class), version = 1)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun navigationLogDao(): NavigationLogDao
    companion object {

         var INSTANCE: AppDatabase? = null



    fun getDatabase(context: Context): AppDatabase? {
        if (INSTANCE == null) {
            synchronized(AppDatabase::class.java) {
                if (INSTANCE == null) {
                    // Create database here
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java!!, "navigation_Log_database"
                    )
                        .build()
                }
            }
        }
        return INSTANCE
    }
    }
}