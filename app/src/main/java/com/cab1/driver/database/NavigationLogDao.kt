package com.cab1.driver.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface NavigationLogDao
{
    @Query("SELECT * FROM NavigationLog")
    fun getAll(): List<NavigationLog>

    @Query("SELECT * FROM NavigationLog WHERE bookID IN (:bookID)")
    fun loadAllByBookigId(bookID: String): List<NavigationLog>


    @Insert
    fun insert(vararg navigationLog: NavigationLog)

    @Delete
    fun delete(navigationLog: NavigationLog)

    @Query("DELETE  FROM NavigationLog")
    fun deleteAll()
}