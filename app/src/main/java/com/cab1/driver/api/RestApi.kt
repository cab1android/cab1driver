package com.cab1.driver.api


import com.cab1.driver.pojo.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*


public interface RestApi {
    /*@FormUrlEncoded
     @POST("city/get-city-list")
     fun getCityList(@Field("json") json: String): Call<List<CityListPojo>>*/

    @GET
    fun GetDirectionRoute(@Url url: String): Call<GoogleDirection>

    @FormUrlEncoded
    @POST("drivers/driver-login")
    fun getLoginDriver(@Field("json") json: String): Call<List<DriverData>>
    @FormUrlEncoded
    @POST("drivers/driver-change-password")
    fun changePassword(@Field("json") json: String): Call<List<DriverData>>

    @FormUrlEncoded
    @POST("drivers/get-driver-profile")
    fun getDriverProfile(@Field("json") json: String): Call<List<DriverData>>

    @FormUrlEncoded
    @POST("drivers/driver-update-go-to-home")
    fun goToHome(@Field("json") json: String): Call<List<DriverData>>

    @FormUrlEncoded
    @POST("country/get-country-list")
    fun getCountryList(@Field("json") json: String): Call<List<Countrylist>>

    @FormUrlEncoded
    @POST("drivers/driver-update-duty-status")
    fun getDutyStatus(@Field("json") json: String): Call<List<DriverData>>

    @FormUrlEncoded
    @POST("drivers/driver-current-location")
    fun updateLocationStatus(@Field("json") json: String ): Call<List<DriverData>>


    @FormUrlEncoded
    @POST("drivers/driver-logout")
    fun getDriverLoout(@Field("json") json: String): Call<List<CommonStatus>>

    @FormUrlEncoded
    @POST("trips/driver-accept-booking-request")
    fun acceptRequest(@Field("json") json: String): Call<List<AcceptRequestPojo>>

    @Multipart
    @POST("users/file-upload")
     fun uploadAttachment(
        @Part filePart: MultipartBody.Part, @Part("FilePath") filePath: okhttp3.RequestBody, @Part(
            "json"
        ) json: okhttp3.RequestBody
    ): Call<List<DriverData>>

    @FormUrlEncoded
    @POST("drivers/driver-update-profile")
    fun getProfileUpdate(@Field("json") json: String): Call<List<DriverData>>


    @FormUrlEncoded
    @POST("drivers/driver-update-profile-picture")
    fun getProfilePictureUpdate(@Field("json") json: String): Call<List<DriverData>>

    @FormUrlEncoded
    @POST("cancelreason/get-cancel-reason-list")
    fun cancelTripMaster(@Field("json") json: String): Call<List<CancelTrip>>

    @FormUrlEncoded
    @POST("notification/get-notification-list")
    fun getNotification(@Field("json") json: String): Call<List<NotificationlistPojo>>

    @FormUrlEncoded
    @POST("notification/update-notification-read-status")
    fun readUnReadNotification(@Field("json") json: String): Call<List<NotificationlistPojo>>

    @FormUrlEncoded
    @POST("trips/driver-start-trip")
    fun getStartTrip(@Field("json") json: String): Call<List<StartTripPojo>>

    @FormUrlEncoded
    @POST("trips/driver-end-trip")
    fun getEndTrip(@Field("json") json: String): Call<List<StartTripPojo>>

    @FormUrlEncoded
    @POST("trips/driver-reach-trip")
    fun getReachTrip(@Field("json") json: String): Call<List<StartTripPojo>>

    @FormUrlEncoded
    @POST("trips/driver-cancelled-trip")
    fun getCancelTrip(@Field("json") json: String): Call<List<CommonStatus>>

    @FormUrlEncoded
    @POST("drivers/driver-trip-history")
    fun getTripHistory(@Field("json") json: String): Call<List<TripHistoryPojo>>

    @FormUrlEncoded
    @POST("drivers/driver-earnings")
    fun getTripGraphHistory(@Field("json") json: String): Call<List<EarningHistoryGraphPojo>>

    @FormUrlEncoded
    @POST("drivers/driver-earnings-details")
    fun getTripGraphHistoryDetails(@Field("json") json: String): Call<List<TripHistoryDetailsPojo>>
    
     @FormUrlEncoded
    @POST(" drivers/driver-forgot-password")
    fun forgotPassword(@Field("json") json: String): Call<List<CommonStatus>>

    @FormUrlEncoded
    @POST("trips/street-pickup")
    fun streetPickUp(@Field("json") json: String): Call<List<AcceptRequestPojo>>

    @FormUrlEncoded
    @POST("bookingrequest/driver-reject-booking-request")
    fun driverRejectBookingRequest(@Field("json") json: String): Call<List<CommonStatus>>

}
