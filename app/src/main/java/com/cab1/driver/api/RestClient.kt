package com.cab1.driver.api


import com.cab1.driver.BuildConfig
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RestClient {


    companion object {
        val apiType = "Android"
        val apiVersion = BuildConfig.VERSION_CODE
        val buildVersion = BuildConfig.VERSION_NAME

        // Production server url
      /*  var base_url = "http://betaapplication.com/cab1/backend/web/index.php/v2/"
        var image_base_url = "http://betaapplication.com/backend/web/uploads/"*/
        // state server url
        var base_url = "https://cab1.in/backend/web/index.php/v2/"
        var image_base_url = "https://cab1.in/backend/web/uploads/"

        var image_countryflag_url = image_base_url + "flag/"
        var image_driver_url = image_base_url + "drivers/"
        var image_customer_url = image_base_url + "user/"
        var image_category_url = image_base_url + "category/"
        var googleBaseUrl = "https://maps.googleapis.com/maps/api/"


        var base_url_phase1 = base_url

        internal var REST_CLIENT: RestApi? = null
        internal var REST_GOOGLE_CLIENT: RestApi? = null

        private var restAdapter: Retrofit? = null
        private var restGoogleAdapter: Retrofit? = null


        init {
            setupRestClient()
            setupRestGoogleClient()
        }

        fun setOkHttpClientBuilder(): OkHttpClient.Builder {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            val builder = OkHttpClient.Builder()
            builder.addInterceptor(loggingInterceptor)
            builder.connectTimeout(300, TimeUnit.SECONDS)
            builder.readTimeout(80, TimeUnit.SECONDS)
            builder.writeTimeout(90, TimeUnit.SECONDS)
            return builder
        }

        fun setupRestClient() {

            val gson = GsonBuilder().setLenient().create()
            restAdapter = Retrofit.Builder()
                .baseUrl(base_url_phase1)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(
                    setOkHttpClientBuilder()
                        .build()
                )
                .build()
        }

        fun setupRestGoogleClient() {

            val gson = GsonBuilder().setLenient().create()
            restGoogleAdapter = Retrofit.Builder()
                .baseUrl(googleBaseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(
                    setOkHttpClientBuilder()
                        .build()
                )
                .build()
        }


        fun get(): RestApi? {
            if (REST_CLIENT == null) {
                REST_CLIENT = restAdapter!!.create(
                    RestApi::class.java
                )

            }
            return REST_CLIENT
        }

        fun getGoogle(): RestApi? {
            if (REST_GOOGLE_CLIENT == null) {
                REST_GOOGLE_CLIENT = restGoogleAdapter!!.create(
                    RestApi::class.java
                )

            }
            return REST_GOOGLE_CLIENT
        }


    }


}


