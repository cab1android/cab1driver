package com.cab1.driver.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import androidx.localbroadcastmanager.content.LocalBroadcastManager


class NetworkChangeReceiver : BroadcastReceiver()
{
    override fun onReceive(context: Context?, intent: Intent?) {
        try {
            if (isOnline(context!!))
            {
                broadcastToActivity(context,true)

            } else {
                broadcastToActivity(context,false)

            }
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }

    }

    private fun isOnline(context: Context): Boolean {
        try {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val netInfo = cm.activeNetworkInfo
            //should check null because in airplane mode it will be null
            return netInfo != null && netInfo.isConnected
        } catch (e: NullPointerException) {
            e.printStackTrace()
            return false
        }

    }
    fun broadcastToActivity(context: Context, isInternet:Boolean)
    {
        val intentBroadcast = Intent("NetworkChange")
        intentBroadcast.putExtra("isInternet", isInternet)
        LocalBroadcastManager.getInstance(context).sendBroadcast(intentBroadcast)
    }
}