package com.cab1.driver.pojo
import com.google.gson.annotations.SerializedName


data class NotificationlistPojo(
    @SerializedName("data")
    val `data`: List<NotificationlistData>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
)

data class NotificationlistData(
    @SerializedName("notificationID")
    val notificationID: String,
    @SerializedName("notificationMessageText")
    val notificationMessageText: String,
    @SerializedName("notificationReadStatus")
    var notificationReadStatus: String,
    @SerializedName("notificationReferenceKey")
    val notificationReferenceKey: String,
    @SerializedName("notificationSendDate")
    val notificationSendDate: String,
    @SerializedName("notificationSendTime")
    val notificationSendTime: String,
    @SerializedName("notificationTitle")
    val notificationTitle: String,
    @SerializedName("notificationType")
    val notificationType: String
)