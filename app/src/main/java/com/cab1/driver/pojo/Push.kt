package com.cab1.driver.pojo

import com.google.firebase.database.IgnoreExtraProperties
import java.io.Serializable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



/**
 * Created by Chandresh
 * FIL
 */

class Push : Serializable {
    companion object {
        val  action:String="Push"
        val bokingRequest:String="BookingRequest"
    }

    @SerializedName("RefKey")
    @Expose
    var refKey: String? = ""
    @SerializedName("priority")
    @Expose
    var priority: String? = ""
    @SerializedName("msgKey")
    @Expose
    var msgKey: String? = ""
    @SerializedName("msg")
    @Expose
    var msg: String? = ""
    @SerializedName("body")
    @Expose
    var body: String? = ""
    @SerializedName("type")
    @Expose
    var type: String? = ""
    @SerializedName("title")
    @Expose
    var title: String? = ""
    @SerializedName("msgType")
    @Expose
    var msgType: String? = ""

    @SerializedName("RefData")
    @Expose
    var `RefData`: String?=""


}
data class RefData (
    @SerializedName("badge")
    var badge: String="",
    @SerializedName("body")
    var body: String="",
    @SerializedName("bookingDropAddress")
    var bookingDropAddress: String="",
    @SerializedName("bookingDroplatlong")
    var bookingDroplatlong: String="",
    @SerializedName("bookingID")
    var bookingID: String="",
    @SerializedName("bookingOTP")
    var bookingOTP: String="",
    @SerializedName("bookingPickupAddress")
    var bookingPickupAddress: String="",
    @SerializedName("bookingPickuplatlong")
    var bookingPickuplatlong: String="",
    @SerializedName("categoryID")
    var categoryID: String="",
    @SerializedName("categoryImage")
    var categoryImage: String = "",
    @SerializedName("driverCountryCode")
    var driverCountryCode: String="",
    @SerializedName("driverID")
    var driverID: String="",
    @SerializedName("driverMobile")
    var driverMobile: String="",
    @SerializedName("driverName")
    var driverName: String="",
    @SerializedName("driverProfilePic")
    var driverProfilePic: String="",
    @SerializedName("driverRatting")
    var driverRatting: String="",
    @SerializedName("drvcarColor")
    var drvcarColor: String="",
    @SerializedName("drvcarFrontImage")
    var drvcarFrontImage: String="",
    @SerializedName("drvcarModel")
    var drvcarModel: String="",
    @SerializedName("drvcarNo")
    var drvcarNo: String="",
    @SerializedName("msg")
    var msg: String="",
    @SerializedName("msgKey")
    var msgKey: String="",
    @SerializedName("msgType")
    var msgType: String="",
    @SerializedName("priority")
    var priority: String="",
    @SerializedName("sound")
    var sound: String="",
    @SerializedName("title")
    var title: String="",
    @SerializedName("type")
    var type: String="",
    @SerializedName("userID")
    var userID: String="",
    @SerializedName("bookingDate")
    var bookingDate: String="",
    @SerializedName("bookingDistanceActual")
    var bookingDistanceActual: String="0",
    @SerializedName("bookingPaymentMode")
    var bookingPaymentMode: String="",
    @SerializedName("bookingNo")
    var bookingNo: String="",
    @SerializedName("bookingEstimatedAmount")
    var bookingEstimatedAmount: String="0.0",
    @SerializedName("categorySeats")
    var categorySeats: String = "",
    @SerializedName("categoryNoOfBags")
    var categoryNoOfBags: String = "",
    @SerializedName("categoryCarModel")
    var categoryCarModel: String = "",
    @SerializedName("bookingTripStartTime")
    var bookingTripStartTime: String = "",
    @SerializedName("bookingTripEndTime")
    var bookingTripEndTime: String = "",

    @SerializedName("bookingGST")
    var bookingGST: String = "",
    @SerializedName("bookingDurationActual")
    var bookingDurationActual: String = "",
    @SerializedName("Duration")
    var Duration: String = "",
    @SerializedName("bookingWaitCharge")
    var bookingWaitCharge: String = "",
    @SerializedName("optionPerMinCharge")
    var optionPerMinCharge: String = "",
    @SerializedName("optionBasefare")
    var optionBasefare: String = "",
    @SerializedName("categoryName")
    var categoryName: String = "",
    @SerializedName("categoryType")
    var categoryType: String = "",

    @SerializedName("optionAdditionalKmCharge")
    var optionAdditionalKmCharge: String = "",

    @SerializedName("bookingActualAmount")
    var bookingActualAmount: String  = "0.0",
  @SerializedName("bookingDurationEst")
    var bookingDurationEst: String  = "0",

    @SerializedName("bookingType")
    var bookingType: String = "",
    @SerializedName("bookingSubType")
    @Expose
    var bookingSubType: String = "",
    @SerializedName("bookingTravelRoute")
    val bookingTravelRoute: List<Route> = listOf()
): Serializable
