package com.cab1.driver.pojo
import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class TripHistoryDetailsPojo(
    @SerializedName("data")
    var `data`: java.util.ArrayList<StartTripData> = ArrayList(),
    @SerializedName("message")
    var message: String? = "",
    @SerializedName("status")
    var status: String? = ""
):Serializable

data class TripHistoryDetailsData(
    @SerializedName("bookingActualAmount")
    var bookingActualAmount: String? = "0.0",
    @SerializedName("bookingDropAddress")
    var bookingDropAddress: String? = "",
    @SerializedName("bookingDroplatlong")
    var bookingDroplatlong: String? = "",
    @SerializedName("bookingID")
    var bookingID: String? = "",
    @SerializedName("bookingNo")
    var bookingNo: String? = "",
    @SerializedName("bookingPaymentMode")
    var bookingPaymentMode: String? = "",
    @SerializedName("bookingPickupAddress")
    var bookingPickupAddress: String? = "",
    @SerializedName("bookingPickuplatlong")
    var bookingPickuplatlong: String? = "",
    @SerializedName("bookingType")
    var bookingType: String? = ""
):Serializable