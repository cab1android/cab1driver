package com.cab1.driver.pojo

data class UploadFilePojo(
    val fileName: String = "",
    val status: String = ""
)