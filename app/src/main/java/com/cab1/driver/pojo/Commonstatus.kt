package com.cab1.driver.pojo

import com.google.gson.annotations.SerializedName

data class CommonStatus(
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
)