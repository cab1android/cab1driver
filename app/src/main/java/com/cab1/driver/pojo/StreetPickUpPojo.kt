package com.cab1.driver.pojo
import com.google.gson.annotations.SerializedName


data class StreetPickUpPojo(
    @SerializedName("data")
    var `data`: List<StreetPickUpData?>? = listOf(),
    @SerializedName("message")
    var message: String? = "",
    @SerializedName("status")
    var status: String? = ""
)

data class StreetPickUpData(
    @SerializedName("bookingActualAmount")
    var bookingActualAmount: String? = "",
    @SerializedName("bookingDate")
    var bookingDate: String? = "",
    @SerializedName("bookingDistanceActual")
    var bookingDistanceActual: Any? = Any(),
    @SerializedName("bookingDistanceEst")
    var bookingDistanceEst: String? = "",
    @SerializedName("bookingDropAddress")
    var bookingDropAddress: String? = "",
    @SerializedName("bookingDroplatlong")
    var bookingDroplatlong: String? = "",
    @SerializedName("bookingDurationActual")
    var bookingDurationActual: Any? = Any(),
    @SerializedName("bookingDurationEst")
    var bookingDurationEst: String? = "",
    @SerializedName("bookingEstimatedAmount")
    var bookingEstimatedAmount: String? = "",
    @SerializedName("bookingID")
    var bookingID: String? = "",
    @SerializedName("bookingNo")
    var bookingNo: String? = "",
    @SerializedName("bookingOTP")
    var bookingOTP: String? = "",
    @SerializedName("bookingPaymentMode")
    var bookingPaymentMode: String? = "",
    @SerializedName("bookingPickupAddress")
    var bookingPickupAddress: String? = "",
    @SerializedName("bookingPickupTime")
    var bookingPickupTime: String? = "",
    @SerializedName("bookingPickuplatlong")
    var bookingPickuplatlong: String? = "",
    @SerializedName("bookingRatting")
    var bookingRatting: String? = "",
    @SerializedName("bookingReturnTime")
    var bookingReturnTime: String? = "",
    @SerializedName("bookingSubType")
    var bookingSubType: String? = "",
    @SerializedName("bookingTravelRoute")
    var bookingTravelRoute: List<Any?>? = listOf(),
    @SerializedName("bookingTraveledRoute")
    var bookingTraveledRoute: String? = "",
    @SerializedName("bookingTripEndTime")
    var bookingTripEndTime: String? = "",
    @SerializedName("bookingTripEndlatlong")
    var bookingTripEndlatlong: String? = "",
    @SerializedName("bookingTripStartTime")
    var bookingTripStartTime: String? = "",
    @SerializedName("bookingTripStartlatlong")
    var bookingTripStartlatlong: String? = "",
    @SerializedName("bookingType")
    var bookingType: String? = "",
    @SerializedName("bookingWaitCharge")
    var bookingWaitCharge: String? = "",
    @SerializedName("bookingWaitMinute")
    var bookingWaitMinute: String? = "",
    @SerializedName("brandID")
    var brandID: String? = "",
    @SerializedName("brandImage")
    var brandImage: String? = "",
    @SerializedName("brandName")
    var brandName: String? = "",
    @SerializedName("categoryCarModel")
    var categoryCarModel: String? = "",
    @SerializedName("categoryID")
    var categoryID: String? = "",
    @SerializedName("categoryImage")
    var categoryImage: String? = "",
    @SerializedName("categoryName")
    var categoryName: String? = "",
    @SerializedName("categoryNoOfBags")
    var categoryNoOfBags: String? = "",
    @SerializedName("categoryType")
    var categoryType: String? = "",
    @SerializedName("driverCountryCode")
    var driverCountryCode: String? = "",
    @SerializedName("driverID")
    var driverID: String? = "",
    @SerializedName("driverMobile")
    var driverMobile: String? = "",
    @SerializedName("driverName")
    var driverName: String? = "",
    @SerializedName("driverProfilePic")
    var driverProfilePic: String? = "",
    @SerializedName("driverRatting")
    var driverRatting: String? = "",
    @SerializedName("drvcarBackImage")
    var drvcarBackImage: String? = "",
    @SerializedName("drvcarColor")
    var drvcarColor: String? = "",
    @SerializedName("drvcarFrontImage")
    var drvcarFrontImage: String? = "",
    @SerializedName("drvcarID")
    var drvcarID: String? = "",
    @SerializedName("drvcarInteriorImage")
    var drvcarInteriorImage: String? = "",
    @SerializedName("drvcarLeftImage")
    var drvcarLeftImage: String? = "",
    @SerializedName("drvcarModel")
    var drvcarModel: String? = "",
    @SerializedName("drvcarNo")
    var drvcarNo: String? = "",
    @SerializedName("drvcarRightImage")
    var drvcarRightImage: String? = "",
    @SerializedName("drvcarYear")
    var drvcarYear: String? = "",
    @SerializedName("optionAdditionalKmCharge")
    var optionAdditionalKmCharge: String? = "",
    @SerializedName("optionBaseKM")
    var optionBaseKM: String? = "",
    @SerializedName("optionBaseMinute")
    var optionBaseMinute: String? = "",
    @SerializedName("optionBasefare")
    var optionBasefare: String? = "",
    @SerializedName("optionPerMinCharge")
    var optionPerMinCharge: String? = "",
    @SerializedName("statusID")
    var statusID: String? = "",
    @SerializedName("statusName")
    var statusName: String? = "",
    @SerializedName("statusUpdateDate")
    var statusUpdateDate: String? = "",
    @SerializedName("userEmail")
    var userEmail: String? = "",
    @SerializedName("userFullName")
    var userFullName: String? = "",
    @SerializedName("userID")
    var userID: String? = "",
    @SerializedName("userMobile")
    var userMobile: String? = "",
    @SerializedName("userProfilePicture")
    var userProfilePicture: String? = ""
)