package com.cab1.driver.pojo
import com.google.gson.annotations.SerializedName


data class DutyStatus(
    @SerializedName("data")
    val `data`: List<Data>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
)

data class DutyStatusData(
    @SerializedName("driverCountryCode")
    val driverCountryCode: String,
    @SerializedName("driverDeviceID")
    val driverDeviceID: String,
    @SerializedName("driverDeviceType")
    val driverDeviceType: String,
    @SerializedName("driverEmail")
    val driverEmail: String,
    @SerializedName("driverEmailNotification")
    val driverEmailNotification: String,
    @SerializedName("driverID")
    val driverID: String,
    @SerializedName("driverMobile")
    val driverMobile: String,
    @SerializedName("driverName")
    val driverName: String,
    @SerializedName("driverPushNotification")
    val driverPushNotification: String
)