package com.cab1.driver.pojo

import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class StartTripPojo(
    @SerializedName("data")
    val `data`: List<StartTripData> = listOf(),
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
) : Serializable

data class StartTripData(
    @SerializedName("bookingNo")
    var bookingNo: String? = "",
    @SerializedName("bookingActualAmount")
    val bookingActualAmount: String="0.0",
    @SerializedName("bookingDate")
    val bookingDate: String,
    @SerializedName("bookingDistanceActual")
    val bookingDistanceActual: String="0",
    @SerializedName("bookingDistanceEst")
    val bookingDistanceEst: String="0.0",
    @SerializedName("bookingDropAddress")
    val bookingDropAddress: String="Unknown",
    @SerializedName("bookingDroplatlong")
    val bookingDroplatlong: String,
    @SerializedName("bookingDurationActual")
    val bookingDurationActual: String="0.0",
    @SerializedName("bookingDurationEst")
    val bookingDurationEst: String,
    @SerializedName("bookingEstimatedAmount")
    val bookingEstimatedAmount: String="0.0",
    @SerializedName("bookingID")
    val bookingID: String="",
    @SerializedName("bookingOTP")
    val bookingOTP: String,
    @SerializedName("bookingPaymentMode")
    val bookingPaymentMode: String,
    @SerializedName("bookingPickupAddress")
    val bookingPickupAddress: String,
    @SerializedName("bookingPickupTime")
    val bookingPickupTime: String,
    @SerializedName("bookingPickuplatlong")
    val bookingPickuplatlong: String,
    @SerializedName("bookingTravelRoute")
    var bookingTravelRoute: List<Route>?,

    @SerializedName("bookingTraveledRoute")
    val bookingTraveledRoute: String="",
    @SerializedName("bookingTripEndTime")
    val bookingTripEndTime: String,
    @SerializedName("bookingTripStartTime")
    val bookingTripStartTime: String,
    @SerializedName("bookingType")
    val bookingType: String,
    @SerializedName("bookingWaitCharge")
    val bookingWaitCharge: String = "0",
    @SerializedName("bookingWaitMinute")
    val bookingWaitMinute: String = "",
    @SerializedName("brandID")
    val brandID: String,
    @SerializedName("brandImage")
    val brandImage: String,
    @SerializedName("brandName")
    val brandName: String,
    @SerializedName("categoryID")
    val categoryID: String,
    @SerializedName("categoryImage")
    val categoryImage: String,
    @SerializedName("categoryName")
    val categoryName: String,
    @SerializedName("driverCountryCode")
    val driverCountryCode: String,
    @SerializedName("driverID")
    val driverID: String,
    @SerializedName("driverMobile")
    val driverMobile: String,
    @SerializedName("driverName")
    val driverName: String,
    @SerializedName("driverProfilePic")
    val driverProfilePic: String,
    @SerializedName("driverRatting")
    val driverRatting: String,
    @SerializedName("drvcarBackImage")
    val drvcarBackImage: String,
    @SerializedName("drvcarColor")
    val drvcarColor: String,
    @SerializedName("drvcarFrontImage")
    val drvcarFrontImage: String,
    @SerializedName("drvcarID")
    val drvcarID: String,
    @SerializedName("drvcarInteriorImage")
    val drvcarInteriorImage: String,
    @SerializedName("drvcarLeftImage")
    val drvcarLeftImage: String,
    @SerializedName("drvcarModel")
    val drvcarModel: String,
    @SerializedName("drvcarNo")
    val drvcarNo: String,
    @SerializedName("drvcarRightImage")
    val drvcarRightImage: String,
    @SerializedName("drvcarYear")
    val drvcarYear: String,
    @SerializedName("statusID")
    val statusID: String,
    @SerializedName("statusUpdateDate")
    val statusUpdateDate: String,
    @SerializedName("userEmail")
    val userEmail: String,
    @SerializedName("userFullName")
    val userFullName: String,
    @SerializedName("userID")
    val userID: String,
    @SerializedName("userMobile")
    val userMobile: String,
    @SerializedName("userProfilePicture")
    val userProfilePicture: String,

    @SerializedName("bookingTripEndlatlong")
    var bookingTripEndlatlong: String? = "",

    @SerializedName("bookingTripStartlatlong")
    var bookingTripStartlatlong: String? = "",

    @SerializedName("optionAdditionalKmCharge")
    var optionAdditionalKmCharge: String? = "",
    @SerializedName("optionBaseKM")
    var optionBaseKM: String? = "",
    @SerializedName("optionBaseMinute")
    var optionBaseMinute: String? = "0",
    @SerializedName("optionBasefare")
    var optionBasefare: String? = "0.0",
    @SerializedName("optionPerMinCharge")
    var optionPerMinCharge: String? = "0.0",
    @SerializedName("bookingGST")
    var bookingGST: String = "0.0",
    @SerializedName("bookingSubType")
    var bookingSubType: String = "",
    @SerializedName("bookingSubAmount")
var bookingSubAmount: String = "0.0",
    @SerializedName("bookingDiscount")
    var bookingDiscount: String = "0.0",
    @SerializedName("bookingWallet")
    var bookingWallet: String = "0.0",
    @SerializedName("bookingDriverCommisionAmount")
    var bookingDriverCommisionAmount: String = "0.0",
    @SerializedName("couponCode")
    var couponCode: String = ""




    ) : Serializable

