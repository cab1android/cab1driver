package com.cab1.driver.pojo
import com.google.gson.annotations.SerializedName


data class TripHistoryPojo(
    @SerializedName("data")
    var `data`: List<TripHistoryData?> = listOf(),
    @SerializedName("message")
    var message: String? = "",
    @SerializedName("status")
    var status: String? = ""
)

data class TripHistoryData(
    @SerializedName("OnlineMinutes")
    var onlineMinutes: String? = "0",
    @SerializedName("TotalAmount")
    var totalAmount: String? = "0.0",
    @SerializedName("TotalTrip")
    var totalTrip: String? = "",
    @SerializedName("TripDate")
    var tripDate: String? = "",
    var tripHistoryDetailsList:ArrayList<StartTripData> = ArrayList()
)