package com.cab1.driver.pojo
import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class CancelTrip(
    @SerializedName("data")
    val `data`: List<CancelTripData>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
):Serializable

data class CancelTripData(
    @SerializedName("reasonCharge")
    val reasonCharge: String,
    @SerializedName("reasonChargeWhom")
    val reasonChargeWhom: String,
    @SerializedName("reasonID")
    val reasonID: String,
    @SerializedName("reasonPayment")
    val reasonPayment: String,
    @SerializedName("reasonReason")
    val reasonReason: String
):Serializable