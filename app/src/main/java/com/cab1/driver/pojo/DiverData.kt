package com.cab1.driver.pojo
import com.google.gson.annotations.SerializedName
import java.util.ArrayList


data class DriverData(
    @SerializedName("data")
    val `data`: List<Data1>,
    @SerializedName("message")
    val message: String,
    @SerializedName("fileName")
    val fileName: String,
    @SerializedName("status")
    val status: String ,
    @SerializedName("code")
    val code: String="",
    @SerializedName("tripdetails")
val tripdetails: List<StartTripData> = ArrayList()
)

data class Data1(
    @SerializedName("driverCountryCode")
    val driverCountryCode: String,
    @SerializedName("driverDeviceID")
    val driverDeviceID: String,
    @SerializedName("driverDeviceType")
    val driverDeviceType: String,
    @SerializedName("driverEmail")
    val driverEmail: String,
    @SerializedName("driverEmailNotification")
    val driverEmailNotification: String,
    @SerializedName("driverID")
    val driverID: String,
    @SerializedName("driverMobile")
    val driverMobile: String,
    @SerializedName("driverName")
    val driverName: String,
    @SerializedName("driverPushNotification")
    val driverPushNotification: String,
    @SerializedName("driverDutyStatus")
    var driverDutyStatus: String? ="",
    @SerializedName("driverGoToHome")
    var driverGoToHome: String? ="",
    @SerializedName("driverProfilePic")
    var driverProfilePic: String? ="",
    @SerializedName("countryName")
    var countryName: String? ="",
    @SerializedName("countryFlagImage")
    var countryFlagImage: String? ="",
    @SerializedName("franchiseID")
    var franchiseID: String? ="",
    @SerializedName("franchiseFullName")
    var franchiseFullName: String? ="",
    @SerializedName("franchiseEmail")
    var franchiseEmail: String? ="",
    @SerializedName("franchiseMobile")
    var franchiseMobile: String? ="",
    @SerializedName("categoryVehicleType")
    var categoryVehicleType: String? = "Car",


    @SerializedName("drivercars")
    val `drivercars`: List<Drivercars> = listOf(),

    @SerializedName("setting")
    val `setting`: List<Setting> = listOf(),
    @SerializedName("apiversion")
    val apiversion: List<ApiVersionPojo> = listOf()


)

data class Drivercars(
    @SerializedName("drvcarBackImage")
    var drvcarBackImage: String? = "",
    @SerializedName("drvcarColor")
    var drvcarColor: String? = "",
    @SerializedName("drvcarFrontImage")
    var drvcarFrontImage: String? = "",
    @SerializedName("drvcarID")
    var drvcarID: String? = "",
    @SerializedName("drvcarInteriorImage")
    var drvcarInteriorImage: String? = "",
    @SerializedName("drvcarLeftImage")
    var drvcarLeftImage: String? = "",
    @SerializedName("drvcarModel")
    var drvcarModel: String? = "",
    @SerializedName("drvcarNo")
    var drvcarNo: String? = "",
    @SerializedName("drvcarRightImage")
    var drvcarRightImage: String? = "",
    @SerializedName("drvcarYear")
    var drvcarYear: String? = "" ,
    @SerializedName("categoryID")
    var categoryID: String? = ""

)
data class Setting(
    @SerializedName("settingsArrivingDistanceMetre")
    var settingsArrivingDistanceMetre: String? = "",
    @SerializedName("settingsDisplayBookingRequestMinute")
    var settingsDisplayBookingRequestMinute: String? = "",
    @SerializedName("settingsFreeWaitingMinute")
    var settingsFreeWaitingMinute: String? = "",
    @SerializedName("settingsReachDistanceMetre")
    var settingsReachDistanceMetre: String? = "",
    @SerializedName("settingsWaitingPerMinuteCharge")
    var settingsWaitingPerMinuteCharge: String? = ""

)
data class ApiVersionPojo(
    @SerializedName("apiversionDate")
    var apiversionDate: String,
    @SerializedName("apiversionID")
    var apiversionID: String,
    @SerializedName("apiversionIsForcefully")
    var apiversionIsForcefully: String,
    @SerializedName("apiversionMessage")
    var apiversionMessage: String,
    @SerializedName("apiversionNo")
    var apiversionNo: String="0",
    @SerializedName("apiversionUrl")
    var apiversionUrl: String
)



