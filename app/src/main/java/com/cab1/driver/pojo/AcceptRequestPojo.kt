package com.cab1.driver.pojo
import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class AcceptRequestPojo(
    @SerializedName("data")
    var `data`: List<StartTripData?>? = listOf(),
    @SerializedName("message")
    var message: String? = "",
    @SerializedName("status")
    var status: String? = ""
):Serializable

/*
data class AcceptRequestData(
    @SerializedName("bookingActualAmount")
    var bookingActualAmount: String? = "",
    @SerializedName("bookingDate")
    var bookingDate: String? = "",
    @SerializedName("bookingDistanceActual")
    var bookingDistanceActual: Any? = Any(),
    @SerializedName("bookingDistanceEst")
    var bookingDistanceEst: String? = "0.0",
    @SerializedName("bookingDropAddress")
    var bookingDropAddress: String? = "",
    @SerializedName("bookingDroplatlong")
    var bookingDroplatlong: String? = "",
    @SerializedName("bookingDurationActual")
    var bookingDurationActual: Any? = Any(),
    @SerializedName("bookingDurationEst")
    var bookingDurationEst: String? = "",
    @SerializedName("bookingEstimatedAmount")
    var bookingEstimatedAmount: String? = "",
    @SerializedName("bookingID")
    var bookingID: String? = "",
    @SerializedName("bookingPaymentMode")
    var bookingPaymentMode: String? = "",
    @SerializedName("bookingPickupAddress")
    var bookingPickupAddress: String? = "",
    @SerializedName("bookingPickupTime")
    var bookingPickupTime: String? = "",
    @SerializedName("bookingPickuplatlong")
    var bookingPickuplatlong: String? = "",
    @SerializedName("bookingTripEndTime")
    var bookingTripEndTime: String? = "",
    @SerializedName("bookingTripStartTime")
    var bookingTripStartTime: String? = "",
    @SerializedName("bookingType")
    var bookingType: String? = "",
    @SerializedName("bookingWaitCharge")
    var bookingWaitCharge: Any? = Any(),
    @SerializedName("bookingWaitMinute")
    var bookingWaitMinute: Any? = Any(),
    @SerializedName("brandID")
    var brandID: String? = "",
    @SerializedName("brandImage")
    var brandImage: String? = "",
    @SerializedName("brandName")
    var brandName: String? = "",
    @SerializedName("categoryID")
    var categoryID: String? = "",
    @SerializedName("categoryImage")
    var categoryImage: String? = "",
    @SerializedName("categoryName")
    var categoryName: String? = "",
    @SerializedName("driverID")
    var driverID: String? = "",
    @SerializedName("driverMobile")
    var driverMobile: String? = "",
    @SerializedName("driverName")
    var driverName: String? = "",
    @SerializedName("drvcarBackImage")
    var drvcarBackImage: String? = "",
    @SerializedName("drvcarColor")
    var drvcarColor: String? = "",
    @SerializedName("drvcarFrontImage")
    var drvcarFrontImage: String? = "",
    @SerializedName("drvcarID")
    var drvcarID: String? = "",
    @SerializedName("drvcarInteriorImage")
    var drvcarInteriorImage: String? = "",
    @SerializedName("drvcarLeftImage")
    var drvcarLeftImage: String? = "",
    @SerializedName("drvcarModel")
    var drvcarModel: String? = "",
    @SerializedName("drvcarNo")
    var drvcarNo: String? = "",
    @SerializedName("drvcarRightImage")
    var drvcarRightImage: String? = "",
    @SerializedName("drvcarYear")
    var drvcarYear: String? = "",
    @SerializedName("statusID")
    var statusID: String? = "",
    @SerializedName("statusUpdateDate")
    var statusUpdateDate: String? = "",
    @SerializedName("userEmail")
    var userEmail: String? = "",
    @SerializedName("userFullName")
    var userFullName: String? = "",
    @SerializedName("userID")
    var userID: String? = "",
    @SerializedName("userMobile")
    var userMobile: String? = "",
    @SerializedName("userProfilePicture")
    var userProfilePicture: String? = ""
):Serializable*/
