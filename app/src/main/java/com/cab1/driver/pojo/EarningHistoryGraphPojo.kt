package com.cab1.driver.pojo
import com.google.gson.annotations.SerializedName


data class EarningHistoryGraphPojo(
    @SerializedName("data")
    var `data`: ArrayList<EarningHistoryGraphData?>? = ArrayList(),
    @SerializedName("driverTotalEarnings")
    var driverTotalEarnings: String? = "",
    @SerializedName("message")
    var message: String? = "",
    @SerializedName("status")
    var status: String? = ""
)

data class EarningHistoryGraphData(
    @SerializedName("Date")
    var date: String? = "",
    @SerializedName("Day")
    var day: String? = "",
    @SerializedName("OnlineMinutes")
    var onlineMinutes: String? = "0",
    @SerializedName("TotalAmount")
    var totalAmount: String? = "0.0",
    @SerializedName("TotalTrip")
    var totalTrip: String? = ""
)