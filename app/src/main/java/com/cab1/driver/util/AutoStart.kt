package com.cab1.driver.util

import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import androidx.core.content.ContextCompat.startActivity

import android.os.Build
import android.widget.CompoundButton
import androidx.appcompat.widget.AppCompatCheckBox
import android.R.id.edit
import android.app.Activity
import android.content.*
import android.content.Context.MODE_PRIVATE
import android.net.Uri
import android.provider.Settings
import com.cab1.driver.application.MyApplication
import android.provider.Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS
import androidx.appcompat.app.AlertDialog
import com.cab1.driver.BuildConfig
import com.cab1.driver.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import java.util.*


class AutoStart {
    val POWERMANAGER_INTENTS = Arrays.asList(
        Intent().setComponent(
            ComponentName(
                "com.miui.securitycenter",
                "com.miui.permcenter.autostart.AutoStartManagementActivity"
            )
        ),
        Intent().setComponent(
            ComponentName(
                "com.letv.android.letvsafe",
                "com.letv.android.letvsafe.AutobootManageActivity"
            )
        ),
        Intent().setComponent(
            ComponentName(
                "com.huawei.systemmanager",
                "com.huawei.systemmanager.optimize.process.ProtectActivity"
            )
        ),
        Intent().setComponent(
            ComponentName(
                "com.huawei.systemmanager",
                "com.huawei.systemmanager.appcontrol.activity.StartupAppControlActivity"
            )
        ),
        Intent().setComponent(
            ComponentName(
                "com.coloros.oppoguardelf",
                "com.coloros.powermanager.fuelgaue.PowerUsageModelActivity"
            )
        ),
        Intent().setComponent(
            ComponentName(
                "com.coloros.oppoguardelf",
                "com.coloros.powermanager.fuelgaue.PowerSaverModeActivity"
            )
        ),
        Intent().setComponent(
            ComponentName(
                "com.coloros.oppoguardelf",
                "com.coloros.powermanager.fuelgaue.PowerConsumptionActivity"
            )
        ),
        Intent().setComponent(
            ComponentName(
                "com.coloros.safecenter",
                "com.coloros.safecenter.permission.startup.StartupAppListActivity"
            )
        ),
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) Intent().setComponent(
            ComponentName(
                "com.coloros.safecenter",
                "com.coloros.safecenter.startupapp.StartupAppListActivity"
            )
        ).setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS).setData(Uri.parse("package:" + BuildConfig.APPLICATION_ID)) else null,
        Intent().setComponent(
            ComponentName(
                "com.oppo.safe",
                "com.oppo.safe.permission.startup.StartupAppListActivity"
            )
        ),
        Intent().setComponent(
            ComponentName(
                "com.iqoo.secure",
                "com.iqoo.secure.ui.phoneoptimize.AddWhiteListActivity"
            )
        ),
        Intent().setComponent(ComponentName("com.iqoo.secure", "com.iqoo.secure.ui.phoneoptimize.BgStartUpManager")),
        Intent().setComponent(
            ComponentName(
                "com.vivo.permissionmanager",
                "com.vivo.permissionmanager.activity.BgStartUpManagerActivity"
            )
        ),
        Intent().setComponent(ComponentName("com.asus.mobilemanager", "com.asus.mobilemanager.entry.FunctionActivity")),
        Intent().setComponent(
            ComponentName(
                "com.asus.mobilemanager",
                "com.asus.mobilemanager.autostart.AutoStartActivity"
            )
        ),
        Intent().setComponent(
            ComponentName(
                "com.letv.android.letvsafe",
                "com.letv.android.letvsafe.AutobootManageActivity"
            )
        )
            .setData(android.net.Uri.parse("mobilemanager://function/entry/AutoStart")),
        Intent().setComponent(
            ComponentName(
                "com.meizu.safe",
                "com.meizu.safe.security.SHOW_APPSEC"
            )
        ).addCategory(Intent.CATEGORY_DEFAULT).putExtra("packageName", BuildConfig.APPLICATION_ID)
    )

    fun startPowerSaverIntent(context: Activity): Boolean {
        val settings = context.getSharedPreferences("ProtectedApps", Context.MODE_PRIVATE)
        val skipMessage = settings.getBoolean("skipProtectedAppCheck", false)
        if (!skipMessage) {


            if (getIntent(context) == null) {
                val editor = settings.edit()
                editor.putBoolean("skipProtectedAppCheck", true)
                editor.apply()
            }
        }

        return skipMessage

    }

    fun showDialog(context: Activity, onCancelListener: DialogInterface.OnClickListener) {
        val settings = context.getSharedPreferences("ProtectedApps", Context.MODE_PRIVATE)
        val skipMessage = settings.getBoolean("skipProtectedAppCheck", false)
        if (!skipMessage) {
            val editor = settings.edit()
            var intent = getIntent(context)



            if (intent != null) {
                val dontShowAgain = AppCompatCheckBox(context)
                dontShowAgain.text = "Do not show again"
                dontShowAgain.setOnCheckedChangeListener { buttonView, isChecked ->
                    editor.putBoolean("skipProtectedAppCheck", isChecked)
                    editor.apply()
                }

                MaterialAlertDialogBuilder(context)
                    .setTitle("Auto Start Permission")
                    .setMessage(
                        String.format(
                            "%s requires to be enabled  'Auto Start' permission to run app smoothly and getting booking request from customer.%n",
                            context.getString(R.string.app_name)
                        )
                    )
                    //    .setView(dontShowAgain)
                    .setPositiveButton("Go to settings",
                        DialogInterface.OnClickListener { dialog, which ->
                            editor.putBoolean("skipProtectedAppCheck", true)
                            editor.apply()
                            context.startActivity(intent)
                            context.finishAffinity()
                        })
                    .setNegativeButton("Do not show again", DialogInterface.OnClickListener { dialog, which ->

                        editor.putBoolean("skipProtectedAppCheck", true)
                        editor.apply()
                        dialog.dismiss()
                        if (onCancelListener != null)
                            onCancelListener.onClick(dialog, which)
                    })
                    .setCancelable(false)

                    .show()

            } else {

                editor.putBoolean("skipProtectedAppCheck", true)
                editor.apply()

            }


        }
    }

    private fun isCallable(context: Context?, intent: Intent?): Boolean {
        try {
            if (intent == null || context == null) {
                return false
            } else {
                val list = context!!.getPackageManager().queryIntentActivities(
                    intent,
                    PackageManager.MATCH_DEFAULT_ONLY
                )
                return list.size > 0
            }
        } catch (ignored: Exception) {
            return false
        }

    }

    fun openAutoStartIntent(context: Activity) {
        for (intent in POWERMANAGER_INTENTS) {
            if (isCallable(context, intent)) {
                context.startActivity(intent)
            }
        }
    }

    fun getIntent(context: Activity): Intent? {
        var intent1: Intent? = null
        for (intent in POWERMANAGER_INTENTS) {
            if (isCallable(context, intent)) {
                intent1 = intent

                break
            }
        }
        return intent1
    }
}