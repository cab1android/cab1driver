package com.cab1.cab.customer.util

import android.os.Handler
import android.os.SystemClock
import android.view.animation.AccelerateDecelerateInterpolator

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import java.lang.Math.*
import kotlin.random.Random


class MarkerAnimation{

    companion object {


     fun animateMarker(marker: Marker, finalPosition: LatLng) {
        val latLngInterpolator = LatLngInterpolator.Spherical()
        val startPosition = marker.position
        val handler = Handler()
        val start = SystemClock.uptimeMillis()
        val interpolator = AccelerateDecelerateInterpolator()
        val durationInMs =300f

        handler.post(object : Runnable {
            var elapsed: Long = 0

            var t: Float = 0.toFloat()

            var v: Float = 0.toFloat()

            override fun run() {
                // Calculate progress using interpolator
                elapsed = SystemClock.uptimeMillis() - start
                t = elapsed / durationInMs
                v = interpolator.getInterpolation(t)

                marker.position = latLngInterpolator.interpolate(v, startPosition, finalPosition)

                // Repeat till progress is complete.
                if (t < 1) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16)
                }
            }
        })
    }

         fun getLocationInLatLngRad(radiusInMeters: Double, latitude: Double,longitude:Double):LatLng {
            val x0 =longitude
            val y0 = latitude

            val random = Random(32)

            // Convert radius from meters to degrees.
            val radiusInDegrees = radiusInMeters / 111320f

            // Get a random distance and a random angle.
            val u = random.nextDouble()
            val v = random.nextDouble()
            val w = radiusInDegrees * Math.sqrt(u)
            val t = 2.0 * Math.PI * v
            // Get the x and y delta values.
            val x = w * Math.cos(t)
            val y = w * Math.sin(t)

            // Compensate the x value.
            val new_x = x / Math.cos(Math.toRadians(y0))

            val foundLatitude: Double
            val foundLongitude: Double

            foundLatitude = y0 + y
            foundLongitude = x0 + new_x


            return LatLng(foundLatitude,foundLongitude)
        }

    }

    interface LatLngInterpolator {
        fun interpolate(fraction: Float, a: LatLng, b: LatLng): LatLng
        class Spherical : LatLngInterpolator {
            /* From github.com/googlemaps/android-maps-utils */
            override fun interpolate(fraction: Float, from: LatLng, to: LatLng): LatLng {
                // http://en.wikipedia.org/wiki/Slerp
                val fromLat = toRadians(from.latitude)
                val fromLng = toRadians(from.longitude)
                val toLat = toRadians(to.latitude)
                val toLng = toRadians(to.longitude)
                val cosFromLat = cos(fromLat)
                val cosToLat = cos(toLat)
                // Computes Spherical interpolation coefficients.
                val angle = computeAngleBetween(fromLat, fromLng, toLat, toLng)
                val sinAngle = sin(angle)
                if (sinAngle < 1E-6) {
                    return from
                }
                val a = sin((1 - fraction) * angle) / sinAngle
                val b = sin(fraction * angle) / sinAngle
                // Converts from polar to vector and interpolate.
                val x = a * cosFromLat * cos(fromLng) + b * cosToLat * cos(toLng)
                val y = a * cosFromLat * sin(fromLng) + b * cosToLat * sin(toLng)
                val z = a * sin(fromLat) + b * sin(toLat)
                // Converts interpolated vector back to polar.
                val lat = atan2(z, sqrt(x * x + y * y))
                val lng = atan2(y, x)
                return LatLng(toDegrees(lat), toDegrees(lng))
            }

            private fun computeAngleBetween(fromLat: Double, fromLng: Double, toLat: Double, toLng: Double): Double {
                // Haversine's formula
                val dLat = fromLat - toLat
                val dLng = fromLng - toLng
                return 2 * asin(sqrt(pow(sin(dLat / 2), 2.0) + cos(fromLat) * cos(toLat) * pow(sin(dLng / 2), 2.0)))
            }
        }
    }



}