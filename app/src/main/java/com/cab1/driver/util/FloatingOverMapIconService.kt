package com.cab1.driver.util

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.PixelFormat
import android.os.Build
import android.os.IBinder
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.WindowManager
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.cab1.driver.R


class FloatingOverMapIconService : Service() {
    private var windowManager: WindowManager? = null
    private var frameLayout: FrameLayout? = null
 companion object {
     val BROADCAST_ACTION = "com.fil.cab1.driver.MainActivity"
 }

    val NOTIFICATION_CHANNEL_ID_SERVICE = "com.cab1.driver.Service"
    val NOTIFICATION_CHANNEL_ID_INFO = "com.cab1.driver"

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val nm = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            nm.createNotificationChannel(
                NotificationChannel(
                    NOTIFICATION_CHANNEL_ID_SERVICE,
                    "App Service",
                    NotificationManager.IMPORTANCE_DEFAULT
                )
            )
            nm.createNotificationChannel(
                NotificationChannel(
                    NOTIFICATION_CHANNEL_ID_INFO,
                    "Download Info",
                    NotificationManager.IMPORTANCE_DEFAULT
                )
            )
        } else {
            startForeground(1, Notification())
        }// startMyOwnForeground();


        //Inflate the chat head layout we created
        try {
            var flag= WindowManager.LayoutParams.TYPE_PHONE
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
             flag=   WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
            }

            val params = WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                flag,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,

                PixelFormat.TRANSLUCENT
            )
            params.gravity = Gravity.LEFT or Gravity.CENTER_VERTICAL

            windowManager =getSystemService(Context.WINDOW_SERVICE) as WindowManager?
            frameLayout = FrameLayout(this)

            val layoutInflater =getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater?

            // Here is the place where you can inject whatever layout you want in the frame layout
            layoutInflater!!.inflate(R.layout.layout_map_back_widget, frameLayout)

            val backOnMap = frameLayout!!.findViewById(R.id.custom_drawover_back_button) as ImageView
            backOnMap.setOnClickListener {

                if(windowManager!=null&&frameLayout!=null)
                    windowManager?.removeView(frameLayout)

                val intent = Intent(BROADCAST_ACTION)
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
                this@FloatingOverMapIconService.stopSelf()



            }

            windowManager?.addView(frameLayout, params)



        } catch (e: Exception) {
            Log.d("wroker",e.printStackTrace().toString())

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            if (frameLayout != null) windowManager!!.removeView(frameLayout)
        } catch (e: Exception) {
        }
    }
}
