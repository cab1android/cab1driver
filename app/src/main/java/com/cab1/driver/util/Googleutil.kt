package com.cab1.driver.util

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Handler
import android.os.SystemClock
import android.util.Log
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.BounceInterpolator
import android.view.animation.LinearInterpolator
import com.cab1.driver.R
import com.cab1.driver.database.NavigationLog
import com.cab1.driver.pojo.GoogleDirection
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import com.google.gson.Gson
import com.google.maps.android.SphericalUtil
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.StringWriter
import java.util.*
import kotlin.collections.ArrayList


class Googleutil {

    companion object {
        var runnable: Runnable? = null
        var carRunnable: Runnable? = null
        fun getMyLocationAddress(activity: Context, lat: Double, long: Double): String {

            var address = ""
            val geocoder = Geocoder(activity, Locale.getDefault())
            try {
                val addresses = geocoder.getFromLocation(lat, long, 1)
                if (addresses != null) {
                    val strAddress = addresses.get(0)
                    val strbuilderAddress = StringBuilder("")

                    for (i in 0..strAddress.maxAddressLineIndex) {
                        strbuilderAddress.append(strAddress.getAddressLine(i)).append(", ")
                    }
                    address = strbuilderAddress.toString()
                    Log.e("address", address)
                } else {
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }


            return address
        }


        fun drawCircle(mMap: GoogleMap, latLngPickup: LatLng): Polygon {
            var points: ArrayList<LatLng> = ArrayList()
            /*  Log.e("distance", "" + SphericalUtil.computeDistanceBetween(latLngPickup, latLngDestination))
  */
            points.add(LatLng(latLngPickup.latitude - 2, latLngPickup.longitude - 2))
            points.add(LatLng(latLngPickup.latitude - 2, latLngPickup.longitude + 2))
            points.add(LatLng(latLngPickup.latitude + 2, latLngPickup.longitude + 2))
            points.add(LatLng(latLngPickup.latitude + 2, latLngPickup.longitude - 2))


            val hole = ArrayList<LatLng>()
            val p = (360 / 360).toFloat()
            var d = 0f
            var i = 0

            while (i < 360) {
                hole.add(SphericalUtil.computeOffset(latLngPickup, 6000.0, d.toDouble()))

                ++i
                d += p
            }

            return mMap.addPolygon(
                PolygonOptions()
                    .addAll(points)
                    .addHole(hole)
                    .strokeWidth(0f)
                    .fillColor(Color.argb(50, 0, 0, 0))
            )
        }

        fun decodePoly(encoded: String): ArrayList<LatLng> {


            val poly = ArrayList<LatLng>()
            var index = 0
            val len = encoded.length
            var lat = 0
            var lng = 0

            while (index < len) {
                var b: Int
                var shift = 0
                var result = 0
                do {
                    b = encoded[index++].toInt() - 63
                    result = result or (b and 0x1f shl shift)
                    shift += 5
                } while (b >= 0x20)
                val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
                lat += dlat

                shift = 0
                result = 0
                do {
                    b = encoded[index++].toInt() - 63
                    result = result or (b and 0x1f shl shift)
                    shift += 5
                } while (b >= 0x20)
                val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
                lng += dlng

                val p = LatLng(
                    lat.toDouble() / 1E5,
                    lng.toDouble() / 1E5
                )
                poly.add(p)
            }

            return poly

        }

        /** Encodes a sequence of LatLngs into an encoded path string.  */

        fun encode(path: List<NavigationLog>): String {
            var lastLat: Long = 0
            var lastLng: Long = 0

            val result = StringBuilder()

            for (point in path) {
                val lat = Math.round(point.latitude!! * 1e5)
                val lng = Math.round(point.longitude!! * 1e5)

                val dLat = lat - lastLat
                val dLng = lng - lastLng

                encode(dLat, result)
                encode(dLng, result)

                lastLat = lat
                lastLng = lng
            }
            return result.toString()
        }

        private fun encode(v: Long, result: StringBuilder) {
            var v = v
            v = if (v < 0) (v shl 1).inv() else v shl 1
            while (v >= 0x20) {
                var v1 = (0x20 or (v and 0x1f).toInt())
                result.append(Character.toChars(v1 + 63))
                v = v shr 5
            }
            result.append(Character.toChars((v + 63).toInt()))
        }

        /** Encodes an array of LatLngs into an encoded path string.  */
        fun encode(path: Array<NavigationLog>): String {
            return encode(Arrays.asList(*path))
        }


        fun bounceMarker(mMap: GoogleMap, marker: Marker) {

            val handler = Handler()

            val startTime = SystemClock.uptimeMillis()
            val duration: Long = 1500

            val proj = mMap.projection
            val markerLatLng = marker.position
            val startPoint = proj.toScreenLocation(markerLatLng)
            startPoint.offset(0, -100)
            val startLatLng = proj.fromScreenLocation(startPoint)

            val interpolator = BounceInterpolator()
            val elapsed = SystemClock.uptimeMillis() - startTime

            val t = interpolator.getInterpolation(elapsed.toFloat() / duration.toFloat())
            var lng = t * markerLatLng.longitude + (1 - t) * startLatLng.longitude

            val lat = t * markerLatLng.latitude + (1 - t) * startLatLng.latitude

            marker.position = LatLng(lat, lng)
            handler.post(object : Runnable {
                override fun run() {
                    val elapsed = SystemClock.uptimeMillis() - startTime
                    val t = interpolator.getInterpolation(elapsed.toFloat() / duration)
                    val lng = t.toDouble()
                    markerLatLng.longitude + (1 - t)
                    startLatLng.longitude

                    if (t < 1.0) {
                        // Post again 16ms later.
                        handler.postDelayed(this, 16)
                    }
                }
            })

        }

        fun getDemoRoute(context: Context): GoogleDirection {
            val inputStream = context.resources.openRawResource(R.raw.route)
            val writer = StringWriter()
            try {
                val reader = BufferedReader(InputStreamReader(inputStream, "UTF-8"))
                var line: String? = reader.readLine()
                while (line != null) {
                    writer.write(line)
                    line = reader.readLine()
                }
            } catch (e: Exception) {
                Log.e("JsonRead", "Unhandled exception while using JSONResourceReader", e)
            } finally {
                try {
                    inputStream.close()
                } catch (e: Exception) {
                    Log.e("JsonRead", "Unhandled exception while using JSONResourceReader", e)
                }

            }


            val jsonProductsString = writer.toString()
            val gson = Gson()

            return gson.fromJson(jsonProductsString, GoogleDirection::class.java)
        }

        fun getZoomLevel(latLngPickup: LatLng, latLngDestination: LatLng): Int {

            var radius = SphericalUtil.computeDistanceBetween(latLngPickup, latLngDestination) / 2
            val scale = radius / 500
            return (16 - Math.log(scale) / Math.log(2.0)).toInt()
        }


        fun animateMarker(driverMarker: Marker, destination: LatLng) {
            val startPosition = driverMarker.position
            val endPosition = LatLng(destination.latitude, destination.longitude)
            var latLngInterpolator = LatLngInterpolator.LinearFixed()
            val valueAnimator = ValueAnimator.ofFloat(0.0F, 1.0F)
            valueAnimator.duration = 500 // duration 5 seconds
            valueAnimator.interpolator = LinearInterpolator()
            valueAnimator.addUpdateListener { animation ->
                try {
                    val v = animation.animatedFraction
                    val newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition)
                    driverMarker.position = newPosition
                } catch (ex: Exception) {
                }
            }
            valueAnimator.addListener(object : AnimatorListenerAdapter() {
            })
            valueAnimator.start()
        }

        fun bearingBetweenLocations(latLng1: LatLng, latLng2: LatLng): Double {

            val PI = 3.14159
            val lat1 = latLng1.latitude * PI / 180
            val long1 = latLng1.longitude * PI / 180
            val lat2 = latLng2.latitude * PI / 180
            val long2 = latLng2.longitude * PI / 180

            val dLon = long2 - long1

            val y = Math.sin(dLon) * Math.cos(lat2)
            val x = Math.cos(lat1) * Math.sin(lat2) - (Math.sin(lat1)
                    * Math.cos(lat2) * Math.cos(dLon))

            var brng = Math.atan2(y, x)

            brng = Math.toDegrees(brng)
            brng = (brng + 360) % 360

            return brng
        }

        private interface LatLngInterpolator {
            fun interpolate(fraction: Float, a: LatLng, b: LatLng): LatLng
            class LinearFixed : LatLngInterpolator {
                override fun interpolate(fraction: Float, a: LatLng, b: LatLng): LatLng {
                    val lat = (b.latitude - a.latitude) * fraction + a.latitude
                    var lngDelta = b.longitude - a.longitude
                    if (Math.abs(lngDelta) > 180) {
                        lngDelta -= Math.signum(lngDelta) * 360
                    }
                    val lng = lngDelta * fraction + a.longitude
                    return LatLng(lat, lng)
                }
            }
        }

        fun getPolyLineOption(roadColor: Int, points: ArrayList<LatLng>, width: Float = 12f): PolylineOptions {
            var blackPolylineOptions = PolylineOptions()
            blackPolylineOptions.width(width)
            blackPolylineOptions.color(roadColor)
            blackPolylineOptions.startCap(SquareCap())
            blackPolylineOptions.endCap(SquareCap())
            blackPolylineOptions.jointType(JointType.ROUND)
            blackPolylineOptions.addAll(points)
            return blackPolylineOptions
        }

        fun polyLineAnimation(firstPolyline: Polyline, secondPolyLine: Polyline) {
            var polylineAnimator = ValueAnimator.ofFloat(0F, 100.0F)
            polylineAnimator.duration = 10000
            polylineAnimator.interpolator = LinearInterpolator()
            // polylineAnimator.repeatCount =20
            polylineAnimator.addUpdateListener {
                var points = firstPolyline.points
                var percentValue = it.animatedValue
                var size = points?.size
                var newPoints = size!! * (percentValue as Float / 100.0f)
                var p = points?.subList(0, newPoints.toInt())
                secondPolyLine.points = p


            }



            polylineAnimator.addListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animation: Animator?) {

                }

                override fun onAnimationEnd(animation: Animator?) {

                }

                override fun onAnimationCancel(animation: Animator?) {

                }

                override fun onAnimationStart(animation: Animator?) {

                }

            })
            polylineAnimator.start()


        }


        fun moveVechile(myMarker: Marker, finalPosition: LatLng) {

            var startPosition = myMarker.position

            var handler = Handler()
            var start = SystemClock.uptimeMillis()
            var interpolator = AccelerateDecelerateInterpolator()
            var durationInMs = 3000
            var hideMarker = false

            carRunnable = Runnable {
                // Calculate progress using interpolator
                var elapsed = SystemClock.uptimeMillis() - start
                var t = elapsed / durationInMs
                var v = interpolator.getInterpolation(t.toFloat())

                var currentPosition = LatLng(
                    startPosition.latitude * (1 - t) + (finalPosition.latitude) * t,
                    startPosition.longitude * (1 - t) + (finalPosition.longitude) * t
                )
                myMarker.position = currentPosition
                // myMarker.setRotation(finalPosition.getBearing())


                // Repeat till progress is completeelse
                if (t < 1) {
                    // Post again 16ms later.
                    handler.postDelayed({
                        carRunnable
                    }, 16)
                    // handler.postDelayed(this, 100);
                } else {
                    myMarker.isVisible = !hideMarker
                }
            }

            handler.post {

                carRunnable

            }


        }


        fun rotateMarker(marker: Marker, toRotation: Float) {
            var handler = Handler()
            var start = SystemClock.uptimeMillis()
            var startRotation = marker.rotation
            var duration = 1555


            var interpolator = LinearInterpolator()

            runnable = Runnable {
                var elapsed = SystemClock.uptimeMillis() - start
                var t = interpolator.getInterpolation(elapsed.toFloat() / duration)

                var rot = t * toRotation + (1 - t) * startRotation


                marker.rotation = if (-rot > 180) {
                    rot / 2
                } else {
                    rot
                }
                startRotation = if (-rot > 180) rot / 2 else rot
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(runnable, 16)
                }
            }


            handler.post(
                runnable


            )


        }


        fun getMyLocationAddressList(activity: Context, lat: Double, long: Double): List<Address> {

            var address = ""
            val geocoder = Geocoder(activity, Locale.getDefault())

            var addresses: MutableList<Address>? = ArrayList(2)

            try {
                var addresses1 = geocoder.getFromLocation(lat, long, 2)

                if (addresses1 != null && addresses1.size > 0) {
                    for (adr in addresses1) {
                        if (adr.subAdminArea != null && adr.subAdminArea.isNotEmpty()) {
                            addresses!!.add(adr)
                            break
                        }
                        if (adr.locality != null && adr.locality.isNotEmpty()) {

                            addresses!!.add(adr)
                            break
                        }
                    }

                    if (!addresses.isNullOrEmpty() && addresses!![0].postalCode.isNullOrEmpty())
                        addresses!![0].postalCode = ""

                    if (addresses!![0].postalCode.isNullOrEmpty() && addresses!!.size > 1 && !addresses[1]!!.postalCode.isNullOrEmpty())
                        addresses!![0].postalCode = addresses!![1].postalCode

                    if (addresses!![0].adminArea.isNullOrEmpty())
                        addresses!![0].adminArea = ""

                    if (addresses!![0].adminArea.isNullOrEmpty() && addresses!!.size > 1 && !addresses!![1].adminArea.isNullOrEmpty())
                        addresses!![0].adminArea = addresses!![1].adminArea
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }


            return addresses!!
        }

        fun getSpeed(currentLocation: Location, oldLocation: Location?): Double {

            if (oldLocation == null || currentLocation == null)
                return 0.00
            val newLat = currentLocation.latitude
            val newLon = currentLocation.longitude

            val oldLat = oldLocation.latitude
            val oldLon = oldLocation.longitude

            if (currentLocation.hasSpeed()) {
                return currentLocation.speed.toDouble()
            } else {

                val timeDifferent = currentLocation.time - oldLocation.time
                return ((oldLocation.distanceTo(currentLocation) / timeDifferent).toDouble() / 1000)
            }
        }
    }
}