package com.cab1.driver.util

import android.content.Context
import android.widget.TextView
import com.cab1.driver.R
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.utils.MPPointF




private var tvContent: TextView? = null

private val mFormat: java.text.DecimalFormat = java.text.DecimalFormat("###,###,##0.00")
class CustomMarkerView(context: Context?, layoutResource: Int) : MarkerView(context, layoutResource) {
    init {
        tvContent = findViewById<TextView>(R.id.tvContent)
    }


   override fun refreshContent(e: Entry, highlight: Highlight) {

       tvContent?.text = "Rs. "+mFormat.format(e.y)
   }

    override fun getOffset(): MPPointF {
        return MPPointF((-(width / 2)).toFloat(), (-height).toFloat())
    }

}