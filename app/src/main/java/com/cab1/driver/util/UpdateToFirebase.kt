package com.cab1.driver.util

import android.app.Activity
import android.location.Location
import com.cab1.driver.iterfaces.DriverStatusListener
import com.cab1.driver.pojo.Route
import com.cab1.driver.pojo.StartTripData
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.maps.android.SphericalUtil


class UpdateToFirebase(var activity: Activity, var driverStatusListener: DriverStatusListener) {


    var startTripData: StartTripData? = null
    private lateinit var database: DatabaseReference
    var isRouteApiCall: Boolean = false
    var reRouteKM=300
    var isFirebaseEnable:Boolean=false


    var location: Location? = null
        set(value) {
            field = value
            if(isFirebaseEnable) {
                checkReRoute()
                updateLocationFirebase()
            }
        }

    private fun updateLocationFirebase() {
        if (location == null || startTripData == null)
            return

        database = FirebaseDatabase.getInstance().getReference("Trips")
        var key = database.child(startTripData?.bookingID!!).push().key

        if (key != null) {
            database.run {
                child(startTripData?.bookingID!!).child(key).child("latitude").setValue(location?.latitude)
                child(startTripData?.bookingID!!).child(key).child("longitude").setValue(location?.longitude)
                child(startTripData?.bookingID!!).child(key).child("angle").setValue(location?.bearing)

            }
        }


    }

    private fun checkReRoute() {

        if (location == null || startTripData == null || routePoints.isNullOrEmpty())
            return


        var idx = PolyUtil.locationIndexOnEdgeOrPath(
            LatLng(location?.latitude!!, location?.longitude!!),
            routePoints,
            true,
            true,
            100.0
        )
        if (idx == -1)
        {
            var dropLatLong = if (startTripData?.statusID.equals("3")) //Pick Up
                startTripData?.bookingPickuplatlong!!.split(",").toTypedArray()
            else
                startTripData?.bookingDroplatlong!!.split(",").toTypedArray()

            if (!dropLatLong.isNullOrEmpty()&&dropLatLong.size == 2) {
                if (SphericalUtil.computeDistanceBetween(
                        LatLng(dropLatLong[0].toDouble(), dropLatLong[1].toDouble()),
                        LatLng(location?.latitude!!, location?.longitude!!)
                    ) > reRouteKM
                )
                    if (driverStatusListener != null && startTripData != null && !isRouteApiCall)
                        isRouteApiCall = true
                driverStatusListener.onReRoute(
                    startTripData!!,
                    LatLng(dropLatLong[0].toDouble(), dropLatLong[1].toDouble())
                )
            }

            // do your stuff here


        }


    }


    var routePoints: List<LatLng> = ArrayList()
        get() = field
        set(value) {
            field = value
            if (startTripData != null && isFirebaseEnable) {
                database = FirebaseDatabase.getInstance().getReference("Routes")
                var routeType = if (startTripData?.statusID.equals("3")) {
                    "pickupRoute"
                } else {
                    "tripRoute"
                }
                database.child(startTripData?.bookingID!!).child(routeType).setValue(PolyUtil.encode(value))
            }

        }

    fun setPickUpAndTripRoute( points: List<LatLng>)
    {

        if (startTripData != null && isFirebaseEnable) {
routePoints=points
            database.child(startTripData?.bookingID!!).child("pickupRoute").setValue(PolyUtil.encode(points))
            var routes: List<Route> = startTripData?.bookingTravelRoute!!
          var  tripPoints:MutableList<LatLng> =ArrayList()
            if (routes != null && routes.isNotEmpty()) {
                for (it in routes[0]?.legs!!) {
                    it?.steps!!.forEach { step ->

                        tripPoints.addAll(
                            Googleutil.decodePoly(step?.polyline!!.points.toString())
                        )
                    }
                }
            }
            if(!tripPoints.isNullOrEmpty())
            {
                database.child(startTripData?.bookingID!!).child("tripRoute").setValue(PolyUtil.encode(tripPoints))
            }
        }
    }



}