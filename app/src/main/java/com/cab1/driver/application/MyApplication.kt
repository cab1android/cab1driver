package com.cab1.driver.application

import android.app.Application
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager


import android.content.ContentResolver
import android.content.Context
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity

import androidx.appcompat.app.AppCompatDelegate
import androidx.core.app.NotificationCompat
import com.cab1.driver.R
import com.cab1.driver.util.MyUtils
import com.facebook.drawee.backends.pipeline.Fresco






class MyApplication : Application() {

    companion object {
        lateinit var instance: MyApplication
            private set

    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        Fresco.initialize(this)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        createChannel()
    }

    fun createChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            val notificationManager = instance.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            var channelId = MyUtils.channelId
            // Create the NotificationChannel
            val name = instance.getString(R.string.app_name)
            val descriptionText = "Booking Request Customer"
            val importance = NotificationManager.IMPORTANCE_HIGH
            val existingChannel = notificationManager.getNotificationChannel(channelId)

//it will delete existing channel if it exists
            if (existingChannel != null) {
                notificationManager.deleteNotificationChannel(existingChannel.id)
            }


            var soundUri =
                Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + instance.packageName + "/" + R.raw.booking_ring)
            var defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            var mChannel = NotificationChannel(channelId, name, importance)



            mChannel.description = descriptionText
            val audioAttributes = AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .build()
            mChannel.setSound(soundUri, audioAttributes)
            mChannel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
            mChannel.enableVibration(true)

            mChannel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)


            notificationManager.createNotificationChannel(mChannel)

/*
           val notificationCompat = NotificationCompat.Builder(instance, channelId)
                .setSmallIcon(R.drawable.ic_stat_directions_bike)


                .setAutoCancel(true)

                .setSound(soundUri)
                .setTimeoutAfter(0)

                .setPriority(NotificationCompat.PRIORITY_MAX)
            notificationCompat.setDefaults(Notification.DEFAULT_SOUND)
            val notification = notificationCompat.build()

            notificationManager.notify(0, notification)
            notificationManager.cancel(0)*/

        }
    }

}