package com.cab1.driver.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cab1.driver.MainActivity
import com.cab1.driver.R
import com.cab1.driver.adapter.TripHistoryAdapter
import com.cab1.driver.api.RestClient
import com.cab1.driver.model.TripGraphHistoryDetailsModel
import com.cab1.driver.model.TripHistorylistModel
import com.cab1.driver.pojo.StartTripData
import com.cab1.driver.pojo.TripHistoryData
import com.cab1.driver.pojo.TripHistoryDetailsData
import com.cab1.driver.pojo.TripHistoryPojo
import com.cab1.driver.util.MyUtils
import com.example.admin.myapplication.SessionManager
import kotlinx.android.synthetic.main.fragment_notification.*
import kotlinx.android.synthetic.main.nodafound.*
import kotlinx.android.synthetic.main.nointernetconnection.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*


class TripHistoryFragment : Fragment() {

    private  var v: View?=null
    private lateinit var tripHistoryAdapter: TripHistoryAdapter
    private var y: Int = 0
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0
    private var firstVisibleItemPosition: Int = 0
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var isLoading = false
    private var isLastpage = false
    lateinit var sessionManager: SessionManager
    private var tripHistoryList = ArrayList<TripHistoryData?>()
    var pageNo = 0
    lateinit var tripHistoryModel: TripHistorylistModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if(v==null) {
            v = inflater.inflate(R.layout.fragment_notification, container, false)
        }
        return v
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sessionManager = SessionManager(activity as MainActivity)


        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowCustomEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)

        tvVerifcationTitle.text = "Trip History"
        toolbar.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }
        tripHistoryModel =
                ViewModelProviders.of(this@TripHistoryFragment).get(TripHistorylistModel::class.java)

        linearLayoutManager = LinearLayoutManager(activity)
        tripHistoryAdapter =
                TripHistoryAdapter(activity as MainActivity, object : TripHistoryAdapter.OnItemClickListener {
                    override fun onItemClick(position: Int, typeofOperation: String) {

                        if(tripHistoryList[position]!!.tripHistoryDetailsList.isEmpty())
                        getTripHistoryDetails(tripHistoryList[position]!!.tripDate!!,position)
                        else
                            showSheet(tripHistoryList[position]!!.tripHistoryDetailsList,
                                tripHistoryList[position]!!.tripDate!!
                            )


                    }


                }, tripHistoryList)
        notification_RecycleView.layoutManager = linearLayoutManager
        notification_RecycleView.adapter = tripHistoryAdapter
      if(tripHistoryList.size==0)
        getTripHistory()


        notification_RecycleView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                y = dy
                visibleItemCount = linearLayoutManager.getChildCount()
                totalItemCount = linearLayoutManager.getItemCount()
                firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition()
                if (!isLoading && !isLastpage) {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= 10
                    ) {

                        isLoading = true
                        getTripHistory()
                    }
                }
            }
        })

        btnRetry.setOnClickListener {
            getTripHistory()
        }
    }

    private fun getTripHistory() {
        noDatafoundRelativelayout.visibility = View.GONE
        nointernetMainRelativelayout.visibility = View.GONE

        if (pageNo == 0) {
            relativeprogressBar.visibility = View.VISIBLE
            tripHistoryList!!.clear()
            tripHistoryAdapter.notifyDataSetChanged()
        } else {
            relativeprogressBar.visibility = View.GONE
            notification_RecycleView.visibility = (View.VISIBLE)
            tripHistoryList!!.add(null)
            tripHistoryAdapter.notifyItemInserted(tripHistoryList!!.size - 1)
        }
        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {

            jsonObject.put("driverID", sessionManager.get_Authenticate_User().driverID)
            jsonObject.put("page", pageNo)
            jsonObject.put("pagesize", "10")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)

        tripHistoryModel.getTripHistory(activity as MainActivity, jsonArray.toString())
            .observe(this@TripHistoryFragment,
                Observer<List<TripHistoryPojo>>
                { tripHistoryPojo ->
                    if (tripHistoryPojo != null && tripHistoryPojo.isNotEmpty()) {
                        isLoading = false
                        //   remove progress item
                        noDatafoundRelativelayout.visibility = View.GONE
                        nointernetMainRelativelayout.visibility = View.GONE
                        relativeprogressBar.visibility = View.GONE


                        if (pageNo > 0) {
                            tripHistoryList!!.removeAt(tripHistoryList!!.size - 1)
                            tripHistoryAdapter.notifyItemRemoved(tripHistoryList!!.size)
                        }
                        if (tripHistoryPojo[0].status.equals("true")) {

                            sessionManager.NotificationRead = true
                            if (pageNo == 0)
                                tripHistoryList.clear()

                            tripHistoryList.addAll(tripHistoryPojo[0].data)
                            tripHistoryAdapter.notifyDataSetChanged()
                            pageNo += 1
                            if (tripHistoryPojo[0].data!!.size < 10) {
                                isLastpage = true
                            }

                        }
                        relativeprogressBar.visibility = View.GONE

                        if (tripHistoryList!!.size == 0) {
                            noDatafoundRelativelayout.visibility = View.VISIBLE
                            notification_RecycleView.visibility = View.GONE

                        } else {
                            noDatafoundRelativelayout.visibility = View.GONE
                            notification_RecycleView.visibility = View.VISIBLE

                        }

                    } else {


                        if (activity != null) {
                            relativeprogressBar.visibility = View.GONE


                            try {
                                nointernetMainRelativelayout.visibility = View.VISIBLE
                                if (MyUtils.isInternetAvailable(activity!!)) {
                                    nointernetImageview.setImageDrawable(activity!!.getDrawable(R.drawable.ic_warning_black_24dp))
                                    nointernettextview.text = (activity!!.getString(R.string.error_crash_error_message))
                                } else {
                                    nointernetImageview.setImageDrawable(activity!!.getDrawable(R.drawable.ic_signal_wifi_off_black_24dp))

                                    nointernettextview.text = (activity!!.getString(R.string.error_common_network))
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                        }

                    }
                })
    }


    fun getTripHistoryDetails(tripDate: String,position:Int)
    {

        if (activity == null)
            return

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("driverID", SessionManager(activity!!).get_Authenticate_User().driverID)
            jsonObject.put("date", tripDate)

            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)

        MyUtils.showProgressDialog(activity!!, activity!!.getString(R.string.wait))
        var tripGraphHistoryDetailsModel = ViewModelProviders.of(this).get(TripGraphHistoryDetailsModel::class.java)
        tripGraphHistoryDetailsModel?.getTripHistory(activity!!, jsonArray.toString())
            .observe(this@TripHistoryFragment, androidx.lifecycle.Observer {
                MyUtils.dismissProgressDialog()
                if (!it.isNullOrEmpty()) {
                    if (it[0].status.equals("True", true) && !it[0].data.isNullOrEmpty()) {

                        if (activity != null) {
                            tripHistoryList[position]!!.tripHistoryDetailsList.clear()
                            tripHistoryList[position]!!.tripHistoryDetailsList.addAll(it[0].data)
                            showSheet(it[0].data,tripDate)
                        }
                    } else {

                        if (activity != null && activity is MainActivity)
                            (activity as MainActivity).showSnackBar(it[0].message!!)
                    }
                } else {
                    if (activity != null && activity is MainActivity) {
                        (activity as MainActivity).errorMethod()
                    }
                }
            })


    }

    private fun showSheet(data: ArrayList<StartTripData>, tripDate:String) {
        val tripDateWiseHistoryBottomDialogFragment =
            TripDateWiseHistoryBottomSheetFragment()
        var bundle=Bundle()
        bundle.putSerializable("tripData",data)
        bundle.putString("tripDate",tripDate)

        tripDateWiseHistoryBottomDialogFragment.arguments=bundle
        tripDateWiseHistoryBottomDialogFragment.show(
  childFragmentManager,
            "TripDateWiseHistoryBottomDialogFragment"
        )
    }
}
