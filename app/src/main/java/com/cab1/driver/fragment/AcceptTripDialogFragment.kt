package com.cab1.driver.fragment


import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.cab1.driver.MainActivity
import com.cab1.driver.R
import com.cab1.driver.api.RestClient
import com.cab1.driver.iterfaces.DriverStatusListener
import com.cab1.driver.model.AcceptBookingModel
import com.cab1.driver.model.RejectTripModel
import com.cab1.driver.pojo.CommonStatus
import com.cab1.driver.pojo.GoogleDirection
import com.cab1.driver.pojo.RefData
import com.cab1.driver.util.MyUtils
import com.example.admin.myapplication.SessionManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.pickup_request_layout.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.DecimalFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.concurrent.fixedRateTimer





const val PROGRESS_TIME = "progress_time"
const val ROUTE_RESPONSE = "route_response"



class AcceptTripDialogFragment : Fragment(),View.OnClickListener {
    override fun onClick(v: View?) {
       when(v!!.id)
        {
           R.id.btnAcceptTrip -> {
               btnTripDecline.isEnabled=false
               if (progressTask != null) {
                   progressTask!!.cancel()
               }
               acceptBookingRequest()


           }
           R.id.btnTripDecline ->
           {
               MyUtils.showMessageYesNo(activity!!, "Are you sure want reject request?", "Booking Request",
                   DialogInterface.OnClickListener { dialogInterface, i ->
                       dialogInterface.dismiss()
                       btnAcceptTrip.isEnabled=false
                       if (progressTask != null) {
                           progressTask!!.cancel()
                       }
                       rejectRequest()

                   })



           }
        }
    }

    private var mListener: DriverStatusListener? = null
    var progressTask: Timer? = null
    var progressTime=0
    var  customerData= RefData()
    var  directionRouteResponse= GoogleDirection()
    var  actualProgreeTime=0



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.pickup_request_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as MainActivity).bottomNavigation.visibility = View.GONE
        logView()

        if(arguments!=null) {
            customerData= arguments!!.getSerializable("customerData") as RefData
            directionRouteResponse=arguments!!.getSerializable(ROUTE_RESPONSE) as GoogleDirection
        }

        if(directionRouteResponse!=null && ! directionRouteResponse.routes.isNullOrEmpty() && !directionRouteResponse!!.routes!![0]!!.legs.isNullOrEmpty())
        {
            pickUpAddressTv.text=customerData?.bookingPickupAddress
            tv_BookingType.text=customerData?.bookingType
            destinationAddressTv.text = customerData?.bookingDropAddress
            if(destinationAddressTv.text.isEmpty())
                destinationAddressTv.text="Unknown"

            pickUpKmTv.text= String.format("%.1f",(directionRouteResponse!!.routes!![0]!!.legs!![0]!!.distance!!.value.toString().toInt()/1000f) )

            try {
                if(customerData?.bookingType.equals("Rental",true))
                {
                    rentalTimeLayout.visibility=View.VISIBLE
                    timeTv.text="" +customerData?.bookingDurationEst!!.toInt()/60
                    destinationAddressTv.visibility=View.VISIBLE
                    destinationAddressTv.text="Unknown"
                    to_line.visibility=View.GONE
                    tv_to_title.visibility=View.GONE
                    img_dotted_line.visibility=View.GONE
                    img_todestination.visibility=View.GONE

                }else{
                    destinationAddressTv.visibility=View.VISIBLE
                    to_line.visibility=View.VISIBLE
                    tv_to_title.visibility=View.VISIBLE
                    img_dotted_line.visibility=View.VISIBLE
                    img_todestination.visibility=View.VISIBLE
                }
            } catch (e: Exception) {
            }

        }

        btnAcceptTrip.setOnClickListener(this)
        btnTripDecline.setOnClickListener(this)
        progressTime=arguments?.getInt(PROGRESS_TIME)!!
        myTextProgress.text = TimeUnit.SECONDS.toMinutes(progressTime.toLong()).toString()
        actualProgreeTime=progressTime

        progressBar.max = 100
        progressBar.progress = 100
        setProgressBar()
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        val parent = parentFragment
        mListener = if (parent != null) {
            parent as DriverStatusListener
        } else {
            context as DriverStatusListener
        }
    }

    override fun onDetach() {
        mListener = null
        super.onDetach()
    }

   /* interface Listener {
        fun onTripAcceptSuccess(acceptRequestData: AcceptRequestData,   directionRouteResponse: GoogleDirection)
        fun onTripAcceptFailure(position: Int)
    }
*/


    companion object {

        // TODO: Customize parameters
        fun newInstance(progressTimeSecond: Int,pushRefData: RefData,directionRouteResponse:GoogleDirection): AcceptTripDialogFragment =
            AcceptTripDialogFragment().apply {
                arguments = Bundle().apply {
                    putInt(PROGRESS_TIME, progressTimeSecond)
                    putSerializable("customerData", pushRefData)
                    putSerializable(ROUTE_RESPONSE, directionRouteResponse)

                }
            }

    }

    fun setProgressBar() {


        try {

            progressTask = fixedRateTimer("timer", false, 0, 1000) {
                doAsync {

                    uiThread {
                        if (myTextProgress != null && progressBar != null && activity!=null) {


                            if(actualProgreeTime>=60) {
                                myTextProgress.text = formatMinuteSeconds(actualProgreeTime)

                                tvTimeUnit.text = "Min"
                            }
                            else{
                                myTextProgress.text =""+ actualProgreeTime

                                tvTimeUnit.text = "Sec"
                            }
                            actualProgreeTime-=1
                            var progress=actualProgreeTime*100/progressTime
                            progressBar.progress =progress

                            if (actualProgreeTime <= 0) {
                                if (pickupRequestLayout.visibility == View.VISIBLE)

                                progressTask?.cancel()

                                if(mListener!=null)
                                    mListener!!.onTripAcceptFailure(100)



                            }
                        }

                    }
                }


            }



        } catch (e: Exception) {
        }
    }


    private fun formatMinuteSeconds(totalSeconds: Int): String {

        var minutes = Math.ceil((totalSeconds.toFloat() / 60.toFloat()).toDouble())

        return DecimalFormat("#").format(minutes)
    }

    fun acceptBookingRequest()
    {
        logView()

        if(activity==null || customerData==null)
            return

        btnAcceptTrip.startAnimation()

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("driverID", SessionManager(activity!!).get_Authenticate_User().driverID)
            jsonObject.put("bookingID", customerData.bookingID)

            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)

        Log.d("acceptBooking_req",jsonArray.toString())
        logView()


      var  acceptBookingModel = ViewModelProviders.of(this).get(AcceptBookingModel::class.java)
        acceptBookingModel?.acceptBooking(activity!!,  jsonArray.toString()).observe(this@AcceptTripDialogFragment,androidx.lifecycle.Observer {

            if(!it.isNullOrEmpty())
            {
                if(it[0].status.equals("True",true) && !it[0].data.isNullOrEmpty())
                {

                    Log.d("acceptBooking_Data",""+it[0].data)
                    logView()
                    if(mListener!=null)
                        mListener!!.onTripAcceptSuccess(it!![0]!!.data!![0]!!,directionRouteResponse)

                }
                else
                {

                    if(mListener!=null)
                        mListener!!.onTripAcceptFailure(100)
                    if(activity!=null && activity is MainActivity)
                    (activity as MainActivity).showSnackBar(it[0].message!!)
                }
            }
            else
            {


                if(activity!=null && activity is MainActivity)
                {
                    btnAcceptTrip.revertAnimation {
                        btnAcceptTrip.text=activity!!.getString(R.string.accept)
                    }
                    btnTripDecline.isEnabled=true

                    (activity as MainActivity).errorMethod()
                }
            }
        })


    }

    private fun rejectRequest()
    {
        if(activity==null || customerData==null)
            return

        btnTripDecline.startAnimation()

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("driverID", SessionManager(activity!!).get_Authenticate_User().driverID)
            jsonObject.put("bookingID", customerData.bookingID)

            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)


        val rejectTripModel = ViewModelProviders.of(this@AcceptTripDialogFragment).get(RejectTripModel::class.java)
        rejectTripModel.rejectTrip(activity as MainActivity, false, jsonArray.toString()).observe(this@AcceptTripDialogFragment,
            Observer<List<CommonStatus>> { reponse ->
                if (reponse != null && reponse.isNotEmpty()) {
                    if(mListener!=null)
                        mListener!!.onTripAcceptFailure(100)

                } else {

                    if(activity!=null) {
                        btnTripDecline.revertAnimation()
                        btnAcceptTrip.isEnabled = true
                        (activity as MainActivity).errorMethod()
                    }
                }
            })
    }

    fun logView()
    {
        if(activity!=null && activity!! is MainActivity)
        {
            (activity as MainActivity).create_logcat()

        }
    }
}


