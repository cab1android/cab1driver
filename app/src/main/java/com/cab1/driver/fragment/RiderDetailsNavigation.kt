package com.cab1.driver.fragment

import android.Manifest
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.cab1.driver.MainActivity
import com.cab1.driver.R
import com.cab1.driver.api.RestClient
import com.cab1.driver.iterfaces.DriverStatusListener

import com.cab1.driver.pojo.GoogleDirection
import com.cab1.driver.pojo.StartTripData
import com.cab1.driver.util.MyUtils
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.rider_details_nav_layout.*
import kotlinx.android.synthetic.main.rider_details_status_common_layout.*
import java.text.SimpleDateFormat
import java.util.*


class RiderDetailsNavigation : Fragment(), View.OnClickListener {

    private var mListener: DriverStatusListener? = null
    private val MY_PERMISSIONS_REQUEST_CALL_PHONE = 501
    var phoneNumber = ""
    val ACCEPT_REQUEST_DATA = "accepetRequestData"
    var accepetRequestData: StartTripData? = null
    private var snakview: View? = null
    val ROUTE_RESPONSE = "route_response"
    var directionRouteResponse = GoogleDirection()

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ll_start_navigation -> {
                try {
                    if (mListener != null) {
                        val pickLatLong =accepetRequestData!!.bookingPickuplatlong!!.split(",").toTypedArray()
                        if(pickLatLong.size==2){
                        mListener!!.onRidePickUpStartNavigation(LatLng(pickLatLong[0].toDouble()!!,pickLatLong[1].toDouble()))
                        }
                        else{
                            try {
                                Toast.makeText(activity,getString(R.string.error_crash_error_message), Toast.LENGTH_LONG).show()
                            } catch (e: Exception) {
                            }
                        }
                    }
                } catch (e: Exception) {
                }
            }
            R.id.ll_cancelRide -> {

                if (mListener != null)
                    mListener!!.onRideCancel()

            }
            R.id.ll_reach_ride -> {
                if (mListener != null && accepetRequestData!=null)
                    mListener!!.onRideReach(accepetRequestData!!)
            }
            R.id.btnCallRider -> {
                call(phoneNumber)
            }
            R.id.image_down_arrow -> {
                  setExpandCollapseView()
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.rider_details_nav_layout, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as MainActivity).bottomNavigation.visibility = View.GONE

        if (arguments != null) {
            accepetRequestData = arguments?.getSerializable(ACCEPT_REQUEST_DATA) as StartTripData
            directionRouteResponse =
                    arguments!!.getSerializable(com.cab1.driver.fragment.ROUTE_RESPONSE) as GoogleDirection
        }
        if (accepetRequestData != null && directionRouteResponse != null) {
            tv_CustomerName.text = accepetRequestData!!.userFullName
            if(accepetRequestData!!.userFullName!!.isEmpty())
            {
                tv_CustomerName.text=activity!!.getString(R.string.unknown)
            }
            img_Customer.setImageURI(RestClient.image_customer_url + accepetRequestData!!.userProfilePicture)

            minuteTv.text =
                    (directionRouteResponse!!.routes!![0]!!.legs!![0]!!.duration!!.value.toString().toInt() / 60).toString()

            kmTV.text = String.format(
                "%.1f",
                (directionRouteResponse!!.routes!![0]!!.legs!![0]!!.distance!!.value.toString().toInt() / 1000f)
            )


            var calendar = Calendar.getInstance()

            calendar.add(Calendar.SECOND, directionRouteResponse!!.routes!![0]!!.legs!![0]!!.duration!!.value!!)

            var time: String
            var amPm: String
            time = SimpleDateFormat("hh:mm a").format(calendar.time)
            amPm = if (time.contentEquals("AM"))
                "AM"
            else
                "PM"
            reachTimeTv.text = SimpleDateFormat("hh:mm").format(calendar.time)
            amPmTv.text = amPm
            phoneNumber = accepetRequestData!!.userMobile!!

        } else {

        }

        ll_start_navigation.setOnClickListener(this)
        ll_cancelRide.setOnClickListener(this)
        ll_reach_ride.setOnClickListener(this)
        image_down_arrow.setOnClickListener(this)
        btnCallRider.setOnClickListener(this)
    }

    private fun setExpandCollapseView() {
         if(ll_confirmTrip_collspase.visibility==View.VISIBLE)
         {
             image_down_arrow.setImageResource(R.drawable.ic_arrow_down)
             MyUtils.collapse(ll_confirmTrip_collspase)
         }
        else
         {
             image_down_arrow.setImageResource(R.drawable.ic_up_arrow)
             MyUtils.expand(ll_confirmTrip_collspase)
         }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        val parent = parentFragment
        mListener = if (parent != null) {
            parent as DriverStatusListener
        } else {
            context as DriverStatusListener
        }
    }

    override fun onDetach() {
        mListener = null
        super.onDetach()
    }

    companion object {

        // TODO: Customize parameters
        fun newInstance(
            acceptRequestData: StartTripData,
            directionRouteResponse: GoogleDirection
        ): RiderDetailsNavigation =
            RiderDetailsNavigation().apply {
                arguments = Bundle().apply {
                    putSerializable(ACCEPT_REQUEST_DATA, acceptRequestData)
                    putSerializable(ROUTE_RESPONSE, directionRouteResponse)
                }
            }





    }

    private fun call(phoneNumber: String) {

        try {
            if (ActivityCompat.checkSelfPermission(
                    activity!!,
                    Manifest.permission.CALL_PHONE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                this@RiderDetailsNavigation.requestPermissions(
                    arrayOf(Manifest.permission.CALL_PHONE),
                    MY_PERMISSIONS_REQUEST_CALL_PHONE
                )

            } else if (phoneNumber.isNotEmpty()) {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:" + phoneNumber)





                activity!!.startActivity(callIntent)

            }

        } catch (e: ActivityNotFoundException) {
            Log.e("call", "Call failed", e)
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == MY_PERMISSIONS_REQUEST_CALL_PHONE && grantResults.isNotEmpty()) {
            call(phoneNumber)
        }
    }



}
