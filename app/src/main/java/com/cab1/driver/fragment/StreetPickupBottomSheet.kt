package com.cab1.driver.fragment

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import com.cab1.driver.DestinationLocationActivity
import com.cab1.driver.MainActivity
import com.cab1.driver.R
import com.cab1.driver.api.RestClient
import com.cab1.driver.iterfaces.DriverStatusListener
import com.cab1.driver.model.GoogleDirectionModel
import com.cab1.driver.model.StreetPickUpModel
import com.cab1.driver.pojo.Data1
import com.cab1.driver.pojo.GoogleDirection
import com.cab1.driver.pojo.Route
import com.cab1.driver.util.Googleutil
import com.cab1.driver.util.KeyboardUtil
import com.cab1.driver.util.MyUtils
import com.example.admin.myapplication.SessionManager
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.common.GooglePlayServicesUtil
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.libraries.places.compat.AutocompleteFilter
import com.google.android.libraries.places.compat.ui.PlaceAutocomplete
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.bottom_sheet_common_layout.*
import kotlinx.android.synthetic.main.street_pickup.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*


class StreetPickupBottomSheet : BottomSheetDialogFragment(), View.OnClickListener {

    private var mListener: DriverStatusListener? = null

    lateinit var activity: Activity
    var pickupLat: String = "0.0"
    var pickupLongi: String = "0.0"
    var destLongi: String = ""
    var destLat: String = ""
    var desAddress: String = ""
    var desCity: String = ""
    var pickAddress: List<Address> = listOf()
    var dropAddress: List<Address> = listOf()
    var pickUpFullAddress: String = "Unknown"
    lateinit var driverData: Data1


    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.dialogCloseIcon -> {
                dismiss()
            }
            R.id.btnstreetbook -> {
                rider_mobileno_text_input.error = null
                riderdestination_text_input.error = null

                if (rider_mobileno_edit_text.text.toString().trim().isEmpty() || rider_mobileno_edit_text.text.toString().trim().length != 10 || !TextUtils.isDigitsOnly(rider_mobileno_edit_text.text.toString())){
                    rider_mobileno_text_input.error = "Please enter valid mobile number."
                } else if (riderdestination_edit_text.text.toString().trim().isEmpty()) {
                    riderdestination_text_input.error = "Please enter destination location."
                } else {
                    rider_mobileno_text_input.error = null
                    riderdestination_text_input.error = null
                    isCancelable = false
                    btnstreetbook.startAnimation()


                    doAsync {

                        if (parentFragment is MainFragment) {
                            pickupLat = (parentFragment as MainFragment).currentLocation!!.latitude.toString()
                            pickupLongi = (parentFragment as MainFragment).currentLocation!!.longitude.toString()
                        }

                        pickAddress = Googleutil.getMyLocationAddressList(
                            activity,
                            pickupLat.toDouble(),
                            pickupLongi.toDouble()
                        )

                        dropAddress = Googleutil.getMyLocationAddressList(
                            activity, destLat.toDouble()
                            , destLongi.toDouble()
                        )


                        uiThread {
                            drawDirectionRoute(
                                LatLng(pickupLat.toDouble(), pickupLongi.toDouble()),
                                LatLng(destLat.toDouble(), destLongi.toDouble())
                            )
                        }
                    }
                }
            }
            R.id.riderdestination_edit_text -> {
                var i = Intent(activity, DestinationLocationActivity::class.java)
                i.putExtra("lats", pickupLat)
                i.putExtra("from", "Destination")
                i.putExtra("longs", pickupLongi)
                startActivityForResult(i, 2)
                activity!!.overridePendingTransition(
                    R.anim.slide_in_right,
                    R.anim.slide_out_left
                )

                //showAutocomplete()
            }

        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)

        return super.onCreateDialog(savedInstanceState)

    }

    fun setStreetPicupListener(streetPicupListener: DriverStatusListener) {
        this.mListener = streetPicupListener
    }

    /*override fun setupDialog(dialog: Dialog, style: Int) {
        dialog.window.setBackgroundDrawable( ColorDrawable(Color.TRANSPARENT))
    }*/

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.street_pickup, container, false)

        KeyboardUtil(getActivity()!!, view)

        return view

    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        driverData = SessionManager(activity).get_Authenticate_User()
        if (arguments != null) {
            pickupLat = arguments!!.getString("pickupLat")
            pickupLongi = arguments!!.getString("pickupLongi")


        }

        doAsync {
            pickAddress = Googleutil.getMyLocationAddressList(activity, pickupLat.toDouble(), pickupLongi.toDouble())
            if (pickAddress != null && pickAddress.isNotEmpty()) {
                val strAddress = pickAddress.get(0)
                val strbuilderAddress = StringBuilder("")

                for (i in 0..strAddress.maxAddressLineIndex) {
                    strbuilderAddress.append(strAddress.getAddressLine(i)).append(", ")
                }
                pickUpFullAddress = strbuilderAddress.toString()
            }
        }

        backIconImageView.visibility = View.GONE
        headerText.visibility = View.VISIBLE
        headerText.text = "Easy Ride"
        btnstreetbook.setOnClickListener(this)
        dialogCloseIcon.setOnClickListener(this)
        riderdestination_edit_text.setOnClickListener(this)


    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity = context as Activity

    }


    companion object {

        fun newInstance(context: Activity, pickupLongi: String, pickupLat: String): StreetPickupBottomSheet =
            StreetPickupBottomSheet().apply {
                this.activity = context
                arguments = Bundle().apply {
                    putString("pickupLongi", pickupLongi)
                    putString("pickupLat", pickupLat)

                }
            }


    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 2 && resultCode == Activity.RESULT_OK) {
            if (data != null) {

                destLat = data.getStringExtra("lattitude")
                destLongi = data.getStringExtra("longitude")

                desAddress = data.getStringExtra("primaryAddress")
                desCity = data.getStringExtra("desCity")
                if (activity != null)
                    riderdestination_edit_text.setText(desAddress)
            }
        }

        if (requestCode == 10 && resultCode == Activity.RESULT_OK && activity != null) {
            try {
                var place = PlaceAutocomplete.getPlace(activity, data)

                var geocoder = Geocoder(activity, Locale.getDefault())
                var list = geocoder.getFromLocation(place.latLng.latitude, place.latLng.longitude, 1)

                if (list != null && list.size > 0 && list[0].locality != null) {

                    destLat = place.latLng.latitude.toString()
                    destLongi = place.latLng.longitude.toString()


                    desAddress = place.name.toString().capitalize() + ", " + place.address?.toString()
                    desCity = if (!list[0].subAdminArea.isNullOrEmpty()) {
                        list[0].subAdminArea
                    } else {
                        list[0].locality
                    }

                    riderdestination_edit_text.setText(desAddress)


                } else {
                    try {
                        if (!MyUtils.isInternetAvailable(activity as MainActivity)) {
                            Toast.makeText(
                                activity,
                                resources.getString(R.string.error_common_network),
                                Toast.LENGTH_LONG
                            ).show()
                        } else {
                            Toast.makeText(
                                activity,
                                resources.getString(R.string.error_crash_error_message),
                                Toast.LENGTH_LONG
                            ).show()

                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            } catch (e: Exception) {
                Log.e("System out", e.toString())
            }
        }
        /**/

    }

    fun drawDirectionRoute(pickuplatlong: LatLng, dropDestination: LatLng) {
        //  MyUtils.showProgressDialog(activity,"Wait..")


        var googleDirectionModel = ViewModelProviders.of(this).get(GoogleDirectionModel::class.java)
        googleDirectionModel.getRoute(activity, pickuplatlong, dropDestination).observe(this,
            androidx.lifecycle.Observer<GoogleDirection> { googleDirectionResponse: GoogleDirection? ->


                if (googleDirectionResponse != null && googleDirectionResponse.status.equals("OK")) {

                    streetPickUpTrip(googleDirectionResponse)


                } else {
                    if (activity != null) {
                        btnstreetbook.revertAnimation {
                            btnstreetbook.text = "Book Trip"
                        }
                        if (googleDirectionResponse?.error_message != null) {
                            Toast.makeText(activity, googleDirectionResponse.error_message, Toast.LENGTH_LONG).show()
                        } else {

                            try {
                                if (!MyUtils.isInternetAvailable(activity as MainActivity)) {
                                    Toast.makeText(
                                        activity,
                                        resources.getString(R.string.error_common_network),
                                        Toast.LENGTH_LONG
                                    ).show()
                                } else {
                                    Toast.makeText(
                                        activity,
                                        resources.getString(R.string.error_crash_error_message),
                                        Toast.LENGTH_LONG
                                    ).show()

                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    }
                }

            })

    }


    private fun streetPickUpTrip(direction: GoogleDirection) {

        val gson = GsonBuilder().disableHtmlEscaping().create()
        val type = object : TypeToken<List<Route>>() {}.type
        val json1 = gson.toJson(direction.routes, type)


        val jsonObject1 = JSONArray(json1)
        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("driverID", driverData.driverID)
            jsonObject.put("categoryID", driverData.drivercars[0].categoryID)
            jsonObject.put("userFullName", rider_fullname_edit_text.text.toString().trim().capitalize())
            jsonObject.put("userCountryCode", "+91")
            jsonObject.put("userMobile", rider_mobileno_edit_text.text.toString().trim())
            jsonObject.put(
                "bookingDistanceEst",
                direction.routes!![0]!!.legs!![0]!!.distance!!.value!!.toDouble() / 1000
            )

            jsonObject.put(
                "bookingDurationEst",
                direction.routes!![0]!!.legs!![0]!!.duration!!.value!!.toLong() / 60
            )

            jsonObject.put("bookingPickuplatlong", "$pickupLat,$pickupLongi")
            jsonObject.put("bookingPickupAddress", pickUpFullAddress)
            jsonObject.put("bookingDroplatlong", "$destLat,$destLongi")
            jsonObject.put("bookingDropAddress", desAddress)
            if (!pickAddress[0].subAdminArea.isNullOrEmpty()) {
                jsonObject.put("pickupCityName", pickAddress[0].subAdminArea)
            } else if (!pickAddress[0].locality.isNullOrEmpty()) {
                jsonObject.put("pickupCityName", pickAddress[0].locality)
            }

            jsonObject.put("dropCityName", desCity)
            jsonObject.put("bookingTravelRoute", jsonObject1)
            jsonObject.put("pickupStateName", pickAddress[0].adminArea)
            jsonObject.put("dropStateName", dropAddress[0].adminArea)
            jsonObject.put("pickupPincode", pickAddress[0].postalCode)
            jsonObject.put("dropPincode", dropAddress[0].postalCode)


            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)
        val streetPickUpModel = ViewModelProviders.of(this).get(StreetPickUpModel::class.java)
        streetPickUpModel.createStreetPickUpTrip(activity, jsonArray.toString())
            .observe(this, androidx.lifecycle.Observer {
                if (it != null && it.isNotEmpty()) {
                    if (it[0].status.equals("True", true)) {
                        if (activity != null) {
                            MyUtils.dismissProgressDialog()
                            dismiss()
                            if (mListener != null)
                                mListener!!.onStreetTripBook(it[0].data!![0]!!)
                        }

                    } else {

                        if (activity != null) {
                            //    MyUtils.dismissProgressDialog()
                            btnstreetbook.revertAnimation {
                                btnstreetbook.text = "Book Trip"
                            }

                            Toast.makeText(activity, it[0].message, Toast.LENGTH_LONG).show()
                        }
                    }
                } else {
                    if (activity != null) {
                        btnstreetbook.revertAnimation {
                            btnstreetbook.text = "Book Trip"
                        }
                        try {
                            //MyUtils.dismissProgressDialog()
                            if (!MyUtils.isInternetAvailable(activity as MainActivity)) {
                                Toast.makeText(
                                    activity,
                                    resources.getString(R.string.error_common_network),
                                    Toast.LENGTH_LONG
                                ).show()
                            } else {
                                Toast.makeText(
                                    activity,
                                    resources.getString(R.string.error_crash_error_message),
                                    Toast.LENGTH_LONG
                                ).show()

                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }
            })

    }

    private fun showAutocomplete() {
        try {
            var typeFilter = AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_NONE)
                .setCountry("IN")
                .build()
            var bounds = LatLngBounds(
                LatLng(pickupLat.toDouble() - 0.05, pickupLongi.toDouble() - 0.05),
                LatLng(pickupLat.toDouble() + 0.05, pickupLongi.toDouble() + 0.05)
            )

            val intent = PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                .setFilter(typeFilter)
                .setBoundsBias(bounds)

                .build(activity)
            startActivityForResult(intent, 10)
            activity.overridePendingTransition(
                R.anim.slide_in_right,
                R.anim.slide_out_left
            )
        } catch (e: GooglePlayServicesRepairableException) {
            GooglePlayServicesUtil.getErrorDialog(e.connectionStatusCode, activity, 0)
        } catch (e: GooglePlayServicesNotAvailableException) {

        }

    }


}
