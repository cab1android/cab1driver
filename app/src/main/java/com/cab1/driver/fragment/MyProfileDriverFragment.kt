package com.cab1.driver.fragment


import android.Manifest.permission
import android.app.Activity.RESULT_OK
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.cab1.driver.CountryListActivity
import com.cab1.driver.MainActivity
import com.cab1.driver.R
import com.cab1.driver.SplashActivity
import com.cab1.driver.api.RestClient
import com.cab1.driver.model.*
import com.cab1.driver.pojo.*
import com.cab1.driver.service.AlarmReceiver
import com.cab1.driver.util.MyUtils
import com.example.admin.myapplication.SessionManager
import com.facebook.common.util.UriUtil
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_my_profile_driver.*
import kotlinx.android.synthetic.main.login_driver.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.io.Serializable
import java.util.*


class MyProfileDriverFragment : Fragment(), View.OnClickListener {

    private var v: View? = null
    private var isEdit: Boolean? = false
    lateinit var sessionManager: SessionManager
    var mfUser: File? = null
    var mfUser_Profile_Image: File? = null
    var countryCode: String = ""
    private var profileUpdateListener: ProfileUpdateListener? = null
    private var countrylist = ArrayList<Data>()
    var countryname: String = ""
    var countryID: String = ""
    var countryFlagImage: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {

        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

            v = inflater.inflate(R.layout.fragment_my_profile_driver, container, false)

        return v

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sessionManager = SessionManager(activity as MainActivity)

        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowCustomEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)
        (activity as MainActivity).setBottomNavMenuSelected(R.id.profileBottomMenu)
        (activity as MainActivity).bottomNavigation.visibility = View.GONE

        tvVerifcationTitle.text = "Profile"
        img_Logout.visibility = View.VISIBLE
        img_notification.visibility = View.GONE
        img_notification.setOnClickListener(this)
        img_Logout.setOnClickListener(this)
        toolbar.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }


        btnMyProfile!!.setOnClickListener(this)
        changePasswordlayout.setOnClickListener(this)
        img_ChangePwdRightArrow!!.setOnClickListener(this)
        taxiDetailsLayout!!.setOnClickListener(this)
        img_TaxiDetailsRightArrow!!.setOnClickListener(this)
        img_flagDropDown.setOnClickListener(this)
        img_countryflag.setOnClickListener(this)
        img_PhotoEdit.setOnClickListener(this)
        Img_UserProfileIcon.setOnClickListener(this)
        /* if(sessionManager.isLoggedIn() && sessionManager.get_Authenticate_User()!=null) {*/

        fullname_edit_text.setText(sessionManager.get_Authenticate_User().driverName)
        mobileno_edit_text.setText(sessionManager.get_Authenticate_User().driverMobile)
        tv_profile_countrycode.text = sessionManager.get_Authenticate_User().driverCountryCode
        countryCode = sessionManager.get_Authenticate_User().driverCountryCode
        emailid_edit_text.setText(sessionManager.get_Authenticate_User().driverEmail)
        img_countryflag.setImageURI(Uri.parse(RestClient.image_countryflag_url + sessionManager.get_Authenticate_User().countryFlagImage))
        Img_UserProfileIcon.setImageURI(Uri.parse(RestClient.image_driver_url + sessionManager.get_Authenticate_User().driverProfilePic))
        setEnableDisable(false)

        /* }else{
             setEnablDisable(false)
 e
         }*/

        fullname_edit_text.inputType = InputType.TYPE_TEXT_FLAG_CAP_WORDS

        fullname_edit_text.addTextChangedListener(object : TextWatcher {

            override fun onTextChanged(
                s: CharSequence, start: Int, before: Int,
                count: Int
            ) {
                // TODO Auto-generated method stub

            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {
                // TODO Auto-generated method stub

            }

            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub

                if (fullname_edit_text!!.text.toString().trim().length > 0) {

                    var x: Char
                    val t = IntArray(fullname_edit_text!!.text.toString().trim().length)

                    for (i in 0 until fullname_edit_text!!.text.toString().trim().length) {
                        x = fullname_edit_text!!.text.toString().trim().toCharArray()[i]
                        val z = x.toInt()
                        t[i] = z

                        if (z > 47 && z < 58 || z > 64 && z < 91
                            || z > 96 && z < 123 || z == 32
                        ) {

                        } else {

                            Toast.makeText(activity, "" + "Special Character not allowed", Toast.LENGTH_SHORT).show()
                            var ss = fullname_edit_text!!.text.toString()
                                .substring(0, fullname_edit_text!!.text.toString().length - 1)
                            fullname_edit_text.setText(ss)
                            fullname_edit_text.setSelection(fullname_edit_text!!.text.toString().length)
                        }

                    }
                }
            }
        })

    }

    private fun setEnableDisable(EnableDisable: Boolean) {
        fullname_edit_text!!.isEnabled = EnableDisable
        mobileno_edit_text!!.isEnabled = EnableDisable
        emailid_edit_text!!.isEnabled = EnableDisable
        mobileno_baseline!!.isEnabled = EnableDisable
        if (EnableDisable) {
            mobileno_baseline.background = (activity as MainActivity).resources.getDrawable(R.color.text_secondary)
            tv_profile_countrycode.setTextColor(resources.getColor(R.color.text_primary))
            imgdropdown.isEnabled = true
            img_countryflag.isEnabled = true

        } else {
            mobileno_baseline.background = (activity as MainActivity).resources.getDrawable(R.color.baseline_bg)
            tv_profile_countrycode.setTextColor(resources.getColor(R.color.disable_textcolor))
            imgdropdown.isEnabled = false
            img_countryflag.isEnabled = false

        }

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.img_PhotoEdit -> {
                getWriteStoragePermissionOther()
            }
            R.id.Img_UserProfileIcon -> {
                img_PhotoEdit.performClick()
            }
            R.id.btnMyProfile -> {

                if (isEdit == true) {
                    fullname_text_input.error = null // Clear the error
                    tvMobileErrorMSg.text = "" // Clear the error
                    tvMobileErrorMSg.visibility = View.GONE
                    emailid_text_input.error = null // Clear the error

                    when {
                        fullname_edit_text!!.text.toString().isEmpty() -> fullname_text_input.error =
                                activity!!.resources.getString(R.string.textfield_reg_eror_fullname)

                        mobileno_edit_text!!.text.toString().isEmpty() -> {
                            tvMobileErrorMSg.text =
                                    activity!!.resources.getString(R.string.textfield_reg_eror_mobileno)
                            tvMobileErrorMSg.visibility = View.VISIBLE
                        }
                        mobileno_edit_text!!.text.toString().length < 10 -> {
                            tvMobileErrorMSg.text =
                                    activity!!.resources.getString(R.string.textfield_reg_eror_validmobileno)
                            tvMobileErrorMSg.visibility = View.VISIBLE
                        }


                        emailid_edit_text!!.text.toString().isEmpty() -> emailid_text_input.error =
                                activity!!.resources.getString(R.string.textfield_reg_eror_emailid)
                        !MyUtils.isValidEmail(emailid_edit_text!!.text.toString().trim()) -> emailid_text_input.error =
                                activity!!.resources.getString(R.string.signup_enter_valid_emailid)

                        else -> {
                            fullname_text_input.error = null // Clear the error
                            tvMobileErrorMSg.text = "" // Clear the error
                            emailid_text_input.error = null // Clear the error
                            tvMobileErrorMSg.visibility = View.GONE
                            getProfileUpdate()
                        }
                    }
                } else {
                    isEdit = true
                    btnMyProfile!!.text = activity!!.resources.getString(R.string.update)
                    setEnableDisable(true)
                }


            }
            R.id.changePasswordlayout -> {
                (activity as MainActivity).navigateTo(ChangePasswordFragment(), true)
            }
            R.id.img_ChangePwdRightArrow -> {

            }
            R.id.img_notification -> {
                (activity as MainActivity).navigateTo(NotificationFragment(), true)

            }

            R.id.img_Logout -> {
                MyUtils.showMessageOKCancel(activity as MainActivity, "Are you sure you want to Logout?", "Logout",
                    DialogInterface.OnClickListener { dialog, which ->
                        dialog.dismiss()
                        getDriverLogout()
                    })
            }
            R.id.taxiDetailsLayout -> {
                val taxiDetailsBottomSheetFragment = TaxiDetailsBottomSheetFragment()

                taxiDetailsBottomSheetFragment.show(activity!!.supportFragmentManager, "taxiDetailsBottomSheetFragment")
            }
            R.id.img_TaxiDetailsRightArrow -> {
                val taxiDetailsBottomSheetFragment = TaxiDetailsBottomSheetFragment()
                taxiDetailsBottomSheetFragment.show(childFragmentManager, "taxiDetailsBottomSheetFragment")
            }
            R.id.img_flagDropDown -> {
                img_countryflag.performClick()
            }
            R.id.img_countryflag -> {

               // getCountryList()

            }

        }

    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.header_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)


        val notificationTollbarMenu = menu.findItem(R.id.notificationTollbarMenu)

        var actionView = notificationTollbarMenu.actionView



      /*  actionView.setOnClickListener {
            (activity as MainActivity).navigateTo(NotificationFragment(), true)
        }
*/
        notificationTollbarMenu.setOnMenuItemClickListener {

            (activity as MainActivity).navigateTo(NotificationFragment(), true)
            true

        }

    }

    private fun getDriverLogout() {
        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("languageID", "2")
            jsonObject.put("driverCountryCode", sessionManager.get_Authenticate_User().driverCountryCode)
            jsonObject.put("driverMobile", sessionManager.get_Authenticate_User().driverMobile)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)

        val logoutModel = ViewModelProviders.of(activity as MainActivity).get(LogoutModel::class.java)
        logoutModel.getLogout(activity as MainActivity, false, jsonArray.toString()).observe(activity as MainActivity,
            Observer<List<CommonStatus>> { loginPojo ->
                if (loginPojo != null && loginPojo.isNotEmpty()) {
                    if (loginPojo[0].status.equals("true", true)) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            AlarmReceiver.cancelAlarm()
                        }
                        sessionManager.clear_login_session()
                        MyUtils.startActivity(activity as MainActivity,SplashActivity::class.java, true,true)
                    } else {
                        (activity as MainActivity).showSnackBar(loginPojo[0].message)


                    }


                } else {
                    img_login.revertAnimation()
                    (activity as MainActivity).errorMethod()
                }
            })
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        var picturePath: String? = ""
        when (requestCode) {

            1211 ->

                if (data != null && resultCode == RESULT_OK && activity != null) {
                    val imageUri = data.data

                    picturePath = MyUtils.getPath1(imageUri, activity!!)

                    if (picturePath != null) {
                        if (picturePath.contains("https:")) {
                            (activity as MainActivity).showSnackBar("Please, select another profile pic.")
                        } else {
                            mfUser_Profile_Image = File(picturePath)
                            Img_UserProfileIcon.setImageURI(UriUtil.getUriForFile(mfUser_Profile_Image))

                            if (MyUtils.isInternetAvailable(activity!!)) {
                                uploadProfilePic()
                            } else {
                                (activity as MainActivity).showSnackBar(activity?.getString(R.string.error_common_network)!!)
                            }

                        }
                    } else {

                        (activity as MainActivity).showSnackBar("Please, select another profile pic.")

                    }
                }
            203 -> {
                if (data != null) {
                    if (data.hasExtra("countryID")) {
                        countryID = data.getStringExtra("countryID")
                    }
                    if (data.hasExtra("countryname")) {
                        countryname = data.getStringExtra("countryname")
                    }
                    if (data.hasExtra("countryCode")) {
                        countryCode = data.getStringExtra("countryCode")
                        tv_profile_countrycode.text = countryCode

                    }
                    if (data.hasExtra("countryFlagImage")) {
                        countryFlagImage = data.getStringExtra("countryFlagImage")
                        img_countryflag.setImageURI(Uri.parse(RestClient.image_countryflag_url + countryFlagImage))

                    }
                }
            }
        }
    }

    private fun uploadProfilePic() {
        MyUtils.showProgressDialog(activity as MainActivity, "Uploading")


        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", sessionManager.get_Authenticate_User().driverID)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)
        val uploadProfileModel = ViewModelProviders.of(this@MyProfileDriverFragment).get(UploadProfileModel::class.java)
        uploadProfileModel.uploadFile(
            activity!!, false, jsonArray.toString(), "drivers", mfUser_Profile_Image!!
        ).observe(this@MyProfileDriverFragment,
            Observer<List<DriverData>> { loginPojo ->
                if (loginPojo != null && loginPojo.isNotEmpty())
                {
                    if (loginPojo[0].status.equals("true", true)) {
                        uploadProfileApi(loginPojo[0].fileName)
                        MyUtils.dismissProgressDialog()
                        (activity as MainActivity).showSnackBar(loginPojo[0].message)
                    }
                    else
                    {
                        MyUtils.dismissProgressDialog()
                        (activity as MainActivity).showSnackBar(loginPojo[0].message)

                    }


                } else {
                    MyUtils.dismissProgressDialog()
                    (activity as MainActivity).errorMethod()
                }
            })
    }


    fun getWriteStoragePermissionOther() {
        val permissionCheck =
            ContextCompat.checkSelfPermission(activity as MainActivity, permission.WRITE_EXTERNAL_STORAGE)
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            getReadStoragePermissionOther()
        } else {
            this.requestPermissions(arrayOf(permission.WRITE_EXTERNAL_STORAGE), 1001)
        }
    }

    fun getReadStoragePermissionOther() {
        val permissionCheck =
            ContextCompat.checkSelfPermission(activity as MainActivity, permission.READ_EXTERNAL_STORAGE)
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            openGallery()
        } else {
            this.requestPermissions(arrayOf(permission.READ_EXTERNAL_STORAGE), 1002)
        }
    }

    fun openGallery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1211)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            1001 -> {
                if (grantResults.isNotEmpty()) {
                    getReadStoragePermissionOther()
                } else {
                    (activity as MainActivity).showSnackBar("Permission denied")
                }
            }
            1002 -> {
                if (grantResults.isNotEmpty()) {
                    openGallery()
                } else {
                    (activity as MainActivity).showSnackBar("Permission denied")
                }
            }
        }
    }

    private fun getProfileUpdate() {

        btnMyProfile.startAnimation()

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("driverID", sessionManager.get_Authenticate_User().driverID)
            jsonObject.put("driverName", fullname_edit_text.text.toString())
            jsonObject.put("driverCountryCode", countryCode)
            jsonObject.put("driverEmail", emailid_edit_text.text.toString())
            jsonObject.put("driverMobile", mobileno_edit_text.text.toString())
            jsonObject.put("driverMobileIsVerified", "Yes")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)
        val profileUpdateModel = ViewModelProviders.of(this@MyProfileDriverFragment).get(ProfileModel::class.java)
        profileUpdateModel.ProfileUpdate(activity as MainActivity, false, jsonArray.toString())
            .observe(this@MyProfileDriverFragment,
                Observer<List<DriverData>> { loginPojo ->
                    if (loginPojo != null && loginPojo.isNotEmpty()) {
                        if (loginPojo[0].status.equals("true", true)) {
                            btnMyProfile.revertAnimation {
                                btnMyProfile.text = "Update"
                            }

                            StoreSessionManager(loginPojo[0].data)
                            (activity as MainActivity).showSnackBar(loginPojo[0].message)

                            if (profileUpdateListener != null)
                                profileUpdateListener!!.onProfileUpdate(sessionManager.get_Authenticate_User().driverProfilePic!!)
                        } else {
                            btnMyProfile.revertAnimation {
                                btnMyProfile.text = "Update"
                            }

                            (activity as MainActivity).showSnackBar(loginPojo[0].message)
                        }


                    } else {
                        btnMyProfile.revertAnimation {
                            btnMyProfile.text = "Update"
                        }
                        (activity as MainActivity).errorMethod()
                    }
                })
    }

    private fun StoreSessionManager(driverdata: List<Data1>) {
        sessionManager.clear_login_session()
        val gson = Gson()
        val json = gson.toJson(driverdata[0])
        sessionManager.create_login_session(
            json,
            driverdata[0].driverMobile,
            "",
            true,
            sessionManager.isEmailLogin(),true
        )
    }

    private fun uploadProfileApi(mfUser_Profile_Image: String) {

        MyUtils.showProgressDialog(activity as MainActivity, "Uploading")

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("driverID", sessionManager.get_Authenticate_User().driverID)
            jsonObject.put("driverProfilePic", mfUser_Profile_Image)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)
        val profilePictureModel =
            ViewModelProviders.of(this@MyProfileDriverFragment).get(ProfilPictureModel::class.java)
        profilePictureModel.ProfilePictureUpdate(activity!!, false, jsonArray.toString())
            .observe(this@MyProfileDriverFragment,
                Observer<List<DriverData>> { loginPojo ->
                    if (loginPojo != null && loginPojo.isNotEmpty()) {
                        if (loginPojo[0].status.equals("true", true)) {
                            MyUtils.dismissProgressDialog()
                            StoreSessionManager(loginPojo[0].data)
                            if (profileUpdateListener != null)
                                profileUpdateListener!!.onProfileUpdate(sessionManager.get_Authenticate_User().driverProfilePic!!)
                        } else {
                            MyUtils.dismissProgressDialog()
                            (activity as MainActivity).showSnackBar(loginPojo[0].message)
                        }


                    } else {
                        MyUtils.dismissProgressDialog()
                        (activity as MainActivity).errorMethod()
                    }
                })
    }


    private fun getCountryList() {

        MyUtils.showProgressDialog(activity as MainActivity, "Please wait...")


        val countrylistModel = ViewModelProviders.of(this@MyProfileDriverFragment).get(CountrylistModel::class.java)
        countrylistModel.getCountry(activity as MainActivity, false).observe(this@MyProfileDriverFragment,
            Observer<List<Countrylist>> { countrylistPojo ->
                if (countrylistPojo != null && countrylistPojo.size > 0) {
                    if (countrylistPojo[0].status.equals("true")) {
                        MyUtils.dismissProgressDialog()

                        countrylist.clear()
                        countrylist.addAll(countrylistPojo[0].data)

                        var i = Intent(activity as MainActivity, CountryListActivity::class.java)
                        if (countrylist != null) {
                            i.putExtra("countrylist", countrylistPojo[0].data as Serializable)

                        }
                        startActivityForResult(i, 203)
                        (activity as MainActivity).overridePendingTransition(R.anim.slide_in_bottom, R.anim.stay)

                    } else {
                        MyUtils.dismissProgressDialog()
                        (activity as MainActivity).showSnackBar(countrylistPojo[0].message)
                    }

                } else {
                    MyUtils.dismissProgressDialog()

                    (activity as MainActivity).errorMethod()
                }
            })
    }

    interface ProfileUpdateListener {
        fun onProfileUpdate(imageName:String)
    }


}
