package com.cab1.driver.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.cab1.driver.MainActivity
import com.cab1.driver.R
import com.cab1.driver.adapter.TripDateWiseHistoryAdapter
import com.cab1.driver.pojo.StartTripData
import com.cab1.driver.pojo.TripHistoryDetailsData
import com.cab1.driver.util.MyUtils

import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.bottom_sheet_common_layout.*
import kotlinx.android.synthetic.main.fragment_trip_details.*
import kotlinx.android.synthetic.main.trip_datewise_history_bottomsheet_dialog.*

import java.io.Serializable

class TripDateWiseHistoryBottomSheetFragment : BottomSheetDialogFragment() {

    private lateinit var tripDateWiseHistoryAdapter: TripDateWiseHistoryAdapter
    private lateinit var layoutManager: LinearLayoutManager
    var data: ArrayList<StartTripData> = ArrayList()
    var tripDate:String=""
    var v:View?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme1)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {


            v = inflater.inflate(R.layout.trip_datewise_history_bottomsheet_dialog, container, false)

        return v

    }

    @SuppressLint("WrongConstant")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if(arguments!=null)
        {
       headerText.text=MyUtils.formatDate(arguments!!.getString("tripDate"),"yyyy-MM-dd","EEE, MMM dd yyyy")

        data=ArrayList()
            data= arguments!!.getSerializable("tripData") as ArrayList<StartTripData>
        }


    headerText.gravity = Gravity.START
        backIconImageView.visibility = View.VISIBLE

        layoutManager = LinearLayoutManager(context as AppCompatActivity, LinearLayout.VERTICAL, false)

        tripDateWiseHisotryRecyclerView.layoutManager = layoutManager

        tripDateWiseHistoryAdapter = TripDateWiseHistoryAdapter(activity!! ,data,object: TripDateWiseHistoryAdapter.OnItemClickListener{
            override fun onFareBreakup(startTripData: StartTripData) {
                openFareBreakupDialog(startTripData)
            }

            override fun onItemClick(position: Int, typeofOperation: String) {
                var trripDetailsFragment = TripDetailsFragment()
                var bundle = Bundle()
                bundle.putSerializable("endBookingData", data[position] as Serializable)
                trripDetailsFragment.arguments = bundle
                (activity as MainActivity).navigateTo(trripDetailsFragment, true)
            }
        })
        tripDateWiseHisotryRecyclerView.adapter = tripDateWiseHistoryAdapter
        val itemDecor = DividerItemDecoration(context, layoutManager.orientation)
        tripDateWiseHisotryRecyclerView.addItemDecoration(itemDecor)

        backIconImageView.setOnClickListener {
            dismiss()
        }

        dialogCloseIcon.setOnClickListener {
            dismiss()
        }

    }


    fun openFareBreakupDialog(startTripData: StartTripData)
    {
        var dialogBuilder = MaterialAlertDialogBuilder(activity!!)
        dialogBuilder.setCancelable(true)
// ...Irrelevant code for customizing the buttons and title
        var inflater = activity!!.getLayoutInflater()
        var dialogView = inflater.inflate(R.layout.item_farebreakup, null)
        var tvSubAmount = dialogView.findViewById<TextView>(R.id.tvSubAmount)
        var tvGST = dialogView.findViewById<TextView>(R.id.tvGSTAmount)
        var tvTotal = dialogView.findViewById<TextView>(R.id.tvTotalAmount)
        var tvBookingDiscount = dialogView.findViewById<TextView>(R.id.tvBookingDiscount)
        var tvYourAmount = dialogView.findViewById<TextView>(R.id.tvDriverAmount)
        var btnSubmit = dialogView.findViewById<com.cab1.driver.progressButton.customViews.CircularProgressButton>(R.id.btnSubmit)

        tvSubAmount.text="Rs. "+MyUtils.priceFormat(startTripData.bookingSubAmount)
        tvGST.text="Rs. "+MyUtils.priceFormat(startTripData.bookingGST)
        var totalDiscount = 0.0

        try {

            tvBookingDiscount.text = "Rs. " + MyUtils.priceFormat(startTripData.bookingDiscount)
        } catch (e: Exception) {

        }


        tvTotal.text="Rs. "+MyUtils.priceFormat(startTripData.bookingActualAmount)
        tvYourAmount.text="Rs. "+MyUtils.priceFormat(startTripData.bookingDriverCommisionAmount)



        dialogBuilder.setView(dialogView)
        var alertDialog = dialogBuilder.create()
        alertDialog.show()
        btnSubmit.setOnClickListener {
            alertDialog?.dismiss()
        }


    }




}