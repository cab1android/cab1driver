package com.cab1.driver.fragment


import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.NotificationManager
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.cab1.cab.customer.util.MarkerAnimation
import com.cab1.cab.driver.util.LocationProvider
import com.cab1.cab.driver.util.LocationProvider.Companion.CONNECTION_FAILURE_RESOLUTION_REQUEST
import com.cab1.driver.MainActivity
import com.cab1.driver.OtpVerificationActivity
import com.cab1.driver.R
import com.cab1.driver.SplashActivity
import com.cab1.driver.api.RestClient
import com.cab1.driver.application.MyApplication
import com.cab1.driver.database.AppDatabase
import com.cab1.driver.database.NavigationLog
import com.cab1.driver.iterfaces.DriverStatusListener
import com.cab1.driver.model.*
import com.cab1.driver.notification.MyFirebaseMessagingService
import com.cab1.driver.pojo.*
import com.cab1.driver.service.LocationUpdatesService
import com.cab1.driver.util.*
import com.cab1.driver.util.Googleutil.Companion.getPolyLineOption
import com.cab1.driver.util.Googleutil.Companion.polyLineAnimation
import com.example.admin.myapplication.SessionManager
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.google.android.gms.maps.model.Polyline
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.dialog.MaterialAlertDialogBuilder

import com.google.firebase.database.*
import com.google.gson.Gson
import com.google.maps.android.SphericalUtil
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.complete_trip_confirmation_layout.view.*
import kotlinx.android.synthetic.main.fragment_main.*

import kotlinx.android.synthetic.main.item_support.view.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import android.content.ServiceConnection as ServiceConnection1


class MainFragment : Fragment(), OnMapReadyCallback, View.OnClickListener, DriverStatusListener {


    private var mMap: GoogleMap? = null
    //var mCurrLocationMarker: Marker? = null
    private var destinationMarker: Marker? = null

    private var locationProvider: LocationProvider? = null
    private val REQ_CODE_DRAW_OVER = 1004
    var googleDirectionModel: GoogleDirectionModel? = null
    lateinit var sessionManager: SessionManager
    lateinit var driverData: Data1
    var address = ""
    var bookingId = "-1"
    var locationLogType = "No Trip"
    var updateLocationModel: UpdateLocationModel? = null
    var progressTask: Timer? = null
    var navigationLatLong: LatLng? = null
    var defaultZoom = 16f
    var i = 0
    var mStopHandler = false
    var handler: Handler? = null
    var runnable: Runnable? = null
    var routePoints: ArrayList<LatLng> = ArrayList()
    var isTracking = false
    var circlePolygon: Polygon? = null
    var points = ArrayList<LatLng>()
    var directionRouteResponse: GoogleDirection? = null
    var greyPolyLine: Polyline? = null
    var blackPolyline: Polyline? = null
    var currentLocation: Location? = null
    var oldLocation: Location? = null
    var destinationLatLong: LatLng? = null
    var pushRefData = RefData()
    var successBookingRequestData: StartTripData? = null
    private var cancelTripMaster = ArrayList<CancelTripData>()
    var v: View? = null
    lateinit var mapFragment: MapView
    var db: AppDatabase? = null
    var startTiripData: StartTripData? = null

    var mBottomSheetDialog: BottomSheetDialog? = null
    var navigationLogList: List<NavigationLog> = ArrayList()
    var push: Push? = null
    var carMockLocation: Boolean = false

    var streetPickupBottomSheet: StreetPickupBottomSheet? = null
    var waitingTime: Long = 0L
    var waitStartTime: Date? = null

    var needCurrentTripDetails: Boolean = true
    private var mService: LocationUpdatesService? = null

    var myReceiver: MyReceiver? = null


    // Tracks the bound state of the service.
    private var mBound: Boolean = false
    lateinit var activity: Activity

    var mCurrLocationMarker: Marker? = null

    var isDriverOnline: Boolean = false

    var updateToFirebase: UpdateToFirebase? = null

    var mediaPlayer: MediaPlayer? = null

    var phoneNumber: String = ""
    var isLastLocation = false

    //private lateinit var database: DatabaseReference


    private val mServiceConnection = object : ServiceConnection1 {

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            val binder = service as LocationUpdatesService.LocalBinder
            mService = binder.service
            mBound = true
        }

        override fun onServiceDisconnected(name: ComponentName) {
            mService = null
            mBound = false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {

            push = arguments!!.getSerializable("Push") as Push
        }

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity = context as Activity
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        if (v == null) {
            v = inflater.inflate(R.layout.fragment_main, container, false)

            mapFragment = v!!.findViewById(R.id.mapView) as MapView
            mapFragment.onCreate(savedInstanceState)

            mapFragment.onResume() // needed to get the map to display immediately

            MyUtils.isLocationUpdateOnGoing = true
            MapsInitializer.initialize(activity!!.applicationContext)


            mapFragment.getMapAsync(this)
            try {
                var location = Location("current")
                location.latitude = MyUtils.currentLattitude
                location.longitude = MyUtils.currentLongitude
                currentLocation = location
                isLastLocation = true
            } catch (e: Exception) {
            }
        }
        return v
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        myReceiver = MyReceiver()

    //    database = FirebaseDatabase.getInstance().reference
        updateLocationModel = ViewModelProviders.of(this).get(UpdateLocationModel::class.java)
        db = AppDatabase.getDatabase(activity!!)
        sessionManager = SessionManager(activity as MainActivity)
        driverData = sessionManager.get_Authenticate_User()
        updateToFirebase = UpdateToFirebase(activity, this)
        setBookingId(sessionManager.bookingId, sessionManager.tripLogType)

        showPushIndicator()


        val df = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val date = df.format(Calendar.getInstance().time)
        Log.e("date", date)
        if (locationProvider != null) {

            locationProvider!!.connect()
        }

        centerMapFab.setOnClickListener(this)
        (activity as MainActivity).setBottomNavMenuSelected(R.id.homeBottomMenu)

        (activity as MainActivity).bottomNavigation.visibility = View.VISIBLE



        changeStatusButton.setOnClickListener(this)
        if (sessionManager.get_Authenticate_User().driverDutyStatus.equals("Online")) {
            isDriverOnline(true)

        } else {
            isDriverOnline(false)

        }
        if (sessionManager.get_Authenticate_User().driverGoToHome.equals("Yes")) {
            isDriverOnline(true, true)

        } else {
            isDriverOnline(false, true)

        }


        //Google Map display App Icon
        LocalBroadcastManager.getInstance(activity!!)
            .registerReceiver(receiver, IntentFilter(FloatingOverMapIconService.BROADCAST_ACTION))


        toggleStatusButton.setOnClickListener {

            if (toggleStatusButton.text.toString().equals("Online", true)) {

                setDutyStatus(false)
            } else {

                setDutyStatus(true)
            }

        }
        toggleHomeButton.setOnClickListener {

            if (sessionManager.get_Authenticate_User().driverGoToHome.equals("Yes")) {

                setDutyStatus(false, true)
            } else {

                setDutyStatus(true, true)
            }

        }

        if (driverData.franchiseID.isNullOrEmpty()) {
            supportFab.visibility = View.GONE
        }
        supportFab.setOnClickListener {


            var dialogBuilder = MaterialAlertDialogBuilder(activity!!)
            dialogBuilder.setCancelable(true)
// ...Irrelevant code for customizing the buttons and title
            var inflater = activity!!.getLayoutInflater()
            var dialogView = inflater.inflate(R.layout.item_support, null)
            var tv_support_emailId = dialogView.findViewById<TextView>(R.id.tv_support_emailId)
            var tv_support_mobile = dialogView.findViewById<TextView>(R.id.textViewHelpLineNumberSupport)


            if (AutoStart().getIntent(activity!!) == null) {
                dialogView.btnAutoStart.visibility = View.GONE
                dialogView.tvAutoMsg.visibility = View.GONE
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            } else {
                dialogView.btnSound.visibility = View.GONE
                dialogView.tvSoundMsg.visibility = View.GONE
            }
            if (dialogView.btnAutoStart.visibility != View.VISIBLE && dialogView.btnSound.visibility != View.VISIBLE) {
                dialogView.tv_notification.visibility = View.GONE
            }

            if (!driverData.franchiseEmail.isNullOrEmpty())
                tv_support_emailId.setText(driverData.franchiseEmail)
            if (!driverData.franchiseMobile.isNullOrEmpty())
                tv_support_mobile.setText(driverData.franchiseMobile)


            dialogBuilder.setView(dialogView)
            var alertDialog = dialogBuilder.create()
            alertDialog.show();

            dialogView.findViewById<LinearLayout>(R.id.emailLinearLayout).setOnClickListener {
                alertDialog.dismiss()
                if (!driverData.franchiseEmail.isNullOrEmpty()) {
                    val emailIntent = Intent(Intent.ACTION_SEND)
                    emailIntent.type = "plain/text"
                    emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, driverData.franchiseEmail)
                    emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject")
                    emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Text")
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."))
                }
            }
            dialogView.findViewById<LinearLayout>(R.id.CallLinearLayout).setOnClickListener {
                alertDialog.dismiss()
                if (!driverData.franchiseMobile.isNullOrEmpty()) {
                    phoneNumber = driverData.franchiseMobile.toString()
                    call(phoneNumber)
                }

            }
            dialogView.btnAutoStart.setOnClickListener {

                AutoStart().openAutoStartIntent(activity!!)
                alertDialog.dismiss()

            }

            dialogView.btnSound.setOnClickListener {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                    val notificationManager =
                        MyApplication.instance.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                    val existingChannel = notificationManager.getNotificationChannel(MyUtils.channelId)

//it will delete existing channel if it exists
                    if (existingChannel != null) {


                        val intent = Intent(Settings.ACTION_CHANNEL_NOTIFICATION_SETTINGS).apply {
                            putExtra(Settings.EXTRA_APP_PACKAGE, activity!!.packageName)
                            putExtra(Settings.EXTRA_CHANNEL_ID, existingChannel.id)
                        }
                        startActivity(intent)
                    }
                }
            }

        }

    }

    override fun onStart() {
        super.onStart()
        /*   activity.bindService(
               Intent(activity, LocationUpdatesService::class.java), mServiceConnection,
               Context.BIND_AUTO_CREATE
           )*/
    }

    override fun onResume() {
        super.onResume()
        /*  try {
              LocalBroadcastManager.getInstance(activity!!).registerReceiver(
                  myReceiver!!,
                  IntentFilter(LocationUpdatesService.ACTION_BROADCAST)
              )
              requestLocationUpdateBackground()

          } catch (e: Exception) {
          }
  */

    }


    override fun onStop() {

        /* try {
                  if (mBound) {
                      // Unbind from the service. This signals to the service that this activity is no longer
                      // in the foreground, and the service can respond by promoting itself to a foreground
                      // service.
                      activity.unbindService(mServiceConnection);
                      mBound = false;
                  }
              } catch (e: Exception) {
              }*/
        super.onStop()
    }


    override fun onPause() {
        super.onPause()
        //  LocalBroadcastManager.getInstance(activity!!).unregisterReceiver(myReceiver!!)
        if (sessionManager != null) {
            sessionManager.bookingId = bookingId
            sessionManager.tripLogType = locationLogType
        }
    }


    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        mMap!!.mapType = GoogleMap.MAP_TYPE_NORMAL
        mMap!!.uiSettings.isZoomControlsEnabled = false
        mMap!!.uiSettings.isCompassEnabled = false
        mMap!!.uiSettings.isMyLocationButtonEnabled = false
        mMap!!.uiSettings.isMapToolbarEnabled = false
        mMap!!.setMaxZoomPreference(19f)
        mMap!!.setMinZoomPreference(12f)
        mMap!!.isMyLocationEnabled = false
        mMap!!.uiSettings.isRotateGesturesEnabled = true

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            googleMap.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                    activity, R.raw.map_style
                )
            )


        } catch (e: Exception) {
            Log.e("Map Style", "Can't find style. Error: ", e)
        }



        carMarkerIv.visibility = View.GONE
        connectLocation()
        Handler().postDelayed({
            if (push != null)
                pushHandler(push!!)

        }, 500)


    }

    private fun connectLocation() {
        locationProvider = LocationProvider(
            activity,
            LocationProvider.HIGH_ACCURACY,
            object : LocationProvider.CurrentLocationCallback {
                override fun handleNewLocation(location: Location) {

                    Log.d("currentLocation", location.toString())
                    if (carMockLocation)
                        return
                    setSpeedTime(location)


                    if (isLastLocation || currentLocation == null || (mMap != null && location.hasAccuracy() && location.accuracy < 20f && locationProvider?.isBetterLocation(
                            location,
                            currentLocation
                        )!!)
                        && (currentLocation != null && currentLocation!!.distanceTo(location)
                                > 10
                                )
                    ) {


                        if (bookingId != "-1" && points.isNotEmpty()) {
                            var idx = PolyUtil.locationIndexOnEdgeOrPath(
                                LatLng(location.latitude, location.longitude),
                                points,
                                true,
                                true,
                                20.0
                            )
                            if (idx > 0) {
                                // do your stuff here
                                location.latitude = points[idx].latitude
                                location.longitude = points[idx].longitude
                                for (i in 0 until idx) {
                                    if (i < points.size)
                                        points.removeAt(i)
                                }


                            }

                        }
                        updateCarMaker(location)
                        updateMapPosition(location)


                        //Store Add offline fb

                        if (bookingId != "-1" && bookingId.isNotEmpty()) {
                            updateToFirebase?.location = location
                            if (locationLogType.equals("Enroute", true)) {
                                if (currentLocation == null) {
                                    insertNavigationLatLongDb(location)
                                }
                                if (currentLocation != null && currentLocation!!.distanceTo(location)
                                    > 20
                                ) {
                                    insertNavigationLatLongDb(location)
                                }
                            }

                        }
                        isLastLocation = false

                        updateLocationToServer(location)
                        currentLocation = location


                        // if driver is Online location send to server


                    }

                }

            })

        locationProvider!!.connect()
    }

    private fun updateLocationToServer(location: Location) {

        try {
            if (!isDriverOnline) {
                return
            }



            updateLocationModel?.updateCurrentLocation(
                activity!!,
                driverData.driverID,
                bookingId,
                needCurrentTripDetails,
                locationLogType,
                location
            )
                ?.observe(this@MainFragment, Observer
                {

                    if (activity != null && it != null && it.isNotEmpty() && it[0].status.equals(
                            "True",
                            true
                        ) && it[0].data.isNotEmpty()
                    ) {


                        needCurrentTripDetails = false



                        if (it[0].tripdetails != null && it[0].tripdetails.isNotEmpty()) {


                            if (childFragmentManager != null && childFragmentManager.fragments.isEmpty() && MyUtils.isLocationUpdateOnGoing) {
                                MyUtils.isLocationUpdateOnGoing = false
                                startTiripData = it[0].tripdetails[0]

                                when {
                                    startTiripData!!.statusID == "3" -> {
                                        updateToFirebase?.startTripData = startTiripData
                                        setBookingId(startTiripData!!.bookingID, "Pickup")

                                        var latplongPickup =
                                            startTiripData!!.bookingPickuplatlong!!.split(",")
                                                .toTypedArray()

                                        if (latplongPickup.size == 2) {

                                            var latPickup = latplongPickup[0]
                                            var longPickup = latplongPickup[1]


                                            try {
                                                destinationLatLong =
                                                    LatLng(latPickup.toDouble(), longPickup.toDouble())

                                                drawDirectionRoute(

                                                    LatLng(
                                                        currentLocation?.latitude!!,
                                                        currentLocation?.longitude!!
                                                    ),
                                                    destinationLatLong!!
                                                )
                                            } catch (e: Exception) {
                                                e.printStackTrace()
                                            }
                                        }
                                    }
                                    startTiripData!!.statusID == "7" -> {
                                        tripStart(startTiripData!!)
                                        replaceFragment(RiderStartNavigation.newInstance(startTiripData!!))
                                    }
                                    startTiripData!!.statusID == "5" -> {
                                        tripStart(startTiripData!!)
                                        replaceFragment(RiderStartNavigation.newInstance(startTiripData!!))
                                    }
                                }
                            }
                        }

                    }


                })
        } catch (e: Exception) {
            e.printStackTrace()

        }


    }

    private fun reachCustomer(startTripData: StartTripData) {

        waitingTime = 0L
        waitStartTime = null
        setBookingId(startTripData.bookingID, "Reached")
        clearMap()
        replaceFragment(RiderStartNavigation.newInstance(startTripData))

        bookingAcceptFirebase("DriverReachedAtPickupLocation")
    }


    private fun getSetAddress(latlong: LatLng) {

        doAsync {

            try {
                address =
                    Googleutil.getMyLocationAddress(
                        activity as MainActivity,
                        latlong.latitude,
                        latlong.longitude
                    )
            } catch (e: Exception) {
                e.printStackTrace()
            }
            uiThread {
                try {
                    if (currentLocationCardView != null && address != null && address.isNotEmpty()) {
                        if (currentLocationCardView.visibility != View.VISIBLE)
                            MyUtils.expand(currentLocationCardView)

                        tv_location.text = address
                    }
                } catch (e: Exception) {
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (locationProvider != null) {
            locationProvider!!.disconnect()
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.centerMapFab -> {
                carMarkerIv.visibility = View.GONE
                centerMap()
            }
            /*R.id.startPickup -> {

            }*/

            R.id.changeStatusButton -> {
                /* if (changeStatusButton.text.toString().equals("Online", true)) {

                     setDutyStatus(false)
                 } else {

                     setDutyStatus(true)
                 }*/

            }

        }


    }

    private fun centerMap() {
        if (currentLocation != null && mMap != null) {


            mMap!!.animateCamera(
                CameraUpdateFactory.newLatLngZoom(
                    LatLng(
                        currentLocation?.latitude!!,
                        currentLocation?.longitude!!
                    ), defaultZoom
                )
            )
        }
    }

    private fun askDrawOverPermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            // if OS is pre-marshmallow then create the floating icon, no permission is needed
            createFloatingBackButton()
        } else {
            if (!Settings.canDrawOverlays(activity)) {
                // asking for DRAW_OVER permission in settings
                var intent = Intent(
                    Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + activity?.applicationContext?.packageName)
                )
                activity!!.startActivityForResult(intent, REQ_CODE_DRAW_OVER)
                activity!!.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
            } else {
                createFloatingBackButton()
            }
        }
    }

    private fun createFloatingBackButton() {


        var gmmIntentUri =
            Uri.parse("google.navigation:q=" + navigationLatLong?.latitude + "," + navigationLatLong?.longitude)

        if (navigationLatLong?.latitude!! <= 0) {
            gmmIntentUri =
                Uri.parse("geo:" + currentLocation?.latitude + "," + currentLocation?.longitude)
        }

        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        mapIntent.setPackage("com.google.android.apps.maps")
        if (mapIntent.resolveActivity(activity?.packageManager) != null) {
            activity?.startService(Intent(activity, FloatingOverMapIconService::class.java))


            activity!!.startActivityForResult(mapIntent, 1234)
            activity!!.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)


        } else {
            (activity as MainActivity).showSnackBar("Please install Google Map application.")
        }
    }


    @SuppressLint("NewApi")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == REQ_CODE_DRAW_OVER) {

            if (Settings.canDrawOverlays(activity)) {
                createFloatingBackButton()
            } else {
                Toast.makeText(activity, "Navigation permission denied", Toast.LENGTH_LONG).show()
            }
        } else if (requestCode == CONNECTION_FAILURE_RESOLUTION_REQUEST) {
            connectLocation()
        } else if (requestCode == 1234) {
            activity!!.stopService(Intent(activity!!, FloatingOverMapIconService::class.java))

        } else {
            super.onActivityResult(requestCode, resultCode, data)
            if (childFragmentManager != null && childFragmentManager.fragments.isNotEmpty())
                childFragmentManager.fragments.forEach {
                    it.onActivityResult(requestCode, resultCode, data)
                }

        }

    }

    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action!!.equals(
                    FloatingOverMapIconService.BROADCAST_ACTION,
                    ignoreCase = true
                ) && activity != null
            ) {
                val selfIntent = Intent(activity!!, MainActivity::class.java)
                selfIntent.flags =
                    (Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_SINGLE_TOP)
                activity!!.startActivity(selfIntent)
            }
        }

    }

    fun drawDirectionRoute(pickuplatlong: LatLng, dropDestination: LatLng) {
        googleDirectionModel = ViewModelProviders.of(this).get(GoogleDirectionModel::class.java)
        googleDirectionModel?.getRoute(activity!!, pickuplatlong, dropDestination)
            ?.observe(this@MainFragment,
                Observer<GoogleDirection> { googleDirectionResponse: GoogleDirection? ->


                    if (googleDirectionResponse != null && googleDirectionResponse.status.equals("OK")) {

                        directionRouteResponse = googleDirectionResponse
                        points = ArrayList<LatLng>()



                        for (it in googleDirectionResponse.routes!![0]?.legs!!) {
                            it?.steps!!.forEach { step ->

                                points.addAll(
                                    Googleutil.decodePoly(step?.polyline!!.points.toString())
                                )
                            }
                        }

                        updateToFirebase?.routePoints = points
                        updateToFirebase?.isRouteApiCall = false
                        polylinePickUp()

                    } else {
                        if (googleDirectionResponse != null && activity != null) {
                            mMap?.clear()
                            points.clear()
                            updateToFirebase?.isRouteApiCall = false
                            ((activity) as MainActivity).showSnackBar(googleDirectionResponse.error_message!!)
                        }
                    }

                })

    }

    private fun runCarOnRoute() {


        runnable = Runnable()
        {
            carMockLocation = true
            i++

            if (i >= points.size) {
                i = 0
                handler?.removeCallbacks(runnable)
                carMockLocation = false

            } else {

                var location = Location("current")
                location.latitude = points[i].latitude
                location.longitude = points[i].longitude
                if (i > 0 && i < points.size) {
                    location.bearing = SphericalUtil.computeHeading(
                        LatLng(points[i - 1].latitude, points[i - 1].longitude),
                        LatLng(points[i].latitude, points[i].longitude)
                    )
                        .toFloat()
                }
                updateCarMaker(location)
                updateMapPosition(location, isTracking = true)
                updateToFirebase?.location = location
                updateLocationToServer(location)
                if (i >= points.size - 1) {
                    i = 0
                    handler?.removeCallbacks(runnable)
                    carMockLocation = false
                } else {
                    handler?.postDelayed(runnable, 2000)
                }
            }

        }


// start navigationLogList with:
        handler = Handler()
        carMockLocation = true
        handler?.postDelayed(runnable, 10000)


    }

    private fun setDutyStatus(isOnline: Boolean, isGotHome: Boolean = false) {
        MyUtils.showProgressDialog(activity!!, "Wait..")

        //changeStatusButton.startAnimation()
        val jsonArray = JSONArray()
        val jsonObject = JSONObject()


        var apiName = ""
        try {
            jsonObject.put("driverID", sessionManager.get_Authenticate_User().driverID)

            if (isGotHome) {
                apiName = "GotoHome"
                if (isOnline) {
                    jsonObject.put("driverGoToHome", "Yes")
                } else {
                    jsonObject.put("driverGoToHome", "No")
                }
            } else {
                apiName = "DutyStatus"

                if (isOnline) {
                    jsonObject.put("driverDutyStatus", "Online")
                } else {
                    jsonObject.put("driverDutyStatus", "Offline")
                }
            }
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)
        val dutyStatusModel = ViewModelProviders.of(this@MainFragment).get(DutyStatusModel::class.java)
        dutyStatusModel.getDutyStatus(activity as MainActivity, jsonArray.toString(), apiName)
            .observe(this@MainFragment,
                Observer<List<DriverData>> { dutystatus ->

                    MyUtils.dismissProgressDialog()

                    if (dutystatus != null && dutystatus.isNotEmpty()) {

                        if (dutystatus[0].status.equals("true", true)) {


                            isDriverOnline(isOnline, isGotHome)


                            /*   changeStatusButton.revertAnimation {
                                   changeStatusButton.text = dutystatus[0].data[0].driverDutyStatus
                               }*/
                            storeSessionManager(dutystatus[0].data)
                        } else {
                            isDriverOnline(!isOnline, isGotHome)
                            /* changeStatusButton.revertAnimation()*/
                            (activity as MainActivity).showSnackBar("" + dutystatus[0].message)

                        }

                    } else {
                        isDriverOnline(!isOnline, isGotHome)
                        (activity as MainActivity).errorMethod()
                    }
                })
    }

    private fun isDriverOnline(isOnline: Boolean, isGoToHome: Boolean = false) {
        //  tvDriverStatus.visibility=View.INVISIBLE

        if (isGoToHome) {
            if (isOnline) {


                toggleHomeButton.isChecked = true
                /*tvDriverStatus.text = "You are online"
            img_online.setColorFilter(resources.getColor(R.color.text_green))*/


            } else {


                toggleHomeButton.isChecked = false
                // img_online.setColorFilter(resources.getColor(R.color.text_graycolor))


            }
        } else {

            if (isOnline) {

                changeStatusButton.text = "Online"
                changeStatusButton.setBackgroundColor(activity!!.resources.getColor(R.color.text_green))
                toggleStatusButton.text = "Online"
                toggleStatusButton.isChecked = true
                /*tvDriverStatus.text = "You are online"
            img_online.setColorFilter(resources.getColor(R.color.text_green))*/
                isDriverOnline = true

            } else {

                changeStatusButton.text = "Offline"
                // tvDriverStatus.text = "You are offline"
                changeStatusButton.setBackgroundColor(activity!!.resources.getColor(R.color.colorPrimary))
                toggleStatusButton.text = "Offline"
                toggleStatusButton.isChecked = false
                // img_online.setColorFilter(resources.getColor(R.color.text_graycolor))
                isDriverOnline = false

            }
        }

    }

    private fun storeSessionManager(driverData: List<Data1>) {
        val sessionManager = SessionManager(activity as MainActivity)

        val gson = Gson()

        val json = gson.toJson(driverData[0])

        sessionManager.create_login_session(
            json,
            driverData[0].driverMobile,
            "",
            true,
            sessionManager.isEmailLogin(), true
        )

        Log.d("System out", "Session data : $json")
    }


    override fun onTripAcceptSuccess(acceptRequestData: StartTripData, directionRouteResponse: GoogleDirection) {


        tripAccept(acceptRequestData, directionRouteResponse)

        bookingAcceptFirebase("BookingRequestAccepted")

        pausePlayer()


    }

    private fun tripAccept(acceptRequestData: StartTripData, directionRouteResponse: GoogleDirection) {
        sessionManager.tripType = ""
        removeFragment(AcceptTripDialogFragment::class.java.name)
        successBookingRequestData = acceptRequestData

        waitingTime = 0L
        waitStartTime = null

        setBookingId(successBookingRequestData!!.bookingID, "Pickup")

        isTracking = true
        if (mMap != null && circlePolygon != null)
            circlePolygon!!.remove()

        centerMap()
        points = ArrayList<LatLng>()



        for (it in directionRouteResponse.routes!![0]?.legs!!) {
            it?.steps!!.forEach { step ->

                points.addAll(
                    Googleutil.decodePoly(step?.polyline!!.points.toString())
                )
            }
        }

        updateToFirebase?.startTripData = acceptRequestData
        updateToFirebase?.setPickUpAndTripRoute(points)


        //runCarOnRoute()
        replaceFragment(RiderDetailsNavigation.newInstance(acceptRequestData, directionRouteResponse))
        //   runCarOnRoute()

    }

    override fun onTripAcceptFailure(position: Int) {

        removeFragment(AcceptTripDialogFragment::class.java.name)
        clearMap()

        waitingTime = 0L
        waitStartTime = null

        setBookingId("-1", "No Trip")

        (activity as MainActivity).bottomNavigation.visibility = View.VISIBLE
        pausePlayer()
    }


    override fun onRidePickUpStartNavigation(navigationLatLng: LatLng) {
        navigationLatLong = navigationLatLng
        askDrawOverPermission()
    }

    override fun onRideCancel() {
        cancelTripReasonMaster()

    }

    override fun onRideReach(acceptRequestData: StartTripData) {

        getReachTrip(acceptRequestData)


        /*var radius = SphericalUtil.computeDistanceBetween(currentLatLong, destinationLatLong)
              var reachDistanceMeter = 100
              if (driverData?.setting.isNotEmpty())
                  reachDistanceMeter = driverData.setting[0].settingsReachDistanceMetre!!.toInt()

              if (radius <=  reachDistanceMeter) {
                  getReachTrip(acceptRequestData)
              }else{
                  (activity as MainActivity).showSnackBar("You are not reach within $reachDistanceMeter meter circle")
              }
      */
    }

    override fun onCallRider() {

    }


    fun updateCarMaker(location: Location) {
        if (activity == null)
            return

        var newLocation = LatLng(location.latitude, location.longitude)
        if (mCurrLocationMarker != null) {

            MarkerAnimation.animateMarker(mCurrLocationMarker!!, newLocation)


        } else {

            var iconId = if (driverData?.categoryVehicleType.isNullOrEmpty()) {
                R.drawable.car_pin
            } else if (!driverData?.categoryVehicleType.isNullOrEmpty() && driverData?.categoryVehicleType!!.contains(
                    "Bike"
                )
            ) {
                R.drawable.bike_pin
            } else if (!driverData?.categoryVehicleType.isNullOrEmpty() && driverData?.categoryVehicleType!!.contains(
                    "Auto"
                )
            ) {
                R.drawable.riksha_pin
            } else {
                R.drawable.car_pin
            }
            mCurrLocationMarker = mMap!!.addMarker(
                MarkerOptions().position(newLocation).title("You are here").icon(
                    BitmapDescriptorFactory.fromResource(
                        iconId
                    )
                )
            )
        }

        if (bookingId == "-1") {
            if (currentLocation == null) {
                getSetAddress(newLocation)
            } else if (currentLocation!!.distanceTo(location) > 100)

                getSetAddress(newLocation)
        } else {
            if (currentLocationCardView != null && currentLocationCardView.visibility != View.GONE)
                MyUtils.collapse(currentLocationCardView)

        }
    }

    fun updateMapPosition(location: Location, bearing: Float = 0.0f, isTracking: Boolean = false) {


        var previousPosition = mMap!!.cameraPosition


        if (isTracking) {
            var currentPlace = CameraPosition.Builder()
                .target(LatLng(location.latitude, location.longitude))
                .zoom(previousPosition.zoom)
                .bearing(location.bearing)
                .build()
            mMap!!.moveCamera(CameraUpdateFactory.newCameraPosition(currentPlace))

        } else {


            if (bookingId == "-1") {


                var currentPlace = CameraPosition.Builder()
                    .target(LatLng(location.latitude, location.longitude))
                    .zoom(previousPosition.zoom)
                    .bearing(location.bearing)
                    .build()
                mMap!!.moveCamera(CameraUpdateFactory.newCameraPosition(currentPlace))
                //   mMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(currentPlace), 200, null)

            } else {


                var currentPlace = CameraPosition.Builder()
                    .target(LatLng(location.latitude, location.longitude))
                    .zoom(previousPosition.zoom)
                    .bearing(location.bearing)

                    .build()

                //      mMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(currentPlace), 200, null)


                mMap!!.moveCamera(CameraUpdateFactory.newCameraPosition(currentPlace))

            }

        }

    }


    fun pushHandler(push: Push) {
        if (activity == null)
            return
        showPushIndicator()
        when {
            push.type.equals(Push.bokingRequest, true) -> try {
                if (childFragmentManager != null && childFragmentManager.findFragmentById(R.id.childFrame) != null) {

                    return

                }

                if (MyUtils.isInternetAvailable(activity as MainActivity)) {

                    if (activity != null) {

                        pushRefData = Gson().fromJson(push.RefData, RefData::class.java)


                        var latplongPickup = pushRefData.bookingPickuplatlong!!.split(",").toTypedArray()
                        var latplongDrop = pushRefData.bookingDroplatlong!!.split(",").toTypedArray()

                        if (latplongPickup.size == 2) {

                            var latPickup = latplongPickup[0]
                            var longPickup = latplongPickup[1]


                            try {
                                destinationLatLong = LatLng(latPickup.toDouble(), longPickup.toDouble())

                                drawDirectionRoute(

                                    LatLng(currentLocation?.latitude!!, currentLocation?.longitude!!),
                                    destinationLatLong!!
                                )
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    }


                } else {
                    (activity as MainActivity).showSnackBar(resources.getString(R.string.error_common_network))
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            push.type.equals("TripCancelledByUser", true) -> {

                removeFragment(RiderDetailsNavigation::class.java.name)
                removeFragment(RiderStartNavigation::class.java.name)
                MyUtils.showMessageOK(activity!!, "Your trip cancelled by customer.", "Trip Cancelled",
                    DialogInterface.OnClickListener { dialog, which ->
                        dialog.dismiss()
                        clearMap()
                        setBookingId("-1", "No Trip")

                        (activity as MainActivity).bottomNavigation.visibility = View.VISIBLE
                    })


            }
            push.type.equals("Logout", true) -> doAsync {
                sessionManager.clear_login_session()
                if (db != null) {
                    db!!.navigationLogDao().deleteAll()
                }
                uiThread {
                    if (activity != null)
                        MyUtils.startActivity(activity as MainActivity, SplashActivity::class.java, true)
                }
            }
        }

    }

    override fun onRiderTripDropStartNavigation(navigationLatLong1: LatLng) {
        navigationLatLong = navigationLatLong1
        askDrawOverPermission()
    }


    override fun onRideStartEnd(from: String, riderStartNavigationDialog: RiderStartNavigation) {


        if (from.contains("Start", true)) {
            var i = Intent(activity as MainActivity, OtpVerificationActivity::class.java)
            i.putExtra("bookingId", bookingId)
            i.putExtra("currentLatLong", "" + currentLocation!!.latitude + "," + currentLocation!!.longitude)
            activity!!.startActivityForResult(i, 1)
            activity!!.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)

            bookingAcceptFirebase("TripStarted")

        } else {
            var endTripAddress: String = ""


            mBottomSheetDialog = BottomSheetDialog(activity as MainActivity)
            var sheetView = layoutInflater.inflate(R.layout.complete_trip_confirmation_layout, null)
            mBottomSheetDialog!!.setContentView(sheetView)

            mBottomSheetDialog!!.show()
            sheetView.text_endcancel_trip.text = resources.getString(R.string.sure_want_to_complete_trip)
            sheetView.completeTripConfirmationCardView.visibility = View.VISIBLE
            sheetView.btn_cancel.setOnClickListener {
                mBottomSheetDialog!!.dismiss()

            }
            sheetView.btn_endtrip.setOnClickListener {

                clearMap()


                getNavigationLogList(bookingId, riderStartNavigationDialog)


            }
        }
    }

    private fun clearMap() {
        /* if (circlePolygon != null) circlePolygon?.remove()
                if (destinationMarker != null) destinationMarker?.remove()
                if (blackPolyline != null) blackPolyline?.remove()
                if (greyPolyLine != null) greyPolyLine?.remove()


             *//*   mMap!!.clear()
        mCurrLocationMarker=null*/
        if (mMap == null && activity == null)
            return

        mMap!!.clear()

        mMap!!.animateCamera(
            CameraUpdateFactory.newLatLngZoom(
                LatLng(currentLocation?.latitude!!, currentLocation?.longitude!!), defaultZoom
            )
        )
        mCurrLocationMarker = null
        updateCarMaker(currentLocation!!)

    }


    private fun polylinePickUp() {

        if (currentLocation == null && destinationLatLong == null)
            return


        clearMap()


        /* var builder = LatLngBounds.Builder()
         for (point in points) {
             builder.include(point)
         }*/


        //  var bounds = builder.build()
        //  mMap!!.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 200), 2000, null)

        if (locationLogType.equals("PickUp", true) || locationLogType.equals("Reached", true) || locationLogType.equals(
                "Enroute",
                true
            )
        ) {

        } else {

            MyUtils.playSoundVibrate(activity!!)

            try {
                mediaPlayer = MediaPlayer.create(context, R.raw.booking_ring)
                mediaPlayer?.start()
            } catch (e: Exception) {
            }
            circlePolygon = Googleutil.drawCircle(
                mMap!!,
                LatLng(currentLocation?.latitude!!, currentLocation?.longitude!!)

            )
            animateCamera(
                LatLng(
                    currentLocation?.latitude!!,
                    currentLocation?.longitude!!
                ), 12f
            )
        }






        Handler().postDelayed({

            greyPolyLine = mMap!!.addPolyline(
                getPolyLineOption(
                    activity!!.resources.getColor(R.color.colorPrimary),
                    points,
                    12f
                )
            )
            blackPolyline = mMap!!.addPolyline(getPolyLineOption(Color.BLACK, points, 8f))
            polyLineAnimation(greyPolyLine!!, blackPolyline!!)

            destinationMarker = mMap!!.addMarker(
                MarkerOptions().position(this.destinationLatLong!!).title("Customer is here")
            )
            destinationMarker!!.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.rider_map_pin))


        }, 1500)



        if (locationLogType.equals("PickUp", true) && startTiripData != null) {
            tripAccept(startTiripData!!, directionRouteResponse!!)
        } else if (locationLogType.equals("Enroute", true) || locationLogType.equals("Reached", true)) {

        } else {
            try {
                if (MyUtils.isInternetAvailable(activity as MainActivity)) {
                    if (activity != null && pushRefData != null && directionRouteResponse != null) {
                        var acceptTime = 120
                        if (driverData.setting.isNotEmpty()) acceptTime =
                            driverData.setting[0].settingsDisplayBookingRequestMinute!!.toInt() * 60


                        //acceptTime=300
                        replaceFragment(
                            AcceptTripDialogFragment.newInstance(
                                acceptTime,
                                pushRefData,
                                directionRouteResponse!!
                            )
                        )


                    }
                } else {
                    (activity as MainActivity).showSnackBar(resources.getString(R.string.error_common_network))
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }


    }


    override fun onSuccessfullyCancelTrip() {
        waitStartTime = null
        removeFragment(RiderDetailsNavigation::class.java.name)
        clearMap()
        setBookingId("-1", "No Trip")
        (activity as MainActivity).bottomNavigation.visibility = View.VISIBLE

        bookingAcceptFirebase("TripCancelledByDriver")


    }

    override fun onRideStartSuccess(startTripData: StartTripData) {
        waitStartTime = null
        tripStart(startTripData)
    }

    private fun tripStart(startTripData: StartTripData) {
        if (startTripData != null) {

            updateToFirebase?.startTripData = startTripData
            var droplatlong: LatLng? = null

            var latplongDRop = startTripData.bookingDroplatlong.split(",").toTypedArray()

            if (!startTripData!!.bookingType.equals(
                    "Outstation",
                    true
                ) && startTripData!!.bookingSubType.equals("Round Trip", true)
            ) {

                if (sessionManager.tripType.equals("roundTripStart", true)) {

                    latplongDRop = startTripData.bookingPickuplatlong.split(",").toTypedArray()
                    startTripData.bookingTravelRoute = null
                }

            }

            waitingTime = 0L
            waitStartTime = null
            if (startTripData!!.statusID.equals("7"))
                setBookingId(startTripData.bookingID, "Reached")
            else
                setBookingId(startTripData.bookingID, "Enroute")

            if (latplongDRop.size == 2) {

                var latDrop = latplongDRop[0]
                var longDrop = latplongDRop[1]
                try {
                    droplatlong = LatLng(latDrop.toDouble(), longDrop.toDouble())
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                clearMap()
                if (droplatlong != null) {
                    destinationLatLong = droplatlong
                    destinationMarker = mMap!!.addMarker(
                        MarkerOptions().position(droplatlong)
                    )
                    destinationMarker!!.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.rider_map_pin))
                }
                centerMap()
                if (startTripData.bookingTravelRoute != null && startTripData.bookingTravelRoute!!.isNotEmpty()) {
                    points = ArrayList<LatLng>()

                    try {
                        var routes: List<Route> = startTripData.bookingTravelRoute!!
                        if (routes != null && routes.isNotEmpty()) {
                            for (it in routes[0]?.legs!!) {
                                it?.steps!!.forEach { step ->

                                    points.addAll(
                                        Googleutil.decodePoly(step?.polyline!!.points.toString())
                                    )
                                }
                            }
                            updateToFirebase?.routePoints = points
                            blackPolyline = mMap!!.addPolyline(getPolyLineOption(Color.BLACK, points, 8f))
                            //       runCarOnRoute()

                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                } else {


                    drawDirectionRoute(
                        LatLng(currentLocation?.latitude!!, currentLocation?.longitude!!),
                        destinationLatLong!!
                    )
                }
            }

        }
    }


    private fun cancelTripReasonMaster() {
        MyUtils.showProgressDialog(activity as MainActivity, "Please wait...")

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", "0")
            jsonObject.put("reasonFor", "Driver")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)
        val cancelTripModel = ViewModelProviders.of(this@MainFragment).get(CancelTripMasterModel::class.java)
        cancelTripModel.getCancelTripMaster(activity as MainActivity, false, jsonArray.toString())
            .observe(this@MainFragment,
                Observer<List<CancelTrip>> { canceltripPojo ->
                    if (canceltripPojo != null && canceltripPojo.size > 0) {
                        if (canceltripPojo[0].status.equals("true")) {

                            MyUtils.dismissProgressDialog()

                            cancelTripMaster.clear()
                            cancelTripMaster.addAll(canceltripPojo[0].data)
                            var canceltripBottomSheetFragment = CanceltripBottomSheetFragment

                            canceltripBottomSheetFragment.newInstance(canceltripPojo[0].data, bookingId)
                                .show(childFragmentManager, "CancelTrip")

                        } else {
                            MyUtils.dismissProgressDialog()

                            (activity as MainActivity).showSnackBar(canceltripPojo[0].message)
                        }

                    } else {
                        MyUtils.dismissProgressDialog()
                        (activity as MainActivity).errorMethod()
                    }
                })

    }

    fun insertNavigationLatLongDb(location: Location) {
        if (db != null) {
            doAsync {
                var navigationLog = NavigationLog(

                    bookingId,
                    location.latitude,
                    location.longitude,
                    Calendar.getInstance().time
                )

                db?.navigationLogDao()!!.insert(navigationLog)
            }
        }
    }

    fun getNavigationLogList(bookingId: String, riderStartNavigationDialog: RiderStartNavigation) {


        doAsync {

            navigationLogList = db?.navigationLogDao()!!.loadAllByBookigId(bookingId)
            var travelDistance = 0.0


            for (i in 1 until navigationLogList.size) {

                travelDistance += SphericalUtil.computeDistanceBetween(
                    LatLng(navigationLogList[i - 1].latitude!!, navigationLogList[i - 1].longitude!!), LatLng(
                        navigationLogList[i].latitude!!,
                        navigationLogList[i].longitude!!
                    )
                )
            }

            var mins = 0L
            if (navigationLogList.size >= 2) {

                var mills =
                    navigationLogList[navigationLogList.size - 1].logDate!!.time - navigationLogList[0].logDate!!.time


                mins = (mills / (1000 * 60)) % 60


            }
            var endTripAddress = Googleutil.getMyLocationAddress(
                activity!!, currentLocation?.latitude!!,
                currentLocation?.longitude!!
            )
            uiThread {
                if (endTripAddress == null)
                    endTripAddress = ""
                getEndTrip(
                    mins,
                    navigationLogList,
                    travelDistance,
                    riderStartNavigationDialog,
                    bookingId,
                    endTripAddress
                )

            }

        }

    }


    private fun getEndTrip(
        bookingDurationActual: Long,
        navigationLogList: List<NavigationLog>,
        travelDistance: Double,
        riderStartNavigationDialog: RiderStartNavigation,
        BookID: String,
        endTripAddress: String
    ) {


        MyUtils.showProgressDialog(activity!!, "Please wait...")
        var encodeLatlongList = Googleutil.encode(navigationLogList)
        val df = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

        val datetime = df.format(Calendar.getInstance().time)

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("driverID", driverData.driverID)
            jsonObject.put("bookingID", BookID)
            jsonObject.put("bookingDurationActual", bookingDurationActual)
            jsonObject.put("bookingTripEndTime", datetime)
            jsonObject.put("bookingTraveledRoute", encodeLatlongList)
            jsonObject.put("bookingDropAddress", endTripAddress)
            jsonObject.put("bookingDistanceAtTripEnd", travelDistance / 1000)
            jsonObject.put(
                "bookingTripEndlatlong",
                "" + currentLocation!!.latitude + "," + currentLocation!!.longitude
            )



            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)

        var startTiripModel1 = ViewModelProviders.of(this@MainFragment).get(StartTiripModel::class.java)
        startTiripModel1.UpdateBookindStatus(activity!!, false, jsonArray.toString(), "endTrip")
            .observe(this@MainFragment,
                Observer {
                    if (it != null && it.isNotEmpty()) {
                        if (it[0].status.equals("true", true)) {
                            MyUtils.dismissProgressDialog()

                            clearMap()
                            setBookingId("-1", "No Trip")

                            bookingAcceptFirebase("TripCompleted")


                            updateToFirebase?.startTripData = null
                            if (mBottomSheetDialog != null) {
                                mBottomSheetDialog!!.dismiss()
                            }
                            removeFragment(RiderStartNavigation::class.java.name)


                            var trripDetailsFragment = TripDetailsFragment()
                            var bundle = Bundle()
                            bundle.putSerializable("endBookingData", it[0].data[0] as Serializable)
                            trripDetailsFragment.arguments = bundle
                            (activity as MainActivity).navigateTo(trripDetailsFragment, true)
                            doAsync {
                                if (db != null)
                                    db!!.navigationLogDao().deleteAll()
                            }

                        } else {
                            MyUtils.dismissProgressDialog()

                            Toast.makeText(activity!!, it[0].message, Toast.LENGTH_LONG).show()


                        }

                    } else {
                        if (activity != null) {
                            MyUtils.dismissProgressDialog()
                            if (MyUtils.isInternetAvailable(activity!!)) {
                                Toast.makeText(
                                    activity!!,
                                    activity!!.getString(R.string.error_crash_error_message),
                                    Toast.LENGTH_LONG
                                ).show()

                            } else {
                                Toast.makeText(
                                    activity!!,
                                    activity!!.getString(R.string.error_common_network),
                                    Toast.LENGTH_LONG
                                ).show()
                            }


                        }
                    }
                })


    }

    fun getReachTrip(acceptRequestData: StartTripData) {
        MyUtils.showProgressDialog(activity!!, "Please wait...")
        val df = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

        val datetime = df.format(Calendar.getInstance().time)
        Log.e("date", datetime)

        val jsonArray = JSONArray()

        val jsonObject = JSONObject()
        try {
            jsonObject.put("driverID", sessionManager.get_Authenticate_User().driverID)
            jsonObject.put("bookingID", bookingId)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)

        val startTiripModel = ViewModelProviders.of(this@MainFragment).get(StartTiripModel::class.java)
        startTiripModel.UpdateBookindStatus(activity as MainActivity, false, jsonArray.toString(), "reach")
            .observe(this@MainFragment,
                Observer<List<StartTripPojo>> { endTripPojo ->
                    if (endTripPojo != null && endTripPojo.isNotEmpty()) {
                        if (endTripPojo[0].status.equals("true", true)) {
                            MyUtils.dismissProgressDialog()
                            removeFragment(RiderDetailsNavigation::class.java.name)

                            reachCustomer(endTripPojo[0].data[0])
                        } else {
                            MyUtils.dismissProgressDialog()

                            (activity as MainActivity).showSnackBar(endTripPojo[0].message)

                        }

                    } else {
                        MyUtils.dismissProgressDialog()

                        (activity as MainActivity).errorMethod()
                    }
                })


    }


    fun animateCamera(latlong: LatLng, zoomLevel: Float) {
        mMap!!.animateCamera(
            CameraUpdateFactory.newLatLngZoom(
                latlong, zoomLevel
            )
        )
    }

    fun replaceFragment(fragment: Fragment) {
        val transaction = childFragmentManager
            .beginTransaction()
            .setCustomAnimations(
                R.anim.slide_in_left,
                R.anim.slide_out_right,
                R.anim.slide_in_right,
                R.anim.slide_out_left
            )
            .replace(R.id.childFrame, fragment, fragment::class.java.name)




        try {
            transaction.commit()
        } catch (e: Exception) {
            transaction.commitAllowingStateLoss()

        }
    }

    fun removeFragment(fragmentName: String) {
        val fragment = childFragmentManager.findFragmentByTag(fragmentName)
        if (fragment != null)
            childFragmentManager.beginTransaction().remove(fragment).commit()
    }

    fun showPushIndicator() {
        try {
            if (activity != null) {
                if (!sessionManager.NotificationRead) {
                    (activity as MainActivity).showBadge(R.id.profileBottomMenu)
                } else {
                    (activity as MainActivity).removeBadge(R.id.profileBottomMenu)
                }
            }
        } catch (e: Exception) {
        }
    }

    fun openEasyRideDialog() {
        if (activity != null && currentLocation != null) {
            streetPickupBottomSheet =
                StreetPickupBottomSheet.newInstance(
                    activity!!,
                    currentLocation?.longitude.toString(),
                    currentLocation?.latitude.toString()
                )
            streetPickupBottomSheet?.show(childFragmentManager, "StreentPickup")
            streetPickupBottomSheet?.setStreetPicupListener(this)
        }
    }

    override fun onStreetTripBook(streetPickUpData: StartTripData) {
        tripStart(streetPickUpData)
        replaceFragment(RiderStartNavigation.newInstance(streetPickUpData))
    }

    override fun onReRoute(startTripData: StartTripData, dropLatLng: LatLng) {

        drawDirectionRoute(LatLng(currentLocation?.latitude!!, currentLocation?.longitude!!), dropLatLng)

    }

    fun onProfileUpdate(imageName: String) {


        //profileImagview.setImageURI(RestClient.image_driver_url + imageName)

    }


    fun setBookingId(bookID: String, logType: String) {
        bookingId = bookID
        locationLogType = logType

        defaultZoom = if (bookID == "-1")
            16.0f
        else
            18f
        if (bookID == "-1") {
            sessionManager.tripType = ""
            currentLocationCardView.visibility = View.GONE
            speed_time_layout.visibility = View.GONE
        }


    }

    fun updateInternetUI(isInternetAvailable: Boolean) {
        if (isInternetAvailable) {
            if (msgButton != null && msgButton.visibility == View.VISIBLE)
                MyUtils.collapse(msgButton)

        } else {
            if (msgButton != null && msgButton.visibility != View.VISIBLE)
                MyUtils.expand(msgButton)
        }

    }

    fun backPressed() {
        if (streetPickupBottomSheet != null && streetPickupBottomSheet?.isVisible!!) {
            streetPickupBottomSheet?.dismiss()
            streetPickupBottomSheet = null
        } else {
            if (activity != null && activity is MainActivity) {
                (activity as MainActivity).showexit()
            }
        }
    }

    fun setSpeedTime(location: Location) {
        if (speed_time_layout == null) {
            return
        }

        if (bookingId != "-1" && speed_time_layout != null) {

            if (speed_time_layout.visibility != View.VISIBLE) {
                speed_time_layout.visibility = View.VISIBLE
                if (currentLocationCardView != null)
                    currentLocationCardView.visibility = View.GONE

            }

            if (oldLocation != null

            ) {


                waitingTime += location.time - oldLocation?.time!!


            }



            speed_time_layout.setSpeed(Googleutil.getSpeed(location, oldLocation))

            if (locationLogType.equals("Reached", true))
                speed_time_layout.setTimeLabel("Waiting Time")
            else
                speed_time_layout.setTimeLabel("Traveled Time")




            speed_time_layout.setTime(waitingTime)
            oldLocation = location

        } else {
            if (speed_time_layout.visibility == View.VISIBLE) {
                speed_time_layout.visibility = View.GONE
            }
        }
    }


    fun pausePlayer() {
        try {
            if (mediaPlayer != null && mediaPlayer!!.isPlaying) {
                mediaPlayer!!.stop()
            }
        } catch (ex: java.lang.Exception) {
        }
    }

    private fun call(phoneNumber: String) {

        try {
            if (ActivityCompat.checkSelfPermission(
                    activity!!,
                    Manifest.permission.CALL_PHONE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                this@MainFragment.requestPermissions(
                    arrayOf(Manifest.permission.CALL_PHONE),
                    501
                )

            } else if (phoneNumber.isNotEmpty()) {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:" + phoneNumber)

                activity!!.startActivity(callIntent)

            }

        } catch (e: ActivityNotFoundException) {
            Log.e("call", "Call failed", e)
        }

    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 501 && grantResults.isNotEmpty()) {
            call(phoneNumber)
        }
        if (requestCode == 502) {
            if (grantResults.isEmpty()) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.

            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission was granted.
                requestLocationUpdateBackground()
            }
        }
    }

    fun checkPermissions(): Boolean {

        return PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(
            activity!!,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
    }

    fun requestPermissions() {
        var shouldProvideRationale =
            ActivityCompat.shouldShowRequestPermissionRationale(
                activity!!,
                Manifest.permission.ACCESS_FINE_LOCATION
            );

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i("Location Permission", "Displaying permission rationale to provide additional context.");

            this@MainFragment.requestPermissions(
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                502
            )
            /*Snackbar.make(

                    R.string.permission_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    10);
                        }
                    })
                    .show();*/
        } else {
            Log.i("Location Permission", "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            this@MainFragment.requestPermissions(
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                502
            )
        }
    }

    fun requestLocationUpdateBackground() {
        if (!checkPermissions()) {
            requestPermissions();
        } else {
            mService?.requestLocationUpdates()
        }
    }

    fun removeLocationUpdateBackground() {
        mService?.removeLocationUpdates()
    }


    class MyReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            var location: Location? = intent?.getParcelableExtra(LocationUpdatesService.EXTRA_LOCATION)
            if (location != null) {
                Log.d("Location BG", location.toString())
            }
        }

    }

    /*fun getTripStatusFromFirebase(bookingId: String) {
        val myTopPostsQuery = database.child("Booking/${bookingId}")

        myTopPostsQuery.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                var push = dataSnapshot.getValue(Push::class.java)
                if (push != null)
                    pushHandler(push!!)
            }

        })


    }*/

    fun bookingAcceptFirebase(type: String) {

      /*  var acceptRequestData = updateToFirebase?.startTripData
        if (acceptRequestData != null && !acceptRequestData.bookingID.isNullOrEmpty()) {
            var push = Push()
            push.refKey = acceptRequestData?.bookingID
            push.type = type

            // var refData=RefData(bookingDropAddress = acceptRequestData.bookingDropAddress,bookingDroplatlong = acceptRequestData.bookingDroplatlong,bookingPickupAddress = acceptRequestData.bookingPickupAddress,bookingID = acceptRequestData.bookingID,bookingPickuplatlong = acceptRequestData.bookingPickuplatlong,categoryID = acceptRequestData.categoryID,userID = acceptRequestData.userID,bookingDurationEst = acceptRequestData.bookingDurationEst)


            push.RefData = Gson().toJson(acceptRequestData)
            database.child("Booking").child(acceptRequestData.bookingID).setValue(push)
            getTripStatusFromFirebase(acceptRequestData.bookingID)
        }*/
    }


}

