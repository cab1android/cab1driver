package com.cab1.driver.fragment


import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.PopupMenu
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.cab1.driver.MainActivity
import com.cab1.driver.R
import com.cab1.driver.api.RestClient
import com.cab1.driver.model.TripGraphHistorylistModel
import com.cab1.driver.pojo.EarningHistoryGraphData
import com.cab1.driver.util.CustomMarkerView
import com.cab1.driver.util.MyUtils
import com.example.admin.myapplication.SessionManager
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.bottom_sheet_trip_earing_layout.*
import kotlinx.android.synthetic.main.fragment_earnings.*
import kotlinx.android.synthetic.main.nointernetconnection.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class EarningsFragment : Fragment(), View.OnClickListener {


    var v: View? = null
    var weekDay = 0
    var month = 0
    var day=0
   lateinit var tripGraphHistorylistModel:TripGraphHistorylistModel
    var earningHistoryGraphData :ArrayList<EarningHistoryGraphData?>? = ArrayList()
    var startDate:String=""
    var endDate:String=""
    val colorList: ArrayList<Int> = ArrayList()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        if (v == null) {
            v = inflater.inflate(R.layout.fragment_earnings, container, false)

            var c = Calendar.getInstance()
            c.firstDayOfWeek = Calendar.MONDAY

            weekDay = c.get(Calendar.WEEK_OF_YEAR)
            month = c.get(Calendar.MONTH)
            day=c.get(Calendar.DATE)

        }
        return v

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)



        tripGraphHistorylistModel=ViewModelProviders.of(this@EarningsFragment).get(TripGraphHistorylistModel::class.java)

        viewAllText.setOnClickListener(this)
        (activity as AppCompatActivity).setSupportActionBar(toolbar)

        (activity as AppCompatActivity).supportActionBar?.setDisplayShowCustomEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)
        (activity as MainActivity).bottomNavigation.visibility = View.GONE
        (activity as MainActivity).setBottomNavMenuSelected(R.id.earningBottomMenu)

        tvVerifcationTitle.text = "Earnings"
        toolbar.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }





        if(earningHistoryGraphData!!.isEmpty()) {
            setTripData(null)
            getCurrentWeekDate(weekDay)


        }




        arrowLeft_ImageView.setOnClickListener {
            when {
                durationTypeTextview.text.toString().equals("Weekly", true) -> {
                    weekDay -= 1
                    getCurrentWeekDate(weekDay)
                }
                durationTypeTextview.text.toString().equals("Monthly", true) -> {
                    month -= 1
                    getCurrentMonthDate(month)
                }
                else ->
                {
                    day -=1
                    getCurrentDayDate(day)
                }
            }
        }
        arrowRight_ImageView.setOnClickListener {
            when {
                durationTypeTextview.text.toString().equals("Weekly", true) -> {

                    weekDay += 1
                    getCurrentWeekDate(weekDay)
                }
                durationTypeTextview.text.toString().equals("Monthly", true) -> {
                    month += 1
                    getCurrentMonthDate(month)
                }
                else ->
                {
                    day +=1
                    getCurrentDayDate(day)
                }
            }
        }
        durationTypeTextview.setOnClickListener {
            //Creating the instance of PopupMenu
            val popup = PopupMenu(activity!!, durationTypeTextview)

            //Inflating the Popup using xml file
            popup.menuInflater.inflate(R.menu.popup_menu_earning, popup.menu)

            //registering popup with OnMenuItemClickListener
            popup.setOnMenuItemClickListener { item ->
                if(durationTypeTextview!=null)
                   durationTypeTextview.text = item.title

                var c = Calendar.getInstance()
                c.firstDayOfWeek = Calendar.MONDAY

                weekDay = c.get(Calendar.WEEK_OF_YEAR)
                month = c.get(Calendar.MONTH)
                day=c.get(Calendar.DATE)

                if (durationTypeTextview.text.toString().equals("Weekly", true)) {
                    getCurrentWeekDate(weekDay)
                } else if (durationTypeTextview.text.toString().equals("Monthly", true)) {
                    getCurrentMonthDate(month)
                }
                else
                {
                    getCurrentDayDate(day)
                }

                true
            }

            popup.show()//showing popup menu
        }
        btnRetry.setOnClickListener {
            getTripHistory(startDate,endDate)
        }
    }

    private fun setChartData() {
        chartLayout.visibility=View.VISIBLE
        val valsComp1 = ArrayList<BarEntry>()

        colorList.clear()


        for (i in 0 until earningHistoryGraphData!!.size)
        {




            var bardata = BarEntry(i.toFloat(), earningHistoryGraphData!![i]?.totalAmount?.toFloat()!!)


            valsComp1.add(bardata)


            //colorList.add(getRandomColor());

            colorList.add(Color.parseColor("#E8BC00"))


        }

        when {
            durationTypeTextview.text.toString().equals("Weekly", true) -> setChartWeekly(valsComp1)
            durationTypeTextview.text.toString().equals("Monthly", true) -> setChartMonthly(valsComp1)
            else -> setChartDialy(valsComp1)
        }




    }

    private fun setChartDialy(valsComp1: ArrayList<BarEntry>) {

        if(chartDaily.visibility!=View.VISIBLE)
            chartDaily.visibility=View.VISIBLE

        chart.visibility=View.GONE
        chartMonthly.visibility=View.GONE
        val dataSet = BarDataSet(valsComp1, "Earnings")

        dataSet.setDrawValues(false)
        dataSet.colors = colorList


        val data = BarData(dataSet)








        val xAxis = chartDaily.xAxis
        xAxis.setDrawLabels(false)
        xAxis.valueFormatter = IAxisValueFormatter { value, axis ->
            try {




                //    quarters[value.toInt()]


                        MyUtils.formatDate(earningHistoryGraphData!![value.toInt()]?.date!!, "yyyy-MM-dd", "dd-MMM-yyyy")



            } catch (e: Exception) {
                e.printStackTrace()
                return@IAxisValueFormatter ""
            }
        }
        chartDaily.setGridBackgroundColor(128)
        chartDaily.setBorderColor(255)
        chartDaily.axisRight.isEnabled = false
        val leftAxis = chartDaily.axisLeft
        leftAxis.isEnabled = true
        leftAxis.setLabelCount(7,true)
        leftAxis.textColor = resources.getColor(R.color.text_primary)
        leftAxis.valueFormatter = MyYAxisValueFormatter()
        chartDaily.axisRight.setDrawLabels(false)
        chartDaily.axisLeft.setDrawLabels(true)
        chartDaily.axisLeft.axisMinimum=30f
        chartDaily.xAxis.isEnabled = true
        chartDaily.setDrawGridBackground(false)//enable this too
        chartDaily.xAxis.position = XAxis.XAxisPosition.BOTTOM
        chartDaily.xAxis.setDrawLabels(true)


        chartDaily.axisLeft.setDrawGridLines(false)
        chartDaily.axisRight.setDrawGridLines(false)
        chartDaily.xAxis.setDrawGridLines(false)

        chartDaily.setOnChartValueSelectedListener(object : OnChartValueSelectedListener {
            override fun onValueSelected(e: Entry?, h: Highlight?) {
                setTripData(earningHistoryGraphData!![e?.x!!.toInt()])
            }

            override fun onNothingSelected() {
                // Logger.e("Nothing selection")
            }

        })


        chartDaily.legend.isEnabled = false
        chartDaily.description.isEnabled = false
        chartDaily.data = data

        chartDaily . xAxis.granularity = 1f
        chartDaily. xAxis.setCenterAxisLabels(true)
        chartDaily.barData.barWidth=0.1f
        chartDaily.xAxis.setLabelCount(earningHistoryGraphData!!.size,true)


        chartDaily.setNoDataText("No Data Found.")
        chartDaily.setNoDataTextColor(R.color.colorPrimary)

        val mv = CustomMarkerView(activity!!, R.layout.custom_marker_graph_layout)
        chartDaily.marker = mv
        chartDaily.invalidate()
        chartDaily.animateXY(2000, 2000)
        chartDaily.highlightValue(chart.x, 0)
    }

    private fun setChartMonthly(valsComp1: ArrayList<BarEntry>) {


        if(chartMonthly.visibility!=View.VISIBLE)
            chartMonthly.visibility=View.VISIBLE

        chartDaily.visibility=View.GONE
        chart.visibility=View.GONE

        val dataSet = BarDataSet(valsComp1, "Earnings")

        dataSet.setDrawValues(false)
        dataSet.colors = colorList


        val data = BarData(dataSet)





        val xAxis = chartMonthly.xAxis
        xAxis.setDrawLabels(false)
        xAxis.valueFormatter = IAxisValueFormatter { value, axis ->
            try {




                //    quarters[value.toInt()]

                        MyUtils.formatDate(earningHistoryGraphData!![value.toInt()]?.date!!,"yyyy-MM-dd","dd-MMM")



            } catch (e: Exception) {
                e.printStackTrace()
                return@IAxisValueFormatter ""
            }
        }
        chartMonthly.setGridBackgroundColor(128)
        chartMonthly.setBorderColor(255)
        chartMonthly.axisRight.isEnabled = false
        val leftAxis = chartMonthly.axisLeft
        leftAxis.isEnabled = true
        leftAxis.setLabelCount(7,true)
        leftAxis.textColor = resources.getColor(R.color.text_primary)
        leftAxis.valueFormatter = MyYAxisValueFormatter()
        chartMonthly.axisRight.setDrawLabels(false)
        chartMonthly.axisLeft.setDrawLabels(true)
        chartMonthly.axisLeft.axisMinimum=30f
        chartMonthly.xAxis.isEnabled = true
        chartMonthly.setDrawGridBackground(false)//enable this too
        chartMonthly.xAxis.position = XAxis.XAxisPosition.BOTTOM
        chartMonthly.xAxis.setDrawLabels(true)


        chartMonthly.axisLeft.setDrawGridLines(false)
        chartMonthly.axisRight.setDrawGridLines(false)
        chartMonthly.xAxis.setDrawGridLines(false)

        chartMonthly.setOnChartValueSelectedListener(object : OnChartValueSelectedListener {
            override fun onValueSelected(e: Entry?, h: Highlight?) {
                setTripData(earningHistoryGraphData!![e?.x!!.toInt()])
            }

            override fun onNothingSelected() {
                // Logger.e("Nothing selection")
            }

        })


        chartMonthly.legend.isEnabled = false
        chartMonthly.description.isEnabled = false
        chartMonthly.data = data

        chartMonthly.setVisibleXRangeMaximum(7f)
        chartMonthly. xAxis.setCenterAxisLabels(false)
        chartMonthly.xAxis.setLabelCount(7,false)


        chartMonthly.setNoDataText("No Data Found.")
        chartMonthly.setNoDataTextColor(R.color.colorPrimary)

        val mv = CustomMarkerView(activity!!, R.layout.custom_marker_graph_layout)
        chartMonthly.marker = mv
        chartMonthly.invalidate()
        chartMonthly.animateXY(2000, 2000)
        chartMonthly.highlightValue(chart.x, 0)
    }

    private fun setChartWeekly(valsComp1: ArrayList<BarEntry>) {

        if(chart.visibility!=View.VISIBLE)
            chart.visibility=View.VISIBLE

        chartDaily.visibility=View.GONE
        chartMonthly.visibility=View.GONE


        val dataSet = BarDataSet(valsComp1, "Earnings")

        dataSet.setDrawValues(false)
        dataSet.colors = colorList


        val data = BarData(dataSet)



        val xAxis = chart.xAxis
        xAxis.setDrawLabels(false)
        xAxis.valueFormatter = IAxisValueFormatter { value, axis ->
            try {




                //    quarters[value.toInt()]

                        earningHistoryGraphData!![value.toInt()]?.day.toString().capitalize()


            } catch (e: Exception) {
                e.printStackTrace()
                return@IAxisValueFormatter ""
            }
        }
        chart.setGridBackgroundColor(128)
        chart.setBorderColor(255)
        chart.axisRight.isEnabled = false
        val leftAxis = chart.axisLeft
        leftAxis.isEnabled = true
        leftAxis.setLabelCount(7,true)
        leftAxis.textColor = resources.getColor(R.color.text_primary)
        leftAxis.valueFormatter = MyYAxisValueFormatter()
        chart.axisRight.setDrawLabels(false)
        chart.axisLeft.setDrawLabels(true)
        chart.axisLeft.axisMinimum=30f
        chart.xAxis.isEnabled = true
        chart.setDrawGridBackground(false)//enable this too
        chart.xAxis.position = XAxis.XAxisPosition.BOTTOM
        chart.xAxis.setDrawLabels(true)


        chart.axisLeft.setDrawGridLines(false)
        chart.axisRight.setDrawGridLines(false)
        chart.xAxis.setDrawGridLines(false)

        chart.setOnChartValueSelectedListener(object : OnChartValueSelectedListener {
            override fun onValueSelected(e: Entry?, h: Highlight?) {
                setTripData(earningHistoryGraphData!![e?.x!!.toInt()])
            }

            override fun onNothingSelected() {
                // Logger.e("Nothing selection")
            }

        })


        chart.legend.isEnabled = false
        chart.description.isEnabled = false
        chart.data = data

        chart.setVisibleXRangeMaximum(7f)
        chart. xAxis.setCenterAxisLabels(false)
        chart.xAxis.setLabelCount(7,false)


        chart.setNoDataText("No Data Found.")
        chart.setNoDataTextColor(R.color.colorPrimary)

        val mv = CustomMarkerView(activity!!, R.layout.custom_marker_graph_layout)
        chart.marker = mv
        chart.invalidate()
        chart.animateXY(2000, 2000)
        chart.highlightValue(chart.x, 0)
    }

    class MyYAxisValueFormatter : IAxisValueFormatter {


        private val mFormat: java.text.DecimalFormat = java.text.DecimalFormat("###,###,##0")

        init {

            // format values to 1 decimal digit
        }

        override fun getFormattedValue(value: Float, axis: AxisBase): String {
            // "value" represents the position of the label on the axis (x or y)
            return if (value <= 0)
                ""
            else
               "Rs. "+ mFormat.format(value.toDouble())
        }


    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.viewAllText -> {
                (activity as MainActivity).navigateTo(TripHistoryFragment(), true)

            }

        }
    }

    fun getCurrentWeekDate(week: Int) {
        val c = Calendar.getInstance()
        c.firstDayOfWeek = Calendar.MONDAY
        c.set(Calendar.DAY_OF_WEEK, c.firstDayOfWeek)
        c.set(Calendar.WEEK_OF_YEAR, week)
        durationTextview.text = SimpleDateFormat("MMM dd").format(c.time)
        startDate=SimpleDateFormat("yyyy-MM-dd").format(c.time)
        c.add(Calendar.DAY_OF_MONTH, 6)
        durationTextview.text = durationTextview.text.toString() + "-" + SimpleDateFormat("MMM dd").format(c.time)
        endDate=SimpleDateFormat("yyyy-MM-dd").format(c.time)

        getTripHistory(startDate,endDate)
    }

    fun getCurrentMonthDate(month: Int) {
        val c = Calendar.getInstance()
     c.set(Calendar.DATE, 1)



// set actual maximum date of previous month

        c.set(Calendar.MONTH, month)
        durationTextview.text = SimpleDateFormat("MMM-yy").format(c.time)
        startDate=SimpleDateFormat("yyyy-MM-dd").format(c.time)
        c.set(Calendar.DATE,     c.getActualMaximum(Calendar.DAY_OF_MONTH))
        endDate=SimpleDateFormat("yyyy-MM-dd").format(c.time)
        getTripHistory(startDate,endDate)
    }

    fun getCurrentDayDate(day: Int) {
        val c = Calendar.getInstance()
        c.set(Calendar.DATE, day)



// set actual maximum date of previous month


        durationTextview.text = SimpleDateFormat("dd-MMM-yy").format(c.time)
        startDate=SimpleDateFormat("yyyy-MM-dd").format(c.time)

        endDate=SimpleDateFormat("yyyy-MM-dd").format(c.time)
        getTripHistory(startDate,endDate)
    }


    private fun getTripHistory(startDate:String,endDate:String) {

        chartLayout.visibility=View.GONE
        nointernetMainRelativelayout.visibility = View.GONE


            relativeprogressBar.visibility = View.VISIBLE



        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {

        jsonObject.put("driverID", SessionManager(activity!!).get_Authenticate_User().driverID)

            jsonObject.put("StartDate", startDate)
            jsonObject.put("EndDate", endDate)
            jsonObject.put("Type", "Weekly")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)

        tripGraphHistorylistModel.getTripHistory(activity as MainActivity,  jsonArray.toString())
            .observe(this@EarningsFragment,androidx.lifecycle.Observer {


                        tripHistoryPojo     ->
                    if (tripHistoryPojo != null && tripHistoryPojo.isNotEmpty())
                    {

                        //   remove progress item

                        nointernetMainRelativelayout.visibility = View.GONE
                        relativeprogressBar.visibility = View.GONE




                        if (tripHistoryPojo[0].status.equals("true")) {


                            earningHistoryGraphData!!.clear()
                            earningHistoryGraphData=tripHistoryPojo[0].data
                            setChartData()
                            tvTotalEarning.text="Rs. "+MyUtils.priceFormat(tripHistoryPojo[0].driverTotalEarnings!!)
                            setTripData(earningHistoryGraphData!![0]!!)


                        }
                        relativeprogressBar.visibility = View.GONE



                    } else {


                        if (activity != null) {
                            setTripData(null)
                            relativeprogressBar.visibility = View.GONE
                            chartLayout.visibility=View.GONE

                            try {
                                nointernetMainRelativelayout.visibility = View.VISIBLE
                                if (MyUtils.isInternetAvailable(activity!!)) {
                                    nointernetImageview.setImageDrawable(activity!!.getDrawable(R.drawable.ic_warning_black_24dp))
                                    nointernettextview.text = (activity!!.getString(R.string.error_crash_error_message))
                                } else {
                                    nointernetImageview.setImageDrawable(activity!!.getDrawable(R.drawable.ic_signal_wifi_off_black_24dp))

                                    nointernettextview.text = (activity!!.getString(R.string.error_common_network))
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                        }

                    }

            })

    }

    fun setTripData(earningHistoryGraphData: EarningHistoryGraphData?)
    {
        if(earningHistoryGraphData==null)
        {
            tvTripDate.text = SimpleDateFormat("EEE, MMM dd yyyy").format(Calendar.getInstance().time)
            tvTripTotalTime.text =  "0 hrs Online"
            tvTripTotalPriceValue.text = "Rs. 0.00"
            tvNoOfTripValue.text="0 Trip"
        }
        else {

            tvTripDate.text = MyUtils.formatDate(earningHistoryGraphData.date!!, "yyyy-MM-dd", "EEE, MMM dd yyyy")
            try {
                if(earningHistoryGraphData.onlineMinutes?.toInt()!! >=60 )
                tvTripTotalTime.text = earningHistoryGraphData.onlineMinutes?.toInt()?.div(60).toString()+" hrs Online"
                else
                    tvTripTotalTime.text = earningHistoryGraphData.onlineMinutes+" minute Online"
            } catch (e: Exception) {
            }

            tvTripTotalPriceValue.text = "Rs. " + MyUtils.priceFormat(earningHistoryGraphData.totalAmount.toString())
            try {
                if (earningHistoryGraphData.totalTrip.toString().toInt() <= 1)
                    tvNoOfTripValue.text = earningHistoryGraphData.totalTrip + " Trip"
                else
                    tvNoOfTripValue.text = earningHistoryGraphData.totalTrip + " Trips"
            } catch (e: Exception) {
            }
        }

    }

}
