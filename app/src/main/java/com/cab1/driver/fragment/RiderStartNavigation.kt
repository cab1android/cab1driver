package com.cab1.driver.fragment

import android.Manifest
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.cab1.driver.MainActivity
import com.cab1.driver.R
import com.cab1.driver.api.RestClient
import com.cab1.driver.iterfaces.DriverStatusListener
import com.cab1.driver.pojo.StartTripData
import com.cab1.driver.util.MyUtils
import com.example.admin.myapplication.SessionManager
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.rider_trip_location_status_layout.*


class RiderStartNavigation : Fragment(), View.OnClickListener {

    private var mListener: DriverStatusListener? = null
    private val MY_PERMISSIONS_REQUEST_CALL_PHONE = 501
    var strfrom = ""

    var phoneNumber = ""
    val ACCEPT_REQUEST_DATA = "accepetRequestData"
    var accepetRequestData: StartTripData? = null
    lateinit var startTiripData: StartTripData
    lateinit var sessionManager: SessionManager

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ll_rider_start_navigation -> {
                if (accepetRequestData!!.statusID.equals("7", true)) {
                    (activity as MainActivity).showSnackBar("Your trip not started yet, Please start trip.")
                } else {
                    try {
                        if (mListener != null) {
                            var dropLatLong = accepetRequestData!!.bookingDroplatlong!!.split(",").toTypedArray()
                            if (accepetRequestData!!.bookingType.equals(
                                    "Outstation",
                                    true
                                ) && accepetRequestData!!.bookingSubType.equals(
                                    "Round Trip",
                                    true
                                ) && sessionManager.tripType == "RoundTripStart"
                            ) {

                                dropLatLong = accepetRequestData!!.bookingPickuplatlong!!.split(",").toTypedArray()

                          }


                            if (dropLatLong.size == 2) {
                                mListener!!.onRiderTripDropStartNavigation(
                                    LatLng(
                                        dropLatLong[0].toDouble()!!,
                                        dropLatLong[1].toDouble()
                                    )
                                )
                            } else {
                                try {
                                    Toast.makeText(activity,getString(R.string.error_crash_error_message), Toast.LENGTH_LONG).show()
                                } catch (e: Exception) {
                                }
                            }

                        }
                    } catch (e: Exception) {
                    }
                }
            }
            R.id.ll_end_trip -> {


                if (tv_trip_startend.text.toString() == activity!!.resources.getString(R.string.end_trip_oneWay)) {
                    sessionManager.tripType = "roundTripStart"
                    tv_trip_startend.text = (activity!!.resources.getString(R.string.start_trip_roundway)).toString()
                    setFromToRoundTrip()

                } else if (tv_trip_startend.text.toString() == activity!!.resources.getString(R.string.start_trip_roundway)) {
                    sessionManager.tripType = "roundTripEnd"
                    tv_trip_startend.text = (activity!!.resources.getString(R.string.end_trip)).toString()
                    if (mListener != null) {
                        mListener!!.onRideStartSuccess(startTiripData)
                    }

                } else {


                    if (mListener != null) {
                        mListener!!.onRideStartEnd(tv_trip_startend.text.toString().trim(), this)
                    }
                }

            }
            R.id.btnCallRiderTrip -> {
                call(phoneNumber)
            }
            R.id.image_down_arrow -> {
                setExpandCollapseView()
            }
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.rider_trip_location_status_layout, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        sessionManager = SessionManager(activity!!)

        ll_rider_start_navigation.setOnClickListener(this)
        btnCallRiderTrip.setOnClickListener(this)
        ll_end_trip.setOnClickListener(this)
        image_down_arrow.setOnClickListener(this)
        (activity as MainActivity).bottomNavigation.visibility = View.GONE

        /*  strfrom=arguments!!.getString("from","")

          if (strfrom.equals("Start", true)) {
              tv_trip_startend.text = resources.getString(R.string.start_trip)
          } else {
              tv_trip_startend.text = resources.getString(R.string.end_trip)

          }*/

        if (arguments != null) {
            accepetRequestData = arguments?.getSerializable(ACCEPT_REQUEST_DATA) as StartTripData

        }
        if (accepetRequestData != null) {
            tv_BookingType.text = accepetRequestData!!.bookingType.capitalize()
                       if (accepetRequestData!!.bookingSubType.contains("street", true))
                tv_BookingType.text = resources.getString(R.string.street_pickup)

            if (!accepetRequestData!!.statusID.equals("5", true))
            {


                if (accepetRequestData!!.bookingType.equals(
                        "Outstation",
                        true
                    ) && accepetRequestData!!.bookingSubType.equals("Round Trip", true)
                ) {
                    setTextRoundTrip()
                } else {
                    tv_trip_startend.text = resources.getString(R.string.start_trip)
                }
            } else {


                tv_trip_startend.text = resources.getString(R.string.end_trip)


            }

            tv_confirmTrip_customerName.text = accepetRequestData!!.userFullName
            if (accepetRequestData!!.userFullName!!.isEmpty()) {
                tv_confirmTrip_customerName.text = activity!!.getString(R.string.unknown)
            }
            img_confirmTrip_Driver.setImageURI(RestClient.image_customer_url + accepetRequestData!!.userProfilePicture)



            try {
                kmTv.text = String.format("%,.1f", accepetRequestData!!.bookingDistanceEst!!.toFloat())
            } catch (e: Exception) {
                kmTv.text = accepetRequestData!!.bookingDistanceEst
            }

            try {
                if (accepetRequestData!!.bookingType.equals("Rental", true)) {
                    rentalTimeLayout.visibility = View.VISIBLE
                    timeTv.text = "" + accepetRequestData!!.bookingDurationEst!!.toInt() / 60
                }
            } catch (e: Exception) {
            }
            originAddressTv.text = accepetRequestData!!.bookingPickupAddress
            destinationAddressTv.text = accepetRequestData!!.bookingDropAddress
            if (destinationAddressTv.text.isEmpty())
                destinationAddressTv.text = "Unknown"

            setFromToRoundTrip()



            phoneNumber = accepetRequestData!!.userMobile!!

            if (accepetRequestData!!.bookingType.equals("rental", true)) {
                dottedLine.visibility = View.GONE
                dotted.visibility = View.GONE
                to_baseline.visibility = View.GONE
                to_title.visibility = View.GONE
                destinationAddressTv.visibility = View.GONE
            } else {
                dottedLine.visibility = View.VISIBLE
                dotted.visibility = View.VISIBLE
                to_baseline.visibility = View.VISIBLE
                to_title.visibility = View.VISIBLE
                destinationAddressTv.visibility = View.VISIBLE

            }

        }

    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            val parent = parentFragment
            mListener = if (parent != null) {
                parent as DriverStatusListener
            } else {
                context as DriverStatusListener
            }
        } catch (e: Exception) {
        }
    }

    override fun onDetach() {
        mListener = null
        super.onDetach()
    }

    companion object {

        fun newInstance(acceptRequestData: StartTripData): RiderStartNavigation = RiderStartNavigation().apply {
            arguments = Bundle().apply {
                putSerializable(ACCEPT_REQUEST_DATA, acceptRequestData)
            }
        }
    }


    private fun call(phoneNumber: String) {

        try {
            if (ActivityCompat.checkSelfPermission(
                    activity!!,
                    Manifest.permission.CALL_PHONE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                this@RiderStartNavigation.requestPermissions(
                    arrayOf(Manifest.permission.CALL_PHONE),
                    MY_PERMISSIONS_REQUEST_CALL_PHONE
                )

            } else {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:$phoneNumber")




                activity!!.startActivity(callIntent)

            }

        } catch (e: ActivityNotFoundException) {
            Log.e("call", "Call failed", e)
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == MY_PERMISSIONS_REQUEST_CALL_PHONE && grantResults.isNotEmpty()) {
            call(phoneNumber)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {


            if (accepetRequestData!!.bookingType.equals(
                    "Outstation",
                    true
                ) && accepetRequestData!!.bookingSubType.equals("Round Trip", true)
            ) {
                sessionManager.tripType = "oneWayStart"
                setTextRoundTrip()
            } else {

                tv_trip_startend.text = resources.getString(R.string.end_trip)
            }
            if (data != null) {
                if (data.hasExtra("startTiripData")) {


                    startTiripData = data.getSerializableExtra("startTiripData") as StartTripData
                    accepetRequestData=startTiripData
                    if (mListener != null) {
                        mListener!!.onRideStartSuccess(startTiripData)
                    }

                }
            }
        }
    }

    private fun setTextRoundTrip() {
        when {
            sessionManager.tripType.isEmpty() -> tv_trip_startend.text = resources.getString(R.string.start_trip)
            sessionManager.tripType.equals("oneWayStart", true) -> tv_trip_startend.text =
                resources.getString(R.string.end_trip_oneWay)
            sessionManager.tripType.equals("oneWayEnd", true) -> tv_trip_startend.text =
                resources.getString(R.string.start_trip_roundway)

            sessionManager.tripType.equals("roundTripStart", true) -> tv_trip_startend.text =
                resources.getString(R.string.start_trip_roundway)
            else -> tv_trip_startend.text = resources.getString(R.string.end_trip)
        }
    }

    private fun setFromToRoundTrip() {
        if (accepetRequestData!!.bookingType.equals(
                "Outstation",
                true
            ) && accepetRequestData!!.bookingSubType.equals(
                "Round Trip",
                true
            ) && sessionManager.tripType.equals("roundTripStart", true)
        ) {
            destinationAddressTv.text = accepetRequestData!!.bookingPickupAddress
            originAddressTv.text = accepetRequestData!!.bookingDropAddress
            if (originAddressTv.text.isEmpty())
                originAddressTv.text = "Unknown"
        }

    }
    private fun setExpandCollapseView() {
        if(ll_confirmTrip_startTrip.visibility==View.VISIBLE)
        {
            image_down_arrow.setImageResource(R.drawable.ic_arrow_down)
            MyUtils.collapse(ll_confirmTrip_startTrip)
        }
        else
        {
            image_down_arrow.setImageResource(R.drawable.ic_up_arrow)
            MyUtils.expand(ll_confirmTrip_startTrip)
        }
    }
}
