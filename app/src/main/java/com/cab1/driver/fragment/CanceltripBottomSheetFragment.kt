package com.cab1.driver.fragment

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.cab1.driver.MainActivity
import com.cab1.driver.R
import com.cab1.driver.adapter.CancelTripAdapter
import com.cab1.driver.api.RestClient
import com.cab1.driver.iterfaces.DriverStatusListener
import com.cab1.driver.model.CancelTiripModel
import com.cab1.driver.pojo.CancelTripData
import com.cab1.driver.pojo.CommonStatus
import com.cab1.driver.util.MyUtils
import com.example.admin.myapplication.SessionManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment


import kotlinx.android.synthetic.main.bottom_sheet_common_layout.*
import kotlinx.android.synthetic.main.cancel_trip_bottomsheet_dialog.*
import kotlinx.android.synthetic.main.complete_trip_confirmation_layout.view.*


import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable

class CanceltripBottomSheetFragment : BottomSheetDialogFragment(), View.OnClickListener {

    private var mListener: DriverStatusListener? = null
    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var cancelTripAdapter: CancelTripAdapter
    lateinit var sessionManager: SessionManager

    private var cancelTripMaster = ArrayList<CancelTripData>()
    var reasonID1: String = ""
    var reasonTitle: String = ""
    var bookingId: String = ""
    var reasonChargeWhom1: String = ""
    var reasonCharge1: String = ""
    var reasonPayment1: String = ""
    override fun onClick(v: View?) {
        when (v!!.id) {

            R.id.btnCancelTrip -> {

                if (reasonID1 != null && reasonID1.length>0) {
                    var mBottomSheetDialog = BottomSheetDialog(activity as MainActivity)
                    var sheetView = layoutInflater.inflate(R.layout.complete_trip_confirmation_layout, null)
                    mBottomSheetDialog.setContentView(sheetView)
                    mBottomSheetDialog.show()
                    sheetView.completeTripConfirmationCardView.visibility = View.VISIBLE
                    sheetView.text_endcancel_trip.text = resources.getString(R.string.sure_want_to_cancel_trip)

                    sheetView.btn_cancel.setOnClickListener {
                        mBottomSheetDialog.dismiss()
                    }
                    sheetView.btn_endtrip.setOnClickListener {
                        mBottomSheetDialog.dismiss()
                        getCancelTripApi(reasonID1, reasonTitle, bookingId,reasonChargeWhom1,reasonCharge1,reasonPayment1)

                    }
                } else {
                  Toast.makeText(activity,"Please select cancel for reason",Toast.LENGTH_LONG).show()
                }
            }

        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)

    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)

        return super.onCreateDialog(savedInstanceState)

    }

    override fun setupDialog(dialog: Dialog, style: Int) {
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var view = inflater.inflate(R.layout.cancel_trip_bottomsheet_dialog, container, false)

        return view
    }

    @SuppressLint("WrongConstant")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sessionManager = SessionManager(activity as MainActivity)


        headerText.text = resources.getString(R.string.cancel_trip)
        headerText.gravity = Gravity.START
        backIconImageView.visibility = View.VISIBLE
        btnCancelTrip.setOnClickListener(this)
        if (arguments != null) {
            cancelTripMaster = arguments?.getSerializable("CancelTripData") as ArrayList<CancelTripData>
            bookingId = arguments!!.getString("bookingId")
        }
        layoutManager = LinearLayoutManager(context as MainActivity, LinearLayout.VERTICAL, false)


        /*val list = ArrayList<String>()

        list.add("Please tell us why you want to cancel")
        list.add("Driver denied to go to destination")
        list.add("Driver denied to come to pickup")
        list.add("Expected a shorter wait time")
        list.add("Unable to contact driver")
        list.add("My reason is not listed")*/



        cancelTripRecyclerView.layoutManager = layoutManager
        cancelTripAdapter = CancelTripAdapter(
            context as MainActivity,
            cancelTripMaster,
            object : CancelTripAdapter.OnItemClickListener {
                override fun itemClick(
                    pos: Int,
                    reason: String,
                    reasonID: String,
                    reasonChargeWhom: String,
                    reasonCharge: String,
                    reasonPayment:String
                ) {
                    reasonTitle = reason
                    reasonID1 = reasonID
                    reasonChargeWhom1 = reasonChargeWhom
                    reasonCharge1 = reasonCharge
                    reasonPayment1 = reasonPayment
                }


            })
        cancelTripRecyclerView.adapter = cancelTripAdapter

        backIconImageView.setOnClickListener {
            dismiss()
        }

        dialogCloseIcon.setOnClickListener {
            dismiss()
        }

    }


    companion object {
        fun newInstance(data: List<CancelTripData>, bookingId: String): CanceltripBottomSheetFragment =
            CanceltripBottomSheetFragment().apply {
                arguments = Bundle().apply {
                    putSerializable("CancelTripData", data as Serializable)
                    putString("bookingId", bookingId)
                }
            }


    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val parent = parentFragment
        mListener = if (parent != null) {
            parent as DriverStatusListener
        } else {
            context as DriverStatusListener
        }
    }




    private fun getCancelTripApi(
        reasonID: String,
        reasonTitle: String,
        bookingId: String,
        reasonChargeWhom1: String,
        reasonCharge1: String,
        reasonPayment1: String
    ) {
        btnCancelTrip.startAnimation()
        var driverData = sessionManager.get_Authenticate_User()

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("driverID", driverData.driverID)
            jsonObject.put("bookingID", bookingId)
            jsonObject.put("reasonID", reasonID)
            jsonObject.put("reasonPayment", reasonPayment1)
            jsonObject.put("reasonChargeWhom", reasonChargeWhom1)
            jsonObject.put("reasonCharge", reasonCharge1)
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)
        val cancelTripModel = ViewModelProviders.of(this).get(CancelTiripModel::class.java)
        cancelTripModel.cancelTrip(activity as MainActivity, jsonArray.toString())
            .observe(this, Observer<List<CommonStatus>> { canceltripPojo ->
                if (canceltripPojo != null && canceltripPojo.size > 0) {
                    if (canceltripPojo[0].status.equals("true")) {
                        dismiss()
                        btnCancelTrip.revertAnimation()
                        Toast.makeText(activity,canceltripPojo[0].message,Toast.LENGTH_LONG).show()
                        if(mListener!=null)
                            mListener!!.onSuccessfullyCancelTrip()

                    } else {

                        btnCancelTrip.revertAnimation()

                        Toast.makeText(activity,canceltripPojo[0].message,Toast.LENGTH_LONG).show()


                    }

                } else {

                    btnCancelTrip.revertAnimation()
                    try {
                        if (!MyUtils.isInternetAvailable(activity as MainActivity)) {
                            Toast.makeText(activity,resources.getString(R.string.error_common_network),Toast.LENGTH_LONG).show()
                        } else {
                            Toast.makeText(activity,resources.getString(R.string.error_crash_error_message),Toast.LENGTH_LONG).show()

                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            })
    }
}