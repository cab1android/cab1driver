package com.cab1.driver.fragment



import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cab1.driver.MainActivity
import com.cab1.driver.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.bottom_sheet_trip_earing_layout.*


class EarningTripHistoryFragment : BottomSheetDialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.trip_history_earing_bottomsheet_dialog, container, false)

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

       // (activity as MainActivity).setBottomNavMenuSelected(1)


        viewAllText.setOnClickListener {
            (activity as MainActivity).navigateTo(TripHistoryFragment(),true)
        }

        img_ArrowRight.setOnClickListener {

            (activity as MainActivity).navigateTo(TripHistoryFragment(),true)
        }

    }
}
