package com.cab1.driver.fragment


import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.cab1.driver.MainActivity
import com.cab1.driver.R
import com.cab1.driver.SplashActivity
import com.cab1.driver.api.RestClient
import com.cab1.driver.model.LoginModel
import com.cab1.driver.pojo.DriverData
import com.cab1.driver.util.MyUtils
import com.example.admin.myapplication.SessionManager
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.already_logged_in_layout.*
import kotlinx.android.synthetic.main.fragment_change_password.*
import kotlinx.android.synthetic.main.login_driver.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject


/**
 * A simple [Fragment] subclass.
 *
 */
class ChangePasswordFragment : Fragment() {


    private var v: View? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (v == null) {
            v = inflater.inflate(R.layout.fragment_change_password, container, false)
        }
        // Inflate the layout for this fragment
        return v
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)



        (activity as AppCompatActivity).setSupportActionBar(toolbar)

        (activity as AppCompatActivity).supportActionBar?.setDisplayShowCustomEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)

        toolbar.setNavigationIcon(R.drawable.back_icon)
        tvVerifcationTitle.text = "Change Password"
        toolbar.setNavigationOnClickListener {
                  (activity as MainActivity).onBackPressed()

        }


        btnChangePwdSubmit.setOnClickListener {

            validate()

        }

    }


    private fun validate() {

        newPwd_text_input.error=null
        confirmPwd_edit_text.error=null
        when {
            newPwd_edit_text.text!!.isEmpty() -> newPwd_text_input.error="Enter New Password."
            confirmPwd_edit_text.text!!.isEmpty() -> confirmPwd_text_input.error= "Enter Confirm Password."
            newPwd_edit_text.text!!.length<6 -> newPwd_text_input.error="Password should have minimum 6 character."
            confirmPwd_edit_text.text!!.length<6 -> confirmPwd_text_input.error="Password should have minimum 6 character."
            newPwd_edit_text.text.toString().trim() != confirmPwd_edit_text.text.toString().trim() -> confirmPwd_text_input.error=
                    "New password and confirm password mismatch."
            else -> {

                newPwd_text_input.error=null
                confirmPwd_text_input.error=null
                MyUtils.hideKeyboard1(activity!!)
                changePassword()
            }
        }

    }




    private fun changePassword() {

        btnChangePwdSubmit.startAnimation()

        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {

            jsonObject.put("driverID", SessionManager(activity!!).get_Authenticate_User().driverID)
            jsonObject.put("driverDeviceType", "Android")
            jsonObject.put("apiType", RestClient.apiType)

            jsonObject.put("apiVersion", RestClient.apiVersion)
                      jsonObject.put("driverPassword", newPwd_edit_text.text.toString().trim())
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)
        val loginModel = ViewModelProviders.of(this@ChangePasswordFragment).get(LoginModel::class.java)
        loginModel.getLogin(activity!!, false, jsonArray.toString(),"ChangePassword").observe(this@ChangePasswordFragment,
            Observer<List<DriverData>> { changePasswordPojo ->
                if(btnChangePwdSubmit!=null && activity!=null)
                {
                    btnChangePwdSubmit.revertAnimation {
                        btnChangePwdSubmit.text="Submit"
                    }
                }
                if (changePasswordPojo != null && changePasswordPojo.isNotEmpty()) {
                    if (changePasswordPojo[0].status.equals("true", true)) {


                        if(activity!=null) {

                            MyUtils.showMessageOK(activity!!, "Your password changed successfully.Please login again.", "Change Password",
                                DialogInterface.OnClickListener { dialog, which ->
                                    SessionManager(activity!!).clear_login_session()
                                    dialog.dismiss()
                                    var intent = Intent(activity!!, SplashActivity::class.java)
                                    intent.flags=Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                                    startActivity(intent)
                                    activity!!.finish()
                                    activity!!. overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)

                                })

                        }
                    } else {
                        if(activity!=null)
                            MyUtils.showSnackbar(
                                activity as MainActivity,
                                changePasswordPojo[0].message,
                                changePwd_mainLL
                            )

                    }


                } else {
                    img_login.revertAnimation()
                    if(activity!=null&& activity!! is MainActivity)
                    {
                        (activity as MainActivity).errorMethod()
                    }
                }
            })
    }


}
