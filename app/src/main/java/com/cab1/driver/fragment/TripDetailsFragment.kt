package com.cab1.driver.fragment


import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.cab1.driver.MainActivity
import com.cab1.driver.R
import com.cab1.driver.api.RestClient
import com.cab1.driver.pojo.StartTripData
import com.cab1.driver.util.MyUtils
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_trip_details.*
import kotlinx.android.synthetic.main.toolbar.*

/**
 * A simple [Fragment] subclass.
 *
 */
class TripDetailsFragment : Fragment() {

    private var v: View? = null
    lateinit var endTiripData: StartTripData

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (v == null) {
            v = inflater.inflate(R.layout.fragment_trip_details, container, false)
        }
        return v
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        toolbar.setNavigationIcon(R.drawable.back_icon)
        tvVerifcationTitle.text = activity!!.resources.getString(R.string.trip_details)
        (activity as AppCompatActivity).setSupportActionBar(toolbar)

        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar!!.title = ""
        (activity as MainActivity).bottomNavigation.visibility = View.GONE
        toolbar.setNavigationOnClickListener {
            (activity as AppCompatActivity).onBackPressed()
        }
        if (arguments != null) {
            endTiripData = arguments!!.getSerializable("endBookingData") as StartTripData

        }

        setData()

        btnDone.setOnClickListener {
            (activity as MainActivity).navigateTo(MainFragment(), true)

        }
    }

    @SuppressLint("SetTextI18n")
    private fun setData() {
        if (endTiripData != null) {
            tvBookingId.text = "Booking Id: " + endTiripData.bookingNo
            if (endTiripData.bookingSubType.contains("Street Pickup", true)) {
                tvBookingType.text = "Booking Type: " + resources.getString(R.string.street_pickup)

            } else {
                tvBookingType.text = "Booking Type: " + endTiripData.bookingType
            }


            val bookDate = MyUtils.formatDate(endTiripData.bookingDate, "yyyy-MM-dd HH:mm:ss", "EEE,MMM d, hh:mm a")
            tvBookingDate.text = bookDate
            tvPaymentOptionValue.text = endTiripData.bookingPaymentMode
            imgCallDriverIcon.setImageURI(RestClient.image_customer_url + endTiripData.userProfilePicture)
            tvDriverName.text = endTiripData.userFullName
            if (endTiripData.userFullName.isEmpty()) {
                tvDriverName.text = activity!!.getString(R.string.unknown)
            }
            tvDriverValue.text = "" +
                    endTiripData.bookingDistanceActual + " Kms"
            tvestimatedamountValue.text = "Rs. " + MyUtils.priceFormat(endTiripData.bookingEstimatedAmount)
            var totalAmount= 0.0

            try {
                totalAmount = endTiripData.bookingSubAmount.toDouble() + endTiripData.bookingGST.toDouble()
                tvActualAmountValue.text = "Rs. " + MyUtils.priceFormat(totalAmount.toString())
            } catch (e: Exception) {
                tvActualAmountValue.text = "Rs. " + MyUtils.priceFormat(endTiripData.bookingSubAmount)
            }



            if (endTiripData.bookingPaymentMode.equals("Cash", true)) {


                tvCreaditWalletAmountValue.text = "Rs. " + MyUtils.priceFormat(endTiripData.bookingActualAmount)

            } else {
                tvCreaditWalletAmountValue.text = "Rs. 0.00"

            }

            tvFromLocationAddress.text = endTiripData.bookingPickupAddress
            tvdestinationLocationAddress.text = endTiripData.bookingDropAddress
            tvBaseFareValue.text = "Rs. " + MyUtils.priceFormat(endTiripData.optionBasefare.toString())

            tvTimeValue.text = if (!endTiripData.bookingDurationActual.isNullOrEmpty()) {
                "" + endTiripData.bookingDurationActual.toFloat().toInt() + " mins"
            } else {
                "0.0"
            }
            tvSubtotalValue.text = "Rs. " + MyUtils.priceFormat(endTiripData.bookingGST)
            tvTotalValue.text = "Rs. " + MyUtils.priceFormat(endTiripData.bookingSubAmount)
            tvFinalAmountVal.text = "Rs. " + MyUtils.priceFormat(endTiripData.bookingActualAmount)
            tvPerkmValue.text = "Rs. " + MyUtils.priceFormat(endTiripData.optionAdditionalKmCharge.toString())
            tvPerTimeValue.text = "Rs. " + MyUtils.priceFormat(endTiripData.optionPerMinCharge.toString())


            try {

                tvDiscountValue.text = "Rs. " + MyUtils.priceFormat(endTiripData.bookingDiscount)
            } catch (e: Exception) {

            }


            tvCouponCode.text = endTiripData!!.couponCode
            fromDateDisplyTv_completedTrip.text = MyUtils.formatDate(
                endTiripData.bookingTripStartTime,
                "yyyy-MM-dd HH:mm:ss",
                "EEE,dd MMM"
            )

            fromTimeDisplayTv_completedTrip.text =
                MyUtils.formatDate(endTiripData.bookingTripStartTime, "yyyy-MM-dd HH:mm:ss", "hh:mm a")


            toDateDisplayTv_completedTrip.text = MyUtils.formatDate(
                endTiripData.bookingTripEndTime,
                "yyyy-MM-dd HH:mm:ss",
                "EEE,dd MMM"
            )
            toTimeDisplayTv_completedTrip.text =
                MyUtils.formatDate(endTiripData.bookingTripEndTime, "yyyy-MM-dd HH:mm:ss", "hh:mm a")

        }

    }

}
