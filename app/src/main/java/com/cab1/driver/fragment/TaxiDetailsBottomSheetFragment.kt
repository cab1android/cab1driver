package com.cab1.driver.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cab1.driver.R
import com.example.admin.myapplication.SessionManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.bottom_sheet_common_layout.*
import kotlinx.android.synthetic.main.taxi_details_bottomsheet_dialog.*


class TaxiDetailsBottomSheetFragment : BottomSheetDialogFragment() {

    val labelTaxiModel= "Taxi Model: "
    val labelTaxiNumber= "Taxi Number: "
    val labelLicenceNo= "Licence No: "
    lateinit var sessionManager: SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.taxi_details_bottomsheet_dialog, container, false)

        return view
    }

    @SuppressLint("WrongConstant")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        var drivercarlist=SessionManager(activity!!).get_Authenticate_User().drivercars

        headerText.text = "Taxi Details"
        headerText.gravity = Gravity.CENTER
        backIconImageView.visibility = View.INVISIBLE
        if (drivercarlist!=null && drivercarlist.size>0) {

            tvTaxiModelText.text = "$labelTaxiModel "+drivercarlist[0].drvcarModel
            tvTaxiNumberlText.text = "$labelTaxiNumber "+drivercarlist[0].drvcarNo
            tvLicenceNoText.text = "$labelLicenceNo "+drivercarlist[0].drvcarID
        }

        backIconImageView.setOnClickListener {
            dismiss()
        }

        dialogCloseIcon.setOnClickListener {
            dismiss()
        }

    }
}