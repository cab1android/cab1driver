package com.cab1.driver.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cab1.driver.MainActivity
import com.cab1.driver.R
import com.cab1.driver.adapter.NotificationAdapter
import com.cab1.driver.api.RestClient
import com.cab1.driver.model.NotificationlistModel
import com.cab1.driver.pojo.NotificationlistData
import com.cab1.driver.pojo.NotificationlistPojo
import com.cab1.driver.util.MyUtils
import com.example.admin.myapplication.SessionManager
import kotlinx.android.synthetic.main.fragment_notification.*
import kotlinx.android.synthetic.main.nodafound.*
import kotlinx.android.synthetic.main.nointernetconnection.*
import kotlinx.android.synthetic.main.progressbar.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*


class NotificationFragment : Fragment() {


    private lateinit var v: View
    private lateinit var notificationAdapter: NotificationAdapter
    private var y: Int = 0
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0
    private var firstVisibleItemPosition: Int = 0
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var isLoading = false
    private var isLastpage = false
    lateinit var sessionManager: SessionManager
    private var notificationListData = ArrayList<NotificationlistData?>()
    var pageNo = 0
    lateinit var notificationlistModel: NotificationlistModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.fragment_notification, container, false)
        return v
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sessionManager = SessionManager(activity as MainActivity)


        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowCustomEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)

        tvVerifcationTitle.text = "Notifications"
        toolbar.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }
        notificationlistModel =
                ViewModelProviders.of(this@NotificationFragment).get(NotificationlistModel::class.java)

        linearLayoutManager = LinearLayoutManager(activity)
        notificationAdapter = NotificationAdapter(activity as MainActivity, object : NotificationAdapter.OnItemClick {

            override fun onClicklisneter(pos: Int, name: String) {

            }

        }, notificationListData)
        notification_RecycleView.layoutManager = linearLayoutManager
        notification_RecycleView.adapter = notificationAdapter
        getNotificationList()


        notification_RecycleView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                y = dy
                visibleItemCount = linearLayoutManager.getChildCount()
                totalItemCount = linearLayoutManager.getItemCount()
                firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition()
                if (!isLoading && !isLastpage) {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= 10
                    ) {

                        isLoading = true
                        getNotificationList()
                    }
                }
            }
        })

        btnRetry.setOnClickListener {
            getNotificationList()
        }
    }

    private fun getNotificationList() {
        noDatafoundRelativelayout.visibility = View.GONE
        nointernetMainRelativelayout.visibility = View.GONE

        if (pageNo == 0) {
            relativeprogressBar.visibility = View.VISIBLE
            notificationListData!!.clear()
            notificationAdapter.notifyDataSetChanged()
        } else {
            relativeprogressBar.visibility = View.GONE
            notification_RecycleView.visibility = (View.VISIBLE)
            notificationListData!!.add(null)
            notificationAdapter.notifyItemInserted(notificationListData!!.size - 1)
        }
        val jsonArray = JSONArray()
        val jsonObject = JSONObject()
        try {
            jsonObject.put("loginuserID", "0")
            jsonObject.put("logindriverID", sessionManager.get_Authenticate_User().driverID)
            jsonObject.put("page", pageNo)
            jsonObject.put("pagesize", "10")
            jsonObject.put("apiType", RestClient.apiType)
            jsonObject.put("apiVersion", RestClient.apiVersion)

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        jsonArray.put(jsonObject)

        notificationlistModel.getNotificationList(activity as MainActivity, false, jsonArray.toString())
            .observe(this@NotificationFragment,
                Observer<List<NotificationlistPojo>>
                { notificationlistPojo ->
                    if (notificationlistPojo != null && notificationlistPojo.isNotEmpty()) {
                        isLoading = false
                        //   remove progress item
                        noDatafoundRelativelayout.visibility = View.GONE
                        nointernetMainRelativelayout.visibility = View.GONE
                        relativeprogressBar.visibility = View.GONE


                        if (pageNo > 0) {
                            notificationListData!!.removeAt(notificationListData!!.size - 1)
                            notificationAdapter.notifyItemRemoved(notificationListData!!.size)
                        }
                        if (notificationlistPojo[0].status.equals("true")) {

                            sessionManager.NotificationRead=true
                            if (pageNo == 0)
                                notificationListData.clear()

                            notificationListData.addAll(notificationlistPojo[0].data)
                            notificationAdapter.notifyDataSetChanged()
                            pageNo += 1
                            if (notificationlistPojo[0].data!!.size < 10) {
                                isLastpage = true
                            }

                        }
                        relativeprogressBar.visibility = View.GONE

                        if (notificationListData!!.size == 0) {
                            noDatafoundRelativelayout.visibility = View.VISIBLE
                            notification_RecycleView.visibility = View.GONE

                        } else {
                            noDatafoundRelativelayout.visibility = View.GONE
                            notification_RecycleView.visibility = View.VISIBLE

                        }

                    } else {


                        if (activity != null) {
                            relativeprogressBar.visibility = View.GONE


                            try {
                                nointernetMainRelativelayout.visibility = View.VISIBLE
                                if (MyUtils.isInternetAvailable(activity!!)) {
                                    nointernetImageview.setImageDrawable(activity!!.getDrawable(R.drawable.ic_warning_black_24dp))
                                    nointernettextview.text = (activity!!.getString(R.string.error_crash_error_message))
                                } else {
                                    nointernetImageview.setImageDrawable(activity!!.getDrawable(R.drawable.ic_signal_wifi_off_black_24dp))

                                    nointernettextview.text = (activity!!.getString(R.string.error_common_network))
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                        }

                    }
                })
    }

}
