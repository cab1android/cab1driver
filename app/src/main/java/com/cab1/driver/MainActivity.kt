package com.cab1.driver

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.*
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.cab1.driver.fragment.EarningsFragment
import com.cab1.driver.fragment.MainFragment
import com.cab1.driver.fragment.MyProfileDriverFragment
import com.cab1.driver.iterfaces.NavigationHost
import com.cab1.driver.pojo.Push
import com.cab1.driver.service.AlarmReceiver
import com.cab1.driver.service.NetworkChangeReceiver
import com.cab1.driver.util.MyUtils
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import java.io.*


class MainActivity : AppCompatActivity(), NavigationHost, MyProfileDriverFragment.ProfileUpdateListener {


    private var doubleBackToExitPressedOnce = false
    private var snackBarParent: View? = null
    var push: Push? = null
    protected var mWakeLock: PowerManager.WakeLock? = null
    var networkStateChangeReceiver = NetworkChangeReceiver()
    @SuppressLint("WakelockTimeout", "InvalidWakeLockTag")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            AlarmReceiver.setAlarm(true)
        }

        var pm = getSystemService(Context.POWER_SERVICE) as PowerManager
        mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK or PowerManager.ON_AFTER_RELEASE, "WakeUpScreen")
        mWakeLock!!.acquire()

        addViewSnackBar()
        if (intent != null) {
            if (intent.hasExtra("Push")) {
                push = intent.getSerializableExtra("Push") as Push

            }
        }
        if (savedInstanceState == null) {

            var mainFragment = MainFragment()
            if (push != null) {
                var bundle = Bundle()

                bundle.putSerializable("Push", push)

                mainFragment.arguments = bundle
            }


            var transaction = supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, mainFragment, MainFragment::class.java.name)
            try {
                transaction.commit()
            } catch (e: Exception) {
                transaction.commitAllowingStateLoss()

            }
        }


        bottom_navigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.homeBottomMenu -> {
                    if (getCurrentFragment() !is MainFragment) {
                        navigateTo(MainFragment(), true)
                        it.isCheckable = true
                        bottom_navigation.menu.getItem(0).isCheckable = true
                    }
                }
                R.id.earningBottomMenu -> {
                    if (getCurrentFragment() !is EarningsFragment) {
                        navigateTo(EarningsFragment(), true)
                        it.isCheckable = true
                        bottom_navigation.menu.getItem(1).isCheckable = true
                        true

                    }
                }
                R.id.profileBottomMenu -> {
                    if (getCurrentFragment() !is MyProfileDriverFragment) {
                        navigateTo(MyProfileDriverFragment(), true)
                        it.isCheckable = true
                        bottom_navigation.menu.getItem(2).isCheckable = true
                        true

                    }

                }

                else -> {
                    bottom_navigation.menu.getItem(2).isCheckable = false
                }


            }
            true
        }
        startPickup.setOnClickListener {
            if (getCurrentFragment() is MainFragment)
                (getCurrentFragment() as MainFragment).openEasyRideDialog()
            else {

            }

        }
        //For Push
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(pushReceiver, IntentFilter(Push.action))


        //For internet check
        val filter = IntentFilter()
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE")
        registerReceiver(networkStateChangeReceiver, filter)

        LocalBroadcastManager.getInstance(this)
            .registerReceiver(networkChangeReceiver, IntentFilter("NetworkChange"))

    }


    override fun navigateTo(fragment: Fragment, addToBackstack: Boolean) {


        val transaction = supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(
                R.anim.slide_in_left,
                R.anim.slide_out_right,
                R.anim.slide_in_right,
                R.anim.slide_out_left
            )
            .replace(R.id.container, fragment)

        if (addToBackstack) {
            transaction.addToBackStack(null)
        }

        try {
            transaction.commit()
        } catch (e: Exception) {
            transaction.commitAllowingStateLoss()

        }

    }

    private fun getCurrentFragment(): Fragment? {
        return supportFragmentManager.findFragmentById(R.id.container)
    }


    fun showSnackBar(message: String) {
        if ((snackBarParent != null) and !isFinishing)
            Snackbar.make(this.snackBarParent!!, message, Snackbar.LENGTH_LONG).show()

    }

    private fun addViewSnackBar() {
        snackBarParent = View(this)
        val layoutParams = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, 200)
        layoutParams.gravity = Gravity.CENTER
        snackBarParent!!.layoutParams = layoutParams
        container.addView(snackBarParent)
    }

    public fun showexit() {
        //Store Cart Data before exit app

        if (doubleBackToExitPressedOnce) {

            finishAffinity()
            return
        }

        doubleBackToExitPressedOnce = true


        showSnackBar("To exit, press back again.")

        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 3000)

    }

    override fun onBackPressed() {

        if (getCurrentFragment() == null) {
            super.onBackPressed()
        } else if (getCurrentFragment() is MainFragment) {
            (getCurrentFragment() as MainFragment).backPressed()

        } else {
            if (supportFragmentManager.backStackEntryCount >= 1) {

                val f = supportFragmentManager.findFragmentById(R.id.container)
                if (supportFragmentManager.backStackEntryCount == 1) {
                    if (f != null && f is MainFragment) {
                        (getCurrentFragment() as MainFragment).backPressed()
                    } else {
                        super.onBackPressed()
                    }
                } else
                    supportFragmentManager.popBackStack()
            } else {
                showexit()
            }
        }
    }

    fun setBottomNavMenuSelected(selctedMenuId: Int) {

        bottom_navigation.menu.findItem(selctedMenuId).isChecked = true

    }

    fun showBadge(@IdRes itemId: Int) {
        val itemView = bottom_navigation.findViewById<BottomNavigationItemView>(itemId)
        val badge = LayoutInflater.from(this).inflate(R.layout.notification_badge, bottom_navigation, false)

        itemView.addView(badge)
    }

    fun removeBadge(@IdRes itemId: Int) {
        val itemView = bottomNavigation.findViewById<BottomNavigationItemView>(itemId)
        if (itemView.childCount == 3) {
            itemView.removeViewAt(2)
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)


        if (getCurrentFragment() is MainFragment) {

            getCurrentFragment()!!.onActivityResult(requestCode, resultCode, data)

        }
    }

    fun errorMethod() {
        try {
            if (!MyUtils.isInternetAvailable(this@MainActivity)) {
                showSnackBar(resources.getString(R.string.error_common_network))
            } else {
                showSnackBar(resources.getString(R.string.error_crash_error_message))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    private val pushReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action!!.equals(
                    Push.action,
                    ignoreCase = true
                ) && intent.hasExtra("PushData") && !this@MainActivity.isFinishing
            ) {
                if (getCurrentFragment() !is MainFragment) {
                    var mainFragment = supportFragmentManager.findFragmentByTag(MainFragment::class.java.name)
                    if (mainFragment == null)
                        mainFragment = MainFragment()
                    if (push != null) {
                        var bundle = Bundle()

                        bundle.putSerializable("Push", push)

                        mainFragment.arguments = bundle
                    }

                    navigateTo(mainFragment, true)
                } else {
                    var push = intent.getSerializableExtra("PushData") as Push

                    (getCurrentFragment() as MainFragment).pushHandler(push)
                }

            }
        }
    }

    private val networkChangeReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent!!.hasExtra("isInternet") && !this@MainActivity.isFinishing) {
                if (getCurrentFragment() is MainFragment) {
                    (getCurrentFragment() as MainFragment).updateInternetUI(intent.getBooleanExtra("isInternet", false))
                }
            }

        }
    }

    override fun onProfileUpdate(imageName: String) {
        val myFragm = supportFragmentManager.findFragmentByTag(MainFragment::class.java.name) as MainFragment?
        if (myFragm != null) {
            myFragm!!.onProfileUpdate(imageName)
        }
    }

    override fun onDestroy() {
        try {
            mWakeLock!!.release()
            unregisterReceiver(networkStateChangeReceiver)
        } catch (e: Exception) {
        }


        super.onDestroy()
    }


    fun create_logcat() {
        val fullName = "Cab1Driver" + "_log.txt"
        val file = File(Environment.getExternalStorageDirectory(), fullName)


        if (!file.exists()) {
            file.createNewFile()
        } else {


        }
        try {
            val command = String.format("logcat -d")
            val process = Runtime.getRuntime().exec(command)

            val reader = BufferedReader(InputStreamReader(process.inputStream))
            val result = StringBuilder()


            while ((reader.readLine() != null)) {


                result.append(reader.readLine())
                result.append("\n")

            }
            val out = FileWriter(file)
            out.write(result.toString())
            out.close()

        } catch (e: IOException) {
        }

    }

    fun clear_log() {
        try {
            Runtime.getRuntime().exec("logcat -c")
        } catch (e: IOException) {

        }

    }


}
