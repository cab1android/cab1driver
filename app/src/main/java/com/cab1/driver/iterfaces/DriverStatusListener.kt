package com.cab1.driver.iterfaces

import com.cab1.driver.fragment.RiderStartNavigation

import com.cab1.driver.pojo.GoogleDirection
import com.cab1.driver.pojo.StartTripData
import com.google.android.gms.maps.model.LatLng

interface DriverStatusListener {
    fun onTripAcceptSuccess(acceptRequestData: StartTripData, directionRouteResponse: GoogleDirection)
    fun onTripAcceptFailure(position: Int)
    fun onRidePickUpStartNavigation(navigationLatLong: LatLng)
    fun onRideCancel()
    fun onRideReach(acceptRequestData: StartTripData)
    fun onCallRider()
    fun onRiderTripDropStartNavigation(navigationLatLong:LatLng)
    fun onRideStartEnd(from:String,riderStartNavigationDialog: RiderStartNavigation)
    fun onRideStartSuccess(startTripData: StartTripData)

    fun onSuccessfullyCancelTrip()
    fun onStreetTripBook(streetPickUpData: StartTripData)
    fun onReRoute(startTripData: StartTripData,dropLatLng: LatLng)

}